USE [GESOM]
GO
/****** Object:  Table [dbo].[attach_file]    Script Date: 27/04/2020 09:44:57 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[attach_file](
	[attach_file_id] [int] IDENTITY(1,1) NOT NULL,
	[blob] [nvarchar](max) NULL,
 CONSTRAINT [PK_attach_file] PRIMARY KEY CLUSTERED 
(
	[attach_file_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[attach_file_om]    Script Date: 27/04/2020 09:44:57 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[attach_file_om](
	[attach_file_om_id] [int] IDENTITY(1,1) NOT NULL,
	[attach_file_id] [int] NOT NULL,
	[om_id] [int] NOT NULL,
	[upload_date] [datetime] NULL,
 CONSTRAINT [PK_AttachFileOM] PRIMARY KEY CLUSTERED 
(
	[attach_file_om_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[car_ocm]    Script Date: 27/04/2020 09:44:57 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[car_ocm](
	[car_ocm_id] [nvarchar](50) NOT NULL,
	[car_ocm_desc] [nvarchar](max) NULL,
 CONSTRAINT [PK_CarOcm] PRIMARY KEY CLUSTERED 
(
	[car_ocm_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[department]    Script Date: 27/04/2020 09:44:57 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[department](
	[department_id] [int] IDENTITY(1,1) NOT NULL,
	[department_name] [nvarchar](100) NULL,
	[department_budget] [int] NULL,
	[direction_id] [int] NULL,
 CONSTRAINT [PK_department] PRIMARY KEY CLUSTERED 
(
	[department_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[destination]    Script Date: 27/04/2020 09:44:57 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[destination](
	[destination_id] [int] IDENTITY(1,1) NOT NULL,
	[hosting_id] [int] NOT NULL,
	[start_date] [datetime] NULL,
	[end_date] [datetime] NULL,
 CONSTRAINT [PK_Destination_1] PRIMARY KEY CLUSTERED 
(
	[destination_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[destination_om]    Script Date: 27/04/2020 09:44:57 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[destination_om](
	[destination_om_id] [int] IDENTITY(1,1) NOT NULL,
	[destination_id] [int] NOT NULL,
	[om_id] [int] NOT NULL,
 CONSTRAINT [PK_destination_om] PRIMARY KEY CLUSTERED 
(
	[destination_om_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[direction]    Script Date: 27/04/2020 09:44:57 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[direction](
	[direction_id] [int] IDENTITY(1,1) NOT NULL,
	[direction_name] [nvarchar](100) NULL,
	[direction_budget] [int] NULL,
 CONSTRAINT [PK_direction] PRIMARY KEY CLUSTERED 
(
	[direction_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[driver_ocm]    Script Date: 27/04/2020 09:44:57 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[driver_ocm](
	[driver_ocm_id] [int] IDENTITY(1,1) NOT NULL,
	[driver_first_name] [nvarchar](50) NULL,
	[driver_last_name] [nvarchar](50) NULL,
	[driver_availability] [bit] NOT NULL,
 CONSTRAINT [PK_DriverOcm] PRIMARY KEY CLUSTERED 
(
	[driver_ocm_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[hosting]    Script Date: 27/04/2020 09:44:57 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[hosting](
	[hosting_id] [int] IDENTITY(1,1) NOT NULL,
	[town_id] [int] NULL,
	[hosting_name] [nvarchar](50) NULL,
	[fees_host] [int] NULL,
	[fees_food] [int] NULL,
 CONSTRAINT [PK_hosting_1] PRIMARY KEY CLUSTERED 
(
	[hosting_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[om]    Script Date: 27/04/2020 09:44:57 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[om](
	[om_id] [int] IDENTITY(1,1) NOT NULL,
	[user_id] [int] NULL,
	[om_motive] [nvarchar](500) NULL,
	[om_desc] [nvarchar](max) NULL,
	[om_type_id] [int] NULL,
	[sector_id] [int] NULL,
	[create_date] [datetime] NULL,
	[bus_fees] [int] NULL,
	[taxi_fees] [int] NULL,
	[train_fees] [int] NULL,
	[mile_allowance] [int] NULL,
	[road_toll] [int] NULL,
	[car_ocm_id] [nvarchar](50) NULL,
	[driver_ocm_id] [int] NULL,
	[allowed_fees] [int] NULL,
	[daily_plan] [bit] NULL,
	[end_hour] [nvarchar](50) NULL,
	[total_fees] [int] NULL,
 CONSTRAINT [PK_Om] PRIMARY KEY CLUSTERED 
(
	[om_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[om_comments]    Script Date: 27/04/2020 09:44:57 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[om_comments](
	[om_comments_id] [int] IDENTITY(1,1) NOT NULL,
	[om_id] [int] NULL,
	[sender_id] [int] NULL,
	[receiver_id] [int] NULL,
	[comment] [nvarchar](500) NULL,
	[comment_date] [datetime] NULL,
 CONSTRAINT [PK_om_comments] PRIMARY KEY CLUSTERED 
(
	[om_comments_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[om_validator]    Script Date: 27/04/2020 09:44:57 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[om_validator](
	[om_validator_id] [int] IDENTITY(1,1) NOT NULL,
	[om_id] [int] NULL,
	[user_id] [int] NULL,
	[valid_date] [datetime] NULL,
	[state_id] [int] NULL,
	[message] [nchar](500) NULL,
 CONSTRAINT [PK_om_validator] PRIMARY KEY CLUSTERED 
(
	[om_validator_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[profile]    Script Date: 27/04/2020 09:44:57 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[profile](
	[profile_id] [int] IDENTITY(1,1) NOT NULL,
	[profile_code] [nvarchar](50) NULL,
	[profile_name] [nvarchar](50) NULL,
 CONSTRAINT [PK_profile] PRIMARY KEY CLUSTERED 
(
	[profile_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[role]    Script Date: 27/04/2020 09:44:57 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[role](
	[role_id] [int] IDENTITY(1,1) NOT NULL,
	[role_name] [nvarchar](50) NULL,
 CONSTRAINT [PK_Role] PRIMARY KEY CLUSTERED 
(
	[role_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[sector]    Script Date: 27/04/2020 09:44:57 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[sector](
	[sector_id] [int] IDENTITY(1,1) NOT NULL,
	[sector_name] [nvarchar](50) NULL,
 CONSTRAINT [PK_Sector] PRIMARY KEY CLUSTERED 
(
	[sector_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[service]    Script Date: 27/04/2020 09:44:57 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[service](
	[service_id] [int] IDENTITY(1,1) NOT NULL,
	[service_name] [nvarchar](100) NULL,
	[service_budget] [int] NULL,
	[department_id] [int] NULL,
 CONSTRAINT [PK_service_1] PRIMARY KEY CLUSTERED 
(
	[service_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[state]    Script Date: 27/04/2020 09:44:57 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[state](
	[state_id] [int] IDENTITY(1,1) NOT NULL,
	[state_type_id] [int] NOT NULL,
	[om_id] [int] NOT NULL,
	[state_date] [datetime] NULL,
 CONSTRAINT [PK_State] PRIMARY KEY CLUSTERED 
(
	[state_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[state_type]    Script Date: 27/04/2020 09:44:57 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[state_type](
	[state_type_id] [int] IDENTITY(1,1) NOT NULL,
	[state_type_name] [nvarchar](50) NULL,
 CONSTRAINT [PK_StateType] PRIMARY KEY CLUSTERED 
(
	[state_type_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[town]    Script Date: 27/04/2020 09:44:57 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[town](
	[town_id] [int] IDENTITY(1,1) NOT NULL,
	[town_name] [nvarchar](50) NULL,
 CONSTRAINT [PK_Destination] PRIMARY KEY CLUSTERED 
(
	[town_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[type_om]    Script Date: 27/04/2020 09:44:57 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[type_om](
	[type_om_id] [int] IDENTITY(1,1) NOT NULL,
	[type_om_name] [nvarchar](50) NULL,
 CONSTRAINT [PK_TypeOM] PRIMARY KEY CLUSTERED 
(
	[type_om_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[user]    Script Date: 27/04/2020 09:44:57 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[user](
	[user_id] [int] IDENTITY(1,1) NOT NULL,
	[login] [nvarchar](50) NOT NULL,
	[name] [nvarchar](50) NULL,
	[email] [nvarchar](100) NULL,
	[phone] [int] NULL,
	[is_active] [bit] NULL,
	[profile_id] [int] NULL,
	[direction_id] [int] NULL,
	[department_id] [int] NULL,
	[service_id] [int] NULL,
	[birth_date] [datetime] NULL,
	[role_id] [int] NULL,
 CONSTRAINT [PK_User] PRIMARY KEY CLUSTERED 
(
	[user_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
SET IDENTITY_INSERT [dbo].[attach_file] ON 

INSERT [dbo].[attach_file] ([attach_file_id], [blob]) VALUES (68, N'xxx')
INSERT [dbo].[attach_file] ([attach_file_id], [blob]) VALUES (71, N'xxx')
INSERT [dbo].[attach_file] ([attach_file_id], [blob]) VALUES (79, N'xxx')
INSERT [dbo].[attach_file] ([attach_file_id], [blob]) VALUES (80, N'xxx')
INSERT [dbo].[attach_file] ([attach_file_id], [blob]) VALUES (83, N'xxx')
INSERT [dbo].[attach_file] ([attach_file_id], [blob]) VALUES (84, N'xxx')
INSERT [dbo].[attach_file] ([attach_file_id], [blob]) VALUES (85, N'xxx')
INSERT [dbo].[attach_file] ([attach_file_id], [blob]) VALUES (86, N'xxx')
INSERT [dbo].[attach_file] ([attach_file_id], [blob]) VALUES (87, N'xxx')
INSERT [dbo].[attach_file] ([attach_file_id], [blob]) VALUES (88, N'xxx')
INSERT [dbo].[attach_file] ([attach_file_id], [blob]) VALUES (90, N'xxx')
SET IDENTITY_INSERT [dbo].[attach_file] OFF
SET IDENTITY_INSERT [dbo].[attach_file_om] ON 

INSERT [dbo].[attach_file_om] ([attach_file_om_id], [attach_file_id], [om_id], [upload_date]) VALUES (68, 68, 89, CAST(N'2020-04-15T19:40:28.943' AS DateTime))
INSERT [dbo].[attach_file_om] ([attach_file_om_id], [attach_file_id], [om_id], [upload_date]) VALUES (71, 71, 92, CAST(N'2020-04-15T19:43:38.960' AS DateTime))
INSERT [dbo].[attach_file_om] ([attach_file_om_id], [attach_file_id], [om_id], [upload_date]) VALUES (79, 79, 100, CAST(N'2020-04-15T20:12:50.167' AS DateTime))
INSERT [dbo].[attach_file_om] ([attach_file_om_id], [attach_file_id], [om_id], [upload_date]) VALUES (80, 80, 101, CAST(N'2020-04-15T20:14:25.617' AS DateTime))
INSERT [dbo].[attach_file_om] ([attach_file_om_id], [attach_file_id], [om_id], [upload_date]) VALUES (83, 83, 104, CAST(N'2020-04-16T07:39:40.283' AS DateTime))
INSERT [dbo].[attach_file_om] ([attach_file_om_id], [attach_file_id], [om_id], [upload_date]) VALUES (84, 84, 105, CAST(N'2020-04-16T09:36:55.103' AS DateTime))
INSERT [dbo].[attach_file_om] ([attach_file_om_id], [attach_file_id], [om_id], [upload_date]) VALUES (85, 85, 106, CAST(N'2020-04-16T09:45:59.740' AS DateTime))
INSERT [dbo].[attach_file_om] ([attach_file_om_id], [attach_file_id], [om_id], [upload_date]) VALUES (86, 86, 107, CAST(N'2020-04-16T09:49:27.793' AS DateTime))
INSERT [dbo].[attach_file_om] ([attach_file_om_id], [attach_file_id], [om_id], [upload_date]) VALUES (87, 87, 108, CAST(N'2020-04-16T10:00:15.823' AS DateTime))
INSERT [dbo].[attach_file_om] ([attach_file_om_id], [attach_file_id], [om_id], [upload_date]) VALUES (88, 88, 109, CAST(N'2020-04-16T10:11:53.547' AS DateTime))
INSERT [dbo].[attach_file_om] ([attach_file_om_id], [attach_file_id], [om_id], [upload_date]) VALUES (90, 90, 111, CAST(N'2020-04-16T16:42:47.103' AS DateTime))
SET IDENTITY_INSERT [dbo].[attach_file_om] OFF
INSERT [dbo].[car_ocm] ([car_ocm_id], [car_ocm_desc]) VALUES (N'CE197YP', N'Bougna')
INSERT [dbo].[car_ocm] ([car_ocm_id], [car_ocm_desc]) VALUES (N'CE237PJ', N'Toyota Corolla')
INSERT [dbo].[car_ocm] ([car_ocm_id], [car_ocm_desc]) VALUES (N'LT000FC', N'Batmobile')
INSERT [dbo].[car_ocm] ([car_ocm_id], [car_ocm_desc]) VALUES (N'LT001QG', N'Boite de sardine')
INSERT [dbo].[car_ocm] ([car_ocm_id], [car_ocm_desc]) VALUES (N'LT521PC', N'Test Car')
INSERT [dbo].[car_ocm] ([car_ocm_id], [car_ocm_desc]) VALUES (N'OU985TS', N'Clando')
SET IDENTITY_INSERT [dbo].[department] ON 

INSERT [dbo].[department] ([department_id], [department_name], [department_budget], [direction_id]) VALUES (1, N'ADMINISTRATION DEC', 20000000, 1)
INSERT [dbo].[department] ([department_id], [department_name], [department_budget], [direction_id]) VALUES (2, N'Call Center Externes', 20000000, 1)
INSERT [dbo].[department] ([department_id], [department_name], [department_budget], [direction_id]) VALUES (3, N'CONTACT CENTER', 20000000, 1)
INSERT [dbo].[department] ([department_id], [department_name], [department_budget], [direction_id]) VALUES (4, N'EXPERIENCE CLIENT', 20000000, 1)
INSERT [dbo].[department] ([department_id], [department_name], [department_budget], [direction_id]) VALUES (5, N'PMO DEC', 20000000, 1)
INSERT [dbo].[department] ([department_id], [department_name], [department_budget], [direction_id]) VALUES (6, N'RELATION CLIENT B2B', 20000000, 1)
INSERT [dbo].[department] ([department_id], [department_name], [department_budget], [direction_id]) VALUES (7, N'SUPPORT ET QUALITE', 20000000, 1)
INSERT [dbo].[department] ([department_id], [department_name], [department_budget], [direction_id]) VALUES (8, N'ADMINISTRATION DD', 20000000, 2)
INSERT [dbo].[department] ([department_id], [department_name], [department_budget], [direction_id]) VALUES (9, N'CHANELS AND SUPPORT', 20000000, 2)
INSERT [dbo].[department] ([department_id], [department_name], [department_budget], [direction_id]) VALUES (10, N'DELEGATION ADAMAOUA/NORD/EXTREME NORD', 20000000, 2)
INSERT [dbo].[department] ([department_id], [department_name], [department_budget], [direction_id]) VALUES (11, N'DELEGATION CENTRE/EST/SUD', 20000000, 2)
INSERT [dbo].[department] ([department_id], [department_name], [department_budget], [direction_id]) VALUES (12, N'DELEGATION LITTORAL/SUD OUEST', 20000000, 2)
INSERT [dbo].[department] ([department_id], [department_name], [department_budget], [direction_id]) VALUES (13, N'DELEGATION OUEST/NORD OUEST', 20000000, 2)
INSERT [dbo].[department] ([department_id], [department_name], [department_budget], [direction_id]) VALUES (14, N'PERFORMANCE ET GO TO MARKET', 20000000, 2)
INSERT [dbo].[department] ([department_id], [department_name], [department_budget], [direction_id]) VALUES (15, N'PILOTAGE MULTICANAL', 20000000, 2)
INSERT [dbo].[department] ([department_id], [department_name], [department_budget], [direction_id]) VALUES (16, N'PMO', 20000000, 2)
INSERT [dbo].[department] ([department_id], [department_name], [department_budget], [direction_id]) VALUES (17, N'SUPPORT ET ADMINISTRATION DES VENTES', 20000000, 2)
INSERT [dbo].[department] ([department_id], [department_name], [department_budget], [direction_id]) VALUES (18, N'ADMINISTRATION DFASC', 20000000, 3)
INSERT [dbo].[department] ([department_id], [department_name], [department_budget], [direction_id]) VALUES (19, N'DIVCG', 20000000, 3)
INSERT [dbo].[department] ([department_id], [department_name], [department_budget], [direction_id]) VALUES (20, N'DIVCT', 20000000, 3)
INSERT [dbo].[department] ([department_id], [department_name], [department_budget], [direction_id]) VALUES (21, N'DIVISION SUPPLY CHAIN', 20000000, 3)
INSERT [dbo].[department] ([department_id], [department_name], [department_budget], [direction_id]) VALUES (22, N'REVENUE ET ASSURANCE', 20000000, 3)
INSERT [dbo].[department] ([department_id], [department_name], [department_budget], [direction_id]) VALUES (23, N'TRESORERIE', 20000000, 3)
INSERT [dbo].[department] ([department_id], [department_name], [department_budget], [direction_id]) VALUES (24, N'ADMINISTRATION DG', 20000000, 4)
INSERT [dbo].[department] ([department_id], [department_name], [department_budget], [direction_id]) VALUES (25, N'ADMINISTRATION DID', 20000000, 5)
INSERT [dbo].[department] ([department_id], [department_name], [department_budget], [direction_id]) VALUES (26, N'ADMINISTRATION DIDR', 20000000, 6)
INSERT [dbo].[department] ([department_id], [department_name], [department_budget], [direction_id]) VALUES (27, N'DELIVERY RAN', 20000000, 6)
INSERT [dbo].[department] ([department_id], [department_name], [department_budget], [direction_id]) VALUES (28, N'DEPLOIEMENT ET ENVIRONNEMENT TECHNIQUE', 20000000, 6)
INSERT [dbo].[department] ([department_id], [department_name], [department_budget], [direction_id]) VALUES (29, N'INGENIERIE CORE NETWORK', 20000000, 6)
INSERT [dbo].[department] ([department_id], [department_name], [department_budget], [direction_id]) VALUES (30, N'INGENIERIE IP', 20000000, 6)
INSERT [dbo].[department] ([department_id], [department_name], [department_budget], [direction_id]) VALUES (31, N'INGENIERIE TRANSMISSIONS', 20000000, 6)
INSERT [dbo].[department] ([department_id], [department_name], [department_budget], [direction_id]) VALUES (32, N'ADMINISTRATION DMC', 20000000, 7)
INSERT [dbo].[department] ([department_id], [department_name], [department_budget], [direction_id]) VALUES (33, N'BUSINESS INTELLIGENCE', 20000000, 7)
INSERT [dbo].[department] ([department_id], [department_name], [department_budget], [direction_id]) VALUES (34, N'MARKETING OPERATIONS', 20000000, 7)
INSERT [dbo].[department] ([department_id], [department_name], [department_budget], [direction_id]) VALUES (35, N'Projets transverses et Partenariats stratégiques', 20000000, 7)
INSERT [dbo].[department] ([department_id], [department_name], [department_budget], [direction_id]) VALUES (36, N'SEGMENTS INNOVATION AND DIGITAL', 20000000, 7)
INSERT [dbo].[department] ([department_id], [department_name], [department_budget], [direction_id]) VALUES (37, N'STRATEGIC BUSINESS DEVELOPMENT AND PLANNING', 20000000, 7)
INSERT [dbo].[department] ([department_id], [department_name], [department_budget], [direction_id]) VALUES (38, N'VALUE MANAGEMENT', 20000000, 7)
INSERT [dbo].[department] ([department_id], [department_name], [department_budget], [direction_id]) VALUES (39, N'WHOLESALES', 20000000, 7)
INSERT [dbo].[department] ([department_id], [department_name], [department_budget], [direction_id]) VALUES (40, N'ADMINISTRATION DOB', 20000000, 8)
INSERT [dbo].[department] ([department_id], [department_name], [department_budget], [direction_id]) VALUES (41, N'MARKETING B2B', 20000000, 8)
INSERT [dbo].[department] ([department_id], [department_name], [department_budget], [direction_id]) VALUES (42, N'PMO DRS', 20000000, 8)
INSERT [dbo].[department] ([department_id], [department_name], [department_budget], [direction_id]) VALUES (43, N'Sales Support and Business', 20000000, 8)
INSERT [dbo].[department] ([department_id], [department_name], [department_budget], [direction_id]) VALUES (44, N'SME SOHO', 20000000, 8)
INSERT [dbo].[department] ([department_id], [department_name], [department_budget], [direction_id]) VALUES (45, N'Ventes CORPORATE', 20000000, 8)
INSERT [dbo].[department] ([department_id], [department_name], [department_budget], [direction_id]) VALUES (46, N'ADMINISTRATION DOM', 20000000, 9)
INSERT [dbo].[department] ([department_id], [department_name], [department_budget], [direction_id]) VALUES (47, N'CONFORMITE', 20000000, 9)
INSERT [dbo].[department] ([department_id], [department_name], [department_budget], [direction_id]) VALUES (48, N'CORPORATE ET PARTENARIATS', 20000000, 9)
INSERT [dbo].[department] ([department_id], [department_name], [department_budget], [direction_id]) VALUES (49, N'DISTRIBUTION OM', 20000000, 9)
INSERT [dbo].[department] ([department_id], [department_name], [department_budget], [direction_id]) VALUES (50, N'MANAGEMENT DES OPERATIONS ET REPORTING', 20000000, 9)
INSERT [dbo].[department] ([department_id], [department_name], [department_budget], [direction_id]) VALUES (51, N'MARKETING ET INNOVATION', 20000000, 9)
INSERT [dbo].[department] ([department_id], [department_name], [department_budget], [direction_id]) VALUES (52, N'Opérations et Confirmités', 20000000, 9)
INSERT [dbo].[department] ([department_id], [department_name], [department_budget], [direction_id]) VALUES (53, N'PMO', 20000000, 9)
INSERT [dbo].[department] ([department_id], [department_name], [department_budget], [direction_id]) VALUES (54, N'TECHNIQUE ET INFORMATIQUE', 20000000, 9)
INSERT [dbo].[department] ([department_id], [department_name], [department_budget], [direction_id]) VALUES (55, N'DATA CENTER ET SITES SPECIAUX', 20000000, 10)
INSERT [dbo].[department] ([department_id], [department_name], [department_budget], [direction_id]) VALUES (56, N'TMC - Technology Management Center', 20000000, 10)
INSERT [dbo].[department] ([department_id], [department_name], [department_budget], [direction_id]) VALUES (57, N'ADMINISTRATION DRAP', 20000000, 11)
INSERT [dbo].[department] ([department_id], [department_name], [department_budget], [direction_id]) VALUES (58, N'AUDIT ET PROCESS', 20000000, 11)
INSERT [dbo].[department] ([department_id], [department_name], [department_budget], [direction_id]) VALUES (59, N'CONTROLE INTERNE ET RISQUES', 20000000, 11)
INSERT [dbo].[department] ([department_id], [department_name], [department_budget], [direction_id]) VALUES (60, N'INSPECTION ET CONFORMITE', 20000000, 11)
INSERT [dbo].[department] ([department_id], [department_name], [department_budget], [direction_id]) VALUES (61, N'PMO', 20000000, 11)
INSERT [dbo].[department] ([department_id], [department_name], [department_budget], [direction_id]) VALUES (62, N'QUALITE ET PROCESSUS', 20000000, 11)
INSERT [dbo].[department] ([department_id], [department_name], [department_budget], [direction_id]) VALUES (63, N'TRANSFORMATION', 20000000, 11)
INSERT [dbo].[department] ([department_id], [department_name], [department_budget], [direction_id]) VALUES (64, N'ADMINISTRATION DRH', 20000000, 12)
INSERT [dbo].[department] ([department_id], [department_name], [department_budget], [direction_id]) VALUES (65, N'ADMINISTRATION DU PERSONNEL', 20000000, 12)
INSERT [dbo].[department] ([department_id], [department_name], [department_budget], [direction_id]) VALUES (66, N'AFFAIRES GENERALES', 20000000, 12)
INSERT [dbo].[department] ([department_id], [department_name], [department_budget], [direction_id]) VALUES (67, N'COMMUNICATION INTERNE', 20000000, 12)
INSERT [dbo].[department] ([department_id], [department_name], [department_budget], [direction_id]) VALUES (68, N'DEVELOPPEMENT CARRIERE', 20000000, 12)
INSERT [dbo].[department] ([department_id], [department_name], [department_budget], [direction_id]) VALUES (69, N'RECRUTEMENT', 20000000, 12)
INSERT [dbo].[department] ([department_id], [department_name], [department_budget], [direction_id]) VALUES (70, N'ADMINISTRATION DRI', 20000000, 13)
INSERT [dbo].[department] ([department_id], [department_name], [department_budget], [direction_id]) VALUES (71, N'ADMINISTRATION DRS', 20000000, 14)
INSERT [dbo].[department] ([department_id], [department_name], [department_budget], [direction_id]) VALUES (72, N'OPERATIONS BACK OFFICE', 20000000, 14)
INSERT [dbo].[department] ([department_id], [department_name], [department_budget], [direction_id]) VALUES (73, N'OPERATIONS FRONT OFFICE', 20000000, 14)
INSERT [dbo].[department] ([department_id], [department_name], [department_budget], [direction_id]) VALUES (74, N'PMO DRS', 20000000, 14)
INSERT [dbo].[department] ([department_id], [department_name], [department_budget], [direction_id]) VALUES (75, N'SECURITE STRATEGIE ET VEILLE ITN', 20000000, 14)
INSERT [dbo].[department] ([department_id], [department_name], [department_budget], [direction_id]) VALUES (76, N'ADMINISTRATION DSI', 20000000, 15)
INSERT [dbo].[department] ([department_id], [department_name], [department_budget], [direction_id]) VALUES (77, N'INFRASTRUCTURE SI', 20000000, 15)
INSERT [dbo].[department] ([department_id], [department_name], [department_budget], [direction_id]) VALUES (78, N'PLATEFORME DE SERVICES ET DEVELOPPEMENT', 20000000, 15)
INSERT [dbo].[department] ([department_id], [department_name], [department_budget], [direction_id]) VALUES (79, N'SI COMMERCIAL ET CORPORATE', 20000000, 15)
INSERT [dbo].[department] ([department_id], [department_name], [department_budget], [direction_id]) VALUES (80, N'SYSTEMES DECISIONNELS', 20000000, 15)
INSERT [dbo].[department] ([department_id], [department_name], [department_budget], [direction_id]) VALUES (81, N'NOK', 20000000, 16)
INSERT [dbo].[department] ([department_id], [department_name], [department_budget], [direction_id]) VALUES (82, N'ADMINISTRATION FONDATION', 20000000, 17)
INSERT [dbo].[department] ([department_id], [department_name], [department_budget], [direction_id]) VALUES (83, N'INACTIF', 20000000, 18)
INSERT [dbo].[department] ([department_id], [department_name], [department_budget], [direction_id]) VALUES (84, N'ADMINISTRATION ODC', 20000000, 19)
INSERT [dbo].[department] ([department_id], [department_name], [department_budget], [direction_id]) VALUES (85, N'ADMINISTRATION SECURITE', 20000000, 20)
INSERT [dbo].[department] ([department_id], [department_name], [department_budget], [direction_id]) VALUES (86, N'ADMINISTRATION SG', 20000000, 21)
INSERT [dbo].[department] ([department_id], [department_name], [department_budget], [direction_id]) VALUES (87, N'JURIDIQUE ET CONTENTIEUX', 20000000, 21)
INSERT [dbo].[department] ([department_id], [department_name], [department_budget], [direction_id]) VALUES (88, N'REGLEMENTATION WHOLESALE ET INTERCONNEXION', 20000000, 21)
INSERT [dbo].[department] ([department_id], [department_name], [department_budget], [direction_id]) VALUES (89, N'DPT TEST', 20000000, 22)
SET IDENTITY_INSERT [dbo].[department] OFF
SET IDENTITY_INSERT [dbo].[destination] ON 

INSERT [dbo].[destination] ([destination_id], [hosting_id], [start_date], [end_date]) VALUES (1, 1, CAST(N'2020-03-18T00:00:00.000' AS DateTime), CAST(N'2020-03-19T00:00:00.000' AS DateTime))
INSERT [dbo].[destination] ([destination_id], [hosting_id], [start_date], [end_date]) VALUES (2, 2, CAST(N'2020-03-25T00:00:00.000' AS DateTime), CAST(N'2020-03-28T00:00:00.000' AS DateTime))
INSERT [dbo].[destination] ([destination_id], [hosting_id], [start_date], [end_date]) VALUES (3, 1, CAST(N'2020-03-17T11:47:39.253' AS DateTime), CAST(N'2020-03-17T11:47:39.253' AS DateTime))
INSERT [dbo].[destination] ([destination_id], [hosting_id], [start_date], [end_date]) VALUES (4, 1, CAST(N'2020-03-17T11:47:39.253' AS DateTime), CAST(N'2020-03-17T11:47:39.253' AS DateTime))
INSERT [dbo].[destination] ([destination_id], [hosting_id], [start_date], [end_date]) VALUES (5, 1, CAST(N'2020-03-17T13:56:00.000' AS DateTime), CAST(N'2020-03-17T13:56:00.000' AS DateTime))
INSERT [dbo].[destination] ([destination_id], [hosting_id], [start_date], [end_date]) VALUES (6, 1, CAST(N'2020-03-17T14:21:00.000' AS DateTime), CAST(N'2020-03-17T14:21:00.000' AS DateTime))
INSERT [dbo].[destination] ([destination_id], [hosting_id], [start_date], [end_date]) VALUES (7, 3, CAST(N'2020-03-17T14:27:00.000' AS DateTime), CAST(N'2020-03-20T14:27:00.000' AS DateTime))
INSERT [dbo].[destination] ([destination_id], [hosting_id], [start_date], [end_date]) VALUES (8, 1, CAST(N'2020-03-17T14:30:00.000' AS DateTime), CAST(N'2020-03-17T14:30:00.000' AS DateTime))
INSERT [dbo].[destination] ([destination_id], [hosting_id], [start_date], [end_date]) VALUES (9, 1, CAST(N'2020-03-17T14:30:00.000' AS DateTime), CAST(N'2020-03-17T14:30:00.000' AS DateTime))
INSERT [dbo].[destination] ([destination_id], [hosting_id], [start_date], [end_date]) VALUES (12, 1, CAST(N'2020-03-17T14:30:00.000' AS DateTime), CAST(N'2020-03-17T14:30:00.000' AS DateTime))
INSERT [dbo].[destination] ([destination_id], [hosting_id], [start_date], [end_date]) VALUES (14, 3, CAST(N'2020-03-18T00:00:00.000' AS DateTime), CAST(N'2020-03-19T00:00:00.000' AS DateTime))
INSERT [dbo].[destination] ([destination_id], [hosting_id], [start_date], [end_date]) VALUES (15, 3, CAST(N'2020-03-25T00:00:00.000' AS DateTime), CAST(N'2020-03-28T00:00:00.000' AS DateTime))
INSERT [dbo].[destination] ([destination_id], [hosting_id], [start_date], [end_date]) VALUES (16, 3, CAST(N'2020-03-25T00:00:00.000' AS DateTime), CAST(N'2020-03-28T00:00:00.000' AS DateTime))
INSERT [dbo].[destination] ([destination_id], [hosting_id], [start_date], [end_date]) VALUES (17, 3, CAST(N'2020-03-18T00:00:00.000' AS DateTime), CAST(N'2020-03-19T00:00:00.000' AS DateTime))
INSERT [dbo].[destination] ([destination_id], [hosting_id], [start_date], [end_date]) VALUES (18, 3, CAST(N'2020-03-17T14:54:00.000' AS DateTime), CAST(N'2020-03-17T14:54:00.000' AS DateTime))
INSERT [dbo].[destination] ([destination_id], [hosting_id], [start_date], [end_date]) VALUES (19, 4, CAST(N'2020-03-17T14:54:00.000' AS DateTime), CAST(N'2020-03-17T14:54:00.000' AS DateTime))
INSERT [dbo].[destination] ([destination_id], [hosting_id], [start_date], [end_date]) VALUES (20, 3, CAST(N'2020-03-17T14:54:00.000' AS DateTime), CAST(N'2020-03-17T14:54:00.000' AS DateTime))
INSERT [dbo].[destination] ([destination_id], [hosting_id], [start_date], [end_date]) VALUES (21, 4, CAST(N'2020-03-17T14:54:00.000' AS DateTime), CAST(N'2020-03-17T14:54:00.000' AS DateTime))
INSERT [dbo].[destination] ([destination_id], [hosting_id], [start_date], [end_date]) VALUES (22, 3, CAST(N'2020-03-25T00:00:00.000' AS DateTime), CAST(N'2020-03-28T00:00:00.000' AS DateTime))
INSERT [dbo].[destination] ([destination_id], [hosting_id], [start_date], [end_date]) VALUES (23, 3, CAST(N'2020-03-18T00:00:00.000' AS DateTime), CAST(N'2020-03-19T00:00:00.000' AS DateTime))
INSERT [dbo].[destination] ([destination_id], [hosting_id], [start_date], [end_date]) VALUES (24, 3, CAST(N'2020-03-25T00:00:00.000' AS DateTime), CAST(N'2020-03-28T00:00:00.000' AS DateTime))
INSERT [dbo].[destination] ([destination_id], [hosting_id], [start_date], [end_date]) VALUES (25, 3, CAST(N'2020-03-18T00:00:00.000' AS DateTime), CAST(N'2020-03-19T00:00:00.000' AS DateTime))
INSERT [dbo].[destination] ([destination_id], [hosting_id], [start_date], [end_date]) VALUES (26, 3, CAST(N'2020-03-25T00:00:00.000' AS DateTime), CAST(N'2020-03-28T00:00:00.000' AS DateTime))
INSERT [dbo].[destination] ([destination_id], [hosting_id], [start_date], [end_date]) VALUES (27, 3, CAST(N'2020-03-18T00:00:00.000' AS DateTime), CAST(N'2020-03-19T00:00:00.000' AS DateTime))
INSERT [dbo].[destination] ([destination_id], [hosting_id], [start_date], [end_date]) VALUES (28, 3, CAST(N'2020-03-25T00:00:00.000' AS DateTime), CAST(N'2020-03-28T00:00:00.000' AS DateTime))
INSERT [dbo].[destination] ([destination_id], [hosting_id], [start_date], [end_date]) VALUES (29, 3, CAST(N'2020-03-18T00:00:00.000' AS DateTime), CAST(N'2020-03-19T00:00:00.000' AS DateTime))
INSERT [dbo].[destination] ([destination_id], [hosting_id], [start_date], [end_date]) VALUES (30, 3, CAST(N'2020-03-25T00:00:00.000' AS DateTime), CAST(N'2020-03-28T00:00:00.000' AS DateTime))
INSERT [dbo].[destination] ([destination_id], [hosting_id], [start_date], [end_date]) VALUES (31, 3, CAST(N'2020-03-18T00:00:00.000' AS DateTime), CAST(N'2020-03-19T00:00:00.000' AS DateTime))
INSERT [dbo].[destination] ([destination_id], [hosting_id], [start_date], [end_date]) VALUES (34, 1, CAST(N'2020-03-17T16:14:00.000' AS DateTime), CAST(N'2020-03-20T16:14:00.000' AS DateTime))
INSERT [dbo].[destination] ([destination_id], [hosting_id], [start_date], [end_date]) VALUES (36, 2, CAST(N'2020-03-17T16:17:00.000' AS DateTime), CAST(N'2020-03-17T16:17:00.000' AS DateTime))
INSERT [dbo].[destination] ([destination_id], [hosting_id], [start_date], [end_date]) VALUES (37, 1, CAST(N'2020-03-17T16:18:00.000' AS DateTime), CAST(N'2020-03-17T16:18:00.000' AS DateTime))
INSERT [dbo].[destination] ([destination_id], [hosting_id], [start_date], [end_date]) VALUES (38, 3, CAST(N'2020-03-17T16:18:00.000' AS DateTime), CAST(N'2020-03-20T16:18:00.000' AS DateTime))
INSERT [dbo].[destination] ([destination_id], [hosting_id], [start_date], [end_date]) VALUES (39, 3, CAST(N'2020-03-17T16:18:00.000' AS DateTime), CAST(N'2020-03-20T16:18:00.000' AS DateTime))
INSERT [dbo].[destination] ([destination_id], [hosting_id], [start_date], [end_date]) VALUES (41, 1, CAST(N'2020-03-17T16:24:00.000' AS DateTime), CAST(N'2020-03-17T16:24:00.000' AS DateTime))
INSERT [dbo].[destination] ([destination_id], [hosting_id], [start_date], [end_date]) VALUES (42, 3, CAST(N'2020-03-17T16:24:00.000' AS DateTime), CAST(N'2020-03-20T16:24:00.000' AS DateTime))
INSERT [dbo].[destination] ([destination_id], [hosting_id], [start_date], [end_date]) VALUES (43, 1, CAST(N'2020-03-17T16:24:00.000' AS DateTime), CAST(N'2020-03-17T16:24:00.000' AS DateTime))
INSERT [dbo].[destination] ([destination_id], [hosting_id], [start_date], [end_date]) VALUES (44, 3, CAST(N'2020-03-17T16:24:00.000' AS DateTime), CAST(N'2020-03-20T16:24:00.000' AS DateTime))
INSERT [dbo].[destination] ([destination_id], [hosting_id], [start_date], [end_date]) VALUES (45, 1, CAST(N'2020-03-17T16:24:00.000' AS DateTime), CAST(N'2020-03-17T16:24:00.000' AS DateTime))
INSERT [dbo].[destination] ([destination_id], [hosting_id], [start_date], [end_date]) VALUES (46, 1, CAST(N'2020-03-17T16:24:00.000' AS DateTime), CAST(N'2020-03-17T16:24:00.000' AS DateTime))
INSERT [dbo].[destination] ([destination_id], [hosting_id], [start_date], [end_date]) VALUES (47, 3, CAST(N'2020-03-17T16:24:00.000' AS DateTime), CAST(N'2020-03-20T16:24:00.000' AS DateTime))
INSERT [dbo].[destination] ([destination_id], [hosting_id], [start_date], [end_date]) VALUES (48, 1, CAST(N'2020-03-17T16:24:00.000' AS DateTime), CAST(N'2020-03-20T16:24:00.000' AS DateTime))
INSERT [dbo].[destination] ([destination_id], [hosting_id], [start_date], [end_date]) VALUES (49, 3, CAST(N'2020-03-17T16:24:00.000' AS DateTime), CAST(N'2020-03-20T16:24:00.000' AS DateTime))
INSERT [dbo].[destination] ([destination_id], [hosting_id], [start_date], [end_date]) VALUES (50, 3, CAST(N'2020-03-17T16:24:00.000' AS DateTime), CAST(N'2020-03-20T16:24:00.000' AS DateTime))
INSERT [dbo].[destination] ([destination_id], [hosting_id], [start_date], [end_date]) VALUES (51, 1, CAST(N'2020-03-17T16:24:00.000' AS DateTime), CAST(N'2020-03-20T16:24:00.000' AS DateTime))
INSERT [dbo].[destination] ([destination_id], [hosting_id], [start_date], [end_date]) VALUES (52, 3, CAST(N'2020-03-17T16:24:00.000' AS DateTime), CAST(N'2020-03-20T16:24:00.000' AS DateTime))
INSERT [dbo].[destination] ([destination_id], [hosting_id], [start_date], [end_date]) VALUES (53, 4, CAST(N'2020-03-17T16:24:00.000' AS DateTime), CAST(N'2020-03-20T16:24:00.000' AS DateTime))
INSERT [dbo].[destination] ([destination_id], [hosting_id], [start_date], [end_date]) VALUES (54, 3, CAST(N'2020-03-17T16:24:00.000' AS DateTime), CAST(N'2020-03-20T16:24:00.000' AS DateTime))
INSERT [dbo].[destination] ([destination_id], [hosting_id], [start_date], [end_date]) VALUES (55, 4, CAST(N'2020-03-17T16:24:00.000' AS DateTime), CAST(N'2020-03-20T16:24:00.000' AS DateTime))
INSERT [dbo].[destination] ([destination_id], [hosting_id], [start_date], [end_date]) VALUES (56, 4, CAST(N'2020-03-17T16:24:00.000' AS DateTime), CAST(N'2020-03-20T16:24:00.000' AS DateTime))
INSERT [dbo].[destination] ([destination_id], [hosting_id], [start_date], [end_date]) VALUES (57, 3, CAST(N'2020-03-17T16:24:00.000' AS DateTime), CAST(N'2020-03-20T16:24:00.000' AS DateTime))
INSERT [dbo].[destination] ([destination_id], [hosting_id], [start_date], [end_date]) VALUES (58, 3, CAST(N'2020-03-17T16:24:00.000' AS DateTime), CAST(N'2020-03-20T16:24:00.000' AS DateTime))
INSERT [dbo].[destination] ([destination_id], [hosting_id], [start_date], [end_date]) VALUES (59, 3, CAST(N'2020-03-17T16:24:00.000' AS DateTime), CAST(N'2020-03-20T16:24:00.000' AS DateTime))
INSERT [dbo].[destination] ([destination_id], [hosting_id], [start_date], [end_date]) VALUES (60, 1, CAST(N'2020-03-17T16:24:00.000' AS DateTime), CAST(N'2020-03-20T16:24:00.000' AS DateTime))
INSERT [dbo].[destination] ([destination_id], [hosting_id], [start_date], [end_date]) VALUES (61, 3, CAST(N'2020-03-17T16:24:00.000' AS DateTime), CAST(N'2020-03-20T16:24:00.000' AS DateTime))
INSERT [dbo].[destination] ([destination_id], [hosting_id], [start_date], [end_date]) VALUES (62, 1, CAST(N'2020-03-17T16:24:00.000' AS DateTime), CAST(N'2020-03-20T16:24:00.000' AS DateTime))
INSERT [dbo].[destination] ([destination_id], [hosting_id], [start_date], [end_date]) VALUES (63, 3, CAST(N'2020-03-17T16:24:00.000' AS DateTime), CAST(N'2020-03-20T16:24:00.000' AS DateTime))
INSERT [dbo].[destination] ([destination_id], [hosting_id], [start_date], [end_date]) VALUES (64, 1, CAST(N'2020-03-17T16:24:00.000' AS DateTime), CAST(N'2020-03-20T16:24:00.000' AS DateTime))
INSERT [dbo].[destination] ([destination_id], [hosting_id], [start_date], [end_date]) VALUES (65, 1, CAST(N'2020-03-17T16:50:00.000' AS DateTime), CAST(N'2020-03-17T16:50:00.000' AS DateTime))
INSERT [dbo].[destination] ([destination_id], [hosting_id], [start_date], [end_date]) VALUES (66, 3, CAST(N'2020-03-17T16:50:00.000' AS DateTime), CAST(N'2020-03-20T16:50:00.000' AS DateTime))
INSERT [dbo].[destination] ([destination_id], [hosting_id], [start_date], [end_date]) VALUES (67, 1, CAST(N'2020-03-17T16:50:00.000' AS DateTime), CAST(N'2020-03-17T16:50:00.000' AS DateTime))
INSERT [dbo].[destination] ([destination_id], [hosting_id], [start_date], [end_date]) VALUES (68, 3, CAST(N'2020-03-17T16:50:00.000' AS DateTime), CAST(N'2020-03-20T16:50:00.000' AS DateTime))
INSERT [dbo].[destination] ([destination_id], [hosting_id], [start_date], [end_date]) VALUES (69, 1, CAST(N'2020-03-17T16:50:00.000' AS DateTime), CAST(N'2020-03-17T16:50:00.000' AS DateTime))
INSERT [dbo].[destination] ([destination_id], [hosting_id], [start_date], [end_date]) VALUES (70, 3, CAST(N'2020-03-17T16:50:00.000' AS DateTime), CAST(N'2020-03-20T16:50:00.000' AS DateTime))
INSERT [dbo].[destination] ([destination_id], [hosting_id], [start_date], [end_date]) VALUES (71, 1, CAST(N'2020-03-17T16:50:00.000' AS DateTime), CAST(N'2020-03-17T16:50:00.000' AS DateTime))
INSERT [dbo].[destination] ([destination_id], [hosting_id], [start_date], [end_date]) VALUES (72, 3, CAST(N'2020-03-17T16:50:00.000' AS DateTime), CAST(N'2020-03-20T16:50:00.000' AS DateTime))
INSERT [dbo].[destination] ([destination_id], [hosting_id], [start_date], [end_date]) VALUES (73, 1, CAST(N'2020-03-17T16:50:00.000' AS DateTime), CAST(N'2020-03-17T16:50:00.000' AS DateTime))
INSERT [dbo].[destination] ([destination_id], [hosting_id], [start_date], [end_date]) VALUES (74, 3, CAST(N'2020-03-17T16:50:00.000' AS DateTime), CAST(N'2020-03-20T16:50:00.000' AS DateTime))
INSERT [dbo].[destination] ([destination_id], [hosting_id], [start_date], [end_date]) VALUES (75, 1, CAST(N'2020-03-17T16:50:00.000' AS DateTime), CAST(N'2020-03-17T16:50:00.000' AS DateTime))
INSERT [dbo].[destination] ([destination_id], [hosting_id], [start_date], [end_date]) VALUES (76, 3, CAST(N'2020-03-17T16:50:00.000' AS DateTime), CAST(N'2020-03-20T16:50:00.000' AS DateTime))
INSERT [dbo].[destination] ([destination_id], [hosting_id], [start_date], [end_date]) VALUES (77, 1, CAST(N'2020-03-17T16:50:00.000' AS DateTime), CAST(N'2020-03-17T16:50:00.000' AS DateTime))
INSERT [dbo].[destination] ([destination_id], [hosting_id], [start_date], [end_date]) VALUES (78, 3, CAST(N'2020-03-17T16:50:00.000' AS DateTime), CAST(N'2020-03-20T16:50:00.000' AS DateTime))
INSERT [dbo].[destination] ([destination_id], [hosting_id], [start_date], [end_date]) VALUES (79, 3, CAST(N'2020-03-25T00:00:00.000' AS DateTime), CAST(N'2020-03-28T00:00:00.000' AS DateTime))
INSERT [dbo].[destination] ([destination_id], [hosting_id], [start_date], [end_date]) VALUES (80, 3, CAST(N'2020-03-18T00:00:00.000' AS DateTime), CAST(N'2020-03-19T00:00:00.000' AS DateTime))
INSERT [dbo].[destination] ([destination_id], [hosting_id], [start_date], [end_date]) VALUES (81, 1, CAST(N'2020-03-18T00:00:00.000' AS DateTime), CAST(N'2020-03-19T00:00:00.000' AS DateTime))
INSERT [dbo].[destination] ([destination_id], [hosting_id], [start_date], [end_date]) VALUES (82, 2, CAST(N'2020-03-25T00:00:00.000' AS DateTime), CAST(N'2020-03-28T00:00:00.000' AS DateTime))
INSERT [dbo].[destination] ([destination_id], [hosting_id], [start_date], [end_date]) VALUES (83, 1, CAST(N'2020-03-18T00:00:00.000' AS DateTime), CAST(N'2020-03-19T00:00:00.000' AS DateTime))
INSERT [dbo].[destination] ([destination_id], [hosting_id], [start_date], [end_date]) VALUES (84, 2, CAST(N'2020-03-25T00:00:00.000' AS DateTime), CAST(N'2020-03-28T00:00:00.000' AS DateTime))
INSERT [dbo].[destination] ([destination_id], [hosting_id], [start_date], [end_date]) VALUES (103, 1, CAST(N'2020-03-18T00:00:00.000' AS DateTime), CAST(N'2020-03-19T00:00:00.000' AS DateTime))
INSERT [dbo].[destination] ([destination_id], [hosting_id], [start_date], [end_date]) VALUES (104, 2, CAST(N'2020-03-25T00:00:00.000' AS DateTime), CAST(N'2020-03-28T00:00:00.000' AS DateTime))
INSERT [dbo].[destination] ([destination_id], [hosting_id], [start_date], [end_date]) VALUES (107, 1, CAST(N'2020-03-18T00:00:00.000' AS DateTime), CAST(N'2020-03-19T00:00:00.000' AS DateTime))
INSERT [dbo].[destination] ([destination_id], [hosting_id], [start_date], [end_date]) VALUES (108, 2, CAST(N'2020-03-25T00:00:00.000' AS DateTime), CAST(N'2020-03-28T00:00:00.000' AS DateTime))
INSERT [dbo].[destination] ([destination_id], [hosting_id], [start_date], [end_date]) VALUES (123, 1, CAST(N'2020-03-18T00:00:00.000' AS DateTime), CAST(N'2020-03-19T00:00:00.000' AS DateTime))
INSERT [dbo].[destination] ([destination_id], [hosting_id], [start_date], [end_date]) VALUES (124, 2, CAST(N'2020-03-25T00:00:00.000' AS DateTime), CAST(N'2020-03-28T00:00:00.000' AS DateTime))
INSERT [dbo].[destination] ([destination_id], [hosting_id], [start_date], [end_date]) VALUES (125, 1, CAST(N'2020-03-18T00:00:00.000' AS DateTime), CAST(N'2020-03-19T00:00:00.000' AS DateTime))
INSERT [dbo].[destination] ([destination_id], [hosting_id], [start_date], [end_date]) VALUES (126, 2, CAST(N'2020-03-25T00:00:00.000' AS DateTime), CAST(N'2020-03-28T00:00:00.000' AS DateTime))
INSERT [dbo].[destination] ([destination_id], [hosting_id], [start_date], [end_date]) VALUES (128, 1, CAST(N'2020-03-18T00:00:00.000' AS DateTime), CAST(N'2020-03-19T00:00:00.000' AS DateTime))
INSERT [dbo].[destination] ([destination_id], [hosting_id], [start_date], [end_date]) VALUES (129, 2, CAST(N'2020-03-25T00:00:00.000' AS DateTime), CAST(N'2020-03-28T00:00:00.000' AS DateTime))
INSERT [dbo].[destination] ([destination_id], [hosting_id], [start_date], [end_date]) VALUES (130, 2, CAST(N'2020-03-25T00:00:00.000' AS DateTime), CAST(N'2020-03-28T00:00:00.000' AS DateTime))
INSERT [dbo].[destination] ([destination_id], [hosting_id], [start_date], [end_date]) VALUES (131, 1, CAST(N'2020-04-16T00:00:00.000' AS DateTime), CAST(N'2020-04-16T00:00:00.000' AS DateTime))
INSERT [dbo].[destination] ([destination_id], [hosting_id], [start_date], [end_date]) VALUES (132, 1, CAST(N'2020-04-16T00:00:00.000' AS DateTime), CAST(N'2020-04-16T00:00:00.000' AS DateTime))
INSERT [dbo].[destination] ([destination_id], [hosting_id], [start_date], [end_date]) VALUES (133, 1, CAST(N'2020-04-16T00:00:00.000' AS DateTime), CAST(N'2020-04-16T00:00:00.000' AS DateTime))
INSERT [dbo].[destination] ([destination_id], [hosting_id], [start_date], [end_date]) VALUES (134, 1, CAST(N'2020-04-16T00:00:00.000' AS DateTime), CAST(N'2020-04-16T00:00:00.000' AS DateTime))
INSERT [dbo].[destination] ([destination_id], [hosting_id], [start_date], [end_date]) VALUES (137, 1, CAST(N'2020-03-18T00:00:00.000' AS DateTime), CAST(N'2020-03-19T00:00:00.000' AS DateTime))
INSERT [dbo].[destination] ([destination_id], [hosting_id], [start_date], [end_date]) VALUES (138, 2, CAST(N'2020-03-25T00:00:00.000' AS DateTime), CAST(N'2020-03-28T00:00:00.000' AS DateTime))
SET IDENTITY_INSERT [dbo].[destination] OFF
SET IDENTITY_INSERT [dbo].[destination_om] ON 

INSERT [dbo].[destination_om] ([destination_om_id], [destination_id], [om_id]) VALUES (75, 71, 57)
INSERT [dbo].[destination_om] ([destination_om_id], [destination_id], [om_id]) VALUES (76, 72, 57)
INSERT [dbo].[destination_om] ([destination_om_id], [destination_id], [om_id]) VALUES (77, 73, 58)
INSERT [dbo].[destination_om] ([destination_om_id], [destination_id], [om_id]) VALUES (78, 74, 58)
INSERT [dbo].[destination_om] ([destination_om_id], [destination_id], [om_id]) VALUES (79, 75, 59)
INSERT [dbo].[destination_om] ([destination_om_id], [destination_id], [om_id]) VALUES (80, 76, 59)
INSERT [dbo].[destination_om] ([destination_om_id], [destination_id], [om_id]) VALUES (81, 77, 60)
INSERT [dbo].[destination_om] ([destination_om_id], [destination_id], [om_id]) VALUES (82, 78, 60)
INSERT [dbo].[destination_om] ([destination_om_id], [destination_id], [om_id]) VALUES (85, 81, 69)
INSERT [dbo].[destination_om] ([destination_om_id], [destination_id], [om_id]) VALUES (86, 82, 69)
INSERT [dbo].[destination_om] ([destination_om_id], [destination_id], [om_id]) VALUES (87, 83, 78)
INSERT [dbo].[destination_om] ([destination_om_id], [destination_id], [om_id]) VALUES (88, 84, 78)
INSERT [dbo].[destination_om] ([destination_om_id], [destination_id], [om_id]) VALUES (107, 103, 89)
INSERT [dbo].[destination_om] ([destination_om_id], [destination_id], [om_id]) VALUES (108, 104, 89)
INSERT [dbo].[destination_om] ([destination_om_id], [destination_id], [om_id]) VALUES (111, 107, 92)
INSERT [dbo].[destination_om] ([destination_om_id], [destination_id], [om_id]) VALUES (112, 108, 92)
INSERT [dbo].[destination_om] ([destination_om_id], [destination_id], [om_id]) VALUES (127, 123, 100)
INSERT [dbo].[destination_om] ([destination_om_id], [destination_id], [om_id]) VALUES (128, 124, 100)
INSERT [dbo].[destination_om] ([destination_om_id], [destination_id], [om_id]) VALUES (129, 125, 101)
INSERT [dbo].[destination_om] ([destination_om_id], [destination_id], [om_id]) VALUES (130, 126, 101)
INSERT [dbo].[destination_om] ([destination_om_id], [destination_id], [om_id]) VALUES (132, 128, 104)
INSERT [dbo].[destination_om] ([destination_om_id], [destination_id], [om_id]) VALUES (133, 129, 104)
INSERT [dbo].[destination_om] ([destination_om_id], [destination_id], [om_id]) VALUES (134, 130, 105)
INSERT [dbo].[destination_om] ([destination_om_id], [destination_id], [om_id]) VALUES (135, 131, 106)
INSERT [dbo].[destination_om] ([destination_om_id], [destination_id], [om_id]) VALUES (136, 132, 107)
INSERT [dbo].[destination_om] ([destination_om_id], [destination_id], [om_id]) VALUES (137, 133, 108)
INSERT [dbo].[destination_om] ([destination_om_id], [destination_id], [om_id]) VALUES (138, 134, 109)
INSERT [dbo].[destination_om] ([destination_om_id], [destination_id], [om_id]) VALUES (141, 137, 111)
INSERT [dbo].[destination_om] ([destination_om_id], [destination_id], [om_id]) VALUES (142, 138, 111)
SET IDENTITY_INSERT [dbo].[destination_om] OFF
SET IDENTITY_INSERT [dbo].[direction] ON 

INSERT [dbo].[direction] ([direction_id], [direction_name], [direction_budget]) VALUES (1, N'DEC', 200000000)
INSERT [dbo].[direction] ([direction_id], [direction_name], [direction_budget]) VALUES (2, N'DD', 200000000)
INSERT [dbo].[direction] ([direction_id], [direction_name], [direction_budget]) VALUES (3, N'DFASC', 200000000)
INSERT [dbo].[direction] ([direction_id], [direction_name], [direction_budget]) VALUES (4, N'DG', 200000000)
INSERT [dbo].[direction] ([direction_id], [direction_name], [direction_budget]) VALUES (5, N'DID', 200000000)
INSERT [dbo].[direction] ([direction_id], [direction_name], [direction_budget]) VALUES (6, N'DIDR', 200000000)
INSERT [dbo].[direction] ([direction_id], [direction_name], [direction_budget]) VALUES (7, N'DMC', 200000000)
INSERT [dbo].[direction] ([direction_id], [direction_name], [direction_budget]) VALUES (8, N'DOB', 200000000)
INSERT [dbo].[direction] ([direction_id], [direction_name], [direction_budget]) VALUES (9, N'DOM', 200000000)
INSERT [dbo].[direction] ([direction_id], [direction_name], [direction_budget]) VALUES (10, N'DOR', 200000000)
INSERT [dbo].[direction] ([direction_id], [direction_name], [direction_budget]) VALUES (11, N'DRAP', 200000000)
INSERT [dbo].[direction] ([direction_id], [direction_name], [direction_budget]) VALUES (12, N'DRH', 200000000)
INSERT [dbo].[direction] ([direction_id], [direction_name], [direction_budget]) VALUES (13, N'DRI', 200000000)
INSERT [dbo].[direction] ([direction_id], [direction_name], [direction_budget]) VALUES (14, N'DRS', 200000000)
INSERT [dbo].[direction] ([direction_id], [direction_name], [direction_budget]) VALUES (15, N'DSI', 200000000)
INSERT [dbo].[direction] ([direction_id], [direction_name], [direction_budget]) VALUES (16, N'DT', 200000000)
INSERT [dbo].[direction] ([direction_id], [direction_name], [direction_budget]) VALUES (17, N'FONDATION', 200000000)
INSERT [dbo].[direction] ([direction_id], [direction_name], [direction_budget]) VALUES (18, N'INACTIF', 200000000)
INSERT [dbo].[direction] ([direction_id], [direction_name], [direction_budget]) VALUES (19, N'ODC', 200000000)
INSERT [dbo].[direction] ([direction_id], [direction_name], [direction_budget]) VALUES (20, N'SECURITE', 200000000)
INSERT [dbo].[direction] ([direction_id], [direction_name], [direction_budget]) VALUES (21, N'SG', 200000000)
INSERT [dbo].[direction] ([direction_id], [direction_name], [direction_budget]) VALUES (22, N'TEST', 200000000)
SET IDENTITY_INSERT [dbo].[direction] OFF
SET IDENTITY_INSERT [dbo].[driver_ocm] ON 

INSERT [dbo].[driver_ocm] ([driver_ocm_id], [driver_first_name], [driver_last_name], [driver_availability]) VALUES (2, N'KENFACK', N'Jean Jules', 1)
INSERT [dbo].[driver_ocm] ([driver_ocm_id], [driver_first_name], [driver_last_name], [driver_availability]) VALUES (3, N'NDUKONG', N'Jovi', 1)
INSERT [dbo].[driver_ocm] ([driver_ocm_id], [driver_first_name], [driver_last_name], [driver_availability]) VALUES (4, N'ETO''O FILS', N'Samuel', 1)
INSERT [dbo].[driver_ocm] ([driver_ocm_id], [driver_first_name], [driver_last_name], [driver_availability]) VALUES (5, N'STARK', N'Tony', 1)
INSERT [dbo].[driver_ocm] ([driver_ocm_id], [driver_first_name], [driver_last_name], [driver_availability]) VALUES (6, N'UZUMAKI', N'Boruto', 1)
INSERT [dbo].[driver_ocm] ([driver_ocm_id], [driver_first_name], [driver_last_name], [driver_availability]) VALUES (7, N'Transporteur', N'Don', 1)
INSERT [dbo].[driver_ocm] ([driver_ocm_id], [driver_first_name], [driver_last_name], [driver_availability]) VALUES (8, N'Test', N'testor', 1)
SET IDENTITY_INSERT [dbo].[driver_ocm] OFF
SET IDENTITY_INSERT [dbo].[hosting] ON 

INSERT [dbo].[hosting] ([hosting_id], [town_id], [hosting_name], [fees_host], [fees_food]) VALUES (1, 1, N'Hilton', 45000, 10000)
INSERT [dbo].[hosting] ([hosting_id], [town_id], [hosting_name], [fees_host], [fees_food]) VALUES (2, 1, N'Djeuga', 35000, 10000)
INSERT [dbo].[hosting] ([hosting_id], [town_id], [hosting_name], [fees_host], [fees_food]) VALUES (3, 2, N'Akwa Palace', 35000, 10000)
INSERT [dbo].[hosting] ([hosting_id], [town_id], [hosting_name], [fees_host], [fees_food]) VALUES (4, 1, N'Somatel', 20000, 7000)
INSERT [dbo].[hosting] ([hosting_id], [town_id], [hosting_name], [fees_host], [fees_food]) VALUES (5, 1, N'Prestige', 15000, 5000)
INSERT [dbo].[hosting] ([hosting_id], [town_id], [hosting_name], [fees_host], [fees_food]) VALUES (6, 7, N'Seme Beach', 30000, 10000)
INSERT [dbo].[hosting] ([hosting_id], [town_id], [hosting_name], [fees_host], [fees_food]) VALUES (7, 3, N'Michou bar', 25000, 15000)
INSERT [dbo].[hosting] ([hosting_id], [town_id], [hosting_name], [fees_host], [fees_food]) VALUES (8, NULL, NULL, NULL, NULL)
INSERT [dbo].[hosting] ([hosting_id], [town_id], [hosting_name], [fees_host], [fees_food]) VALUES (9, NULL, NULL, NULL, NULL)
INSERT [dbo].[hosting] ([hosting_id], [town_id], [hosting_name], [fees_host], [fees_food]) VALUES (10, 40, N'Hotel Panthere', NULL, NULL)
SET IDENTITY_INSERT [dbo].[hosting] OFF
SET IDENTITY_INSERT [dbo].[om] ON 

INSERT [dbo].[om] ([om_id], [user_id], [om_motive], [om_desc], [om_type_id], [sector_id], [create_date], [bus_fees], [taxi_fees], [train_fees], [mile_allowance], [road_toll], [car_ocm_id], [driver_ocm_id], [allowed_fees], [daily_plan], [end_hour], [total_fees]) VALUES (101, 8, N'xxx', N'xxx', 1, 1, CAST(N'2020-04-15T20:14:25.123' AS DateTime), 50000, 50000, 50000, 7000, 5000, N'CE197YP', 2, NULL, NULL, N'18:00', 0)
INSERT [dbo].[om] ([om_id], [user_id], [om_motive], [om_desc], [om_type_id], [sector_id], [create_date], [bus_fees], [taxi_fees], [train_fees], [mile_allowance], [road_toll], [car_ocm_id], [driver_ocm_id], [allowed_fees], [daily_plan], [end_hour], [total_fees]) VALUES (104, 8, N'test matin', N'xxx', 1, 1, CAST(N'2020-04-16T07:39:40.273' AS DateTime), 50000, 50000, 50000, 7000, 5000, N'CE197YP', 2, NULL, NULL, N'18:00', 0)
INSERT [dbo].[om] ([om_id], [user_id], [om_motive], [om_desc], [om_type_id], [sector_id], [create_date], [bus_fees], [taxi_fees], [train_fees], [mile_allowance], [road_toll], [car_ocm_id], [driver_ocm_id], [allowed_fees], [daily_plan], [end_hour], [total_fees]) VALUES (105, 1, N'test matin', N'xxx', 1, 1, CAST(N'2020-04-16T09:36:54.983' AS DateTime), 50000, 50000, 50000, 7000, 5000, N'CE197YP', 2, 100000, 1, N'18:00', 0)
INSERT [dbo].[om] ([om_id], [user_id], [om_motive], [om_desc], [om_type_id], [sector_id], [create_date], [bus_fees], [taxi_fees], [train_fees], [mile_allowance], [road_toll], [car_ocm_id], [driver_ocm_id], [allowed_fees], [daily_plan], [end_hour], [total_fees]) VALUES (106, 1, N'test final', N'description', 2, 2, CAST(N'2020-04-16T09:45:59.697' AS DateTime), 0, 0, 0, 0, 0, N'CE197YP', 3, NULL, NULL, NULL, 0)
INSERT [dbo].[om] ([om_id], [user_id], [om_motive], [om_desc], [om_type_id], [sector_id], [create_date], [bus_fees], [taxi_fees], [train_fees], [mile_allowance], [road_toll], [car_ocm_id], [driver_ocm_id], [allowed_fees], [daily_plan], [end_hour], [total_fees]) VALUES (107, 1, N'test final', N'description', 2, 2, CAST(N'2020-04-16T09:49:27.647' AS DateTime), 0, 0, 0, 0, 0, N'CE197YP', 3, NULL, NULL, NULL, 0)
INSERT [dbo].[om] ([om_id], [user_id], [om_motive], [om_desc], [om_type_id], [sector_id], [create_date], [bus_fees], [taxi_fees], [train_fees], [mile_allowance], [road_toll], [car_ocm_id], [driver_ocm_id], [allowed_fees], [daily_plan], [end_hour], [total_fees]) VALUES (108, 1, N'test final', N'description', 2, 2, CAST(N'2020-04-16T09:57:11.013' AS DateTime), 0, 0, 0, 0, 0, N'CE197YP', 3, NULL, NULL, NULL, 50000)
INSERT [dbo].[om] ([om_id], [user_id], [om_motive], [om_desc], [om_type_id], [sector_id], [create_date], [bus_fees], [taxi_fees], [train_fees], [mile_allowance], [road_toll], [car_ocm_id], [driver_ocm_id], [allowed_fees], [daily_plan], [end_hour], [total_fees]) VALUES (109, 1, N'test', N'ojouyougio', 1, 1, CAST(N'2020-04-16T10:11:22.163' AS DateTime), 0, 0, 0, 0, 0, N'CE237PJ', 6, 100000, 1, N'12:10', 250000)
INSERT [dbo].[om] ([om_id], [user_id], [om_motive], [om_desc], [om_type_id], [sector_id], [create_date], [bus_fees], [taxi_fees], [train_fees], [mile_allowance], [road_toll], [car_ocm_id], [driver_ocm_id], [allowed_fees], [daily_plan], [end_hour], [total_fees]) VALUES (111, 8, N'test matin', N'xxx', 1, 1, CAST(N'2020-04-16T16:42:43.613' AS DateTime), 50000, 50000, 50000, 7000, 5000, N'CE197YP', 2, NULL, NULL, N'18:00', 150000)
SET IDENTITY_INSERT [dbo].[om] OFF
SET IDENTITY_INSERT [dbo].[om_validator] ON 

INSERT [dbo].[om_validator] ([om_validator_id], [om_id], [user_id], [valid_date], [state_id], [message]) VALUES (6, 101, 8, NULL, NULL, NULL)
INSERT [dbo].[om_validator] ([om_validator_id], [om_id], [user_id], [valid_date], [state_id], [message]) VALUES (7, 104, 8, NULL, NULL, NULL)
INSERT [dbo].[om_validator] ([om_validator_id], [om_id], [user_id], [valid_date], [state_id], [message]) VALUES (8, 105, 8, NULL, NULL, NULL)
INSERT [dbo].[om_validator] ([om_validator_id], [om_id], [user_id], [valid_date], [state_id], [message]) VALUES (9, 106, 8, NULL, NULL, NULL)
INSERT [dbo].[om_validator] ([om_validator_id], [om_id], [user_id], [valid_date], [state_id], [message]) VALUES (10, 107, 8, NULL, NULL, NULL)
INSERT [dbo].[om_validator] ([om_validator_id], [om_id], [user_id], [valid_date], [state_id], [message]) VALUES (11, 108, 8, CAST(N'2020-04-17T18:48:18.360' AS DateTime), 6, NULL)
INSERT [dbo].[om_validator] ([om_validator_id], [om_id], [user_id], [valid_date], [state_id], [message]) VALUES (12, 109, 8, CAST(N'2020-04-17T18:53:21.103' AS DateTime), 6, NULL)
INSERT [dbo].[om_validator] ([om_validator_id], [om_id], [user_id], [valid_date], [state_id], [message]) VALUES (21, 111, 8, CAST(N'2020-04-16T21:18:07.167' AS DateTime), 6, N'Je valide votre mission, en mode PATCH vous pouvez y aller                                                                                                                                                                                                                                                                                                                                                                                                                                                          ')
INSERT [dbo].[om_validator] ([om_validator_id], [om_id], [user_id], [valid_date], [state_id], [message]) VALUES (22, 111, 18, CAST(N'2020-04-16T21:18:08.790' AS DateTime), 6, NULL)
INSERT [dbo].[om_validator] ([om_validator_id], [om_id], [user_id], [valid_date], [state_id], [message]) VALUES (23, 111, 6, CAST(N'2020-04-16T21:18:10.153' AS DateTime), 7, NULL)
INSERT [dbo].[om_validator] ([om_validator_id], [om_id], [user_id], [valid_date], [state_id], [message]) VALUES (24, 108, 18, CAST(N'2020-04-17T18:48:19.573' AS DateTime), 6, NULL)
INSERT [dbo].[om_validator] ([om_validator_id], [om_id], [user_id], [valid_date], [state_id], [message]) VALUES (25, 108, 6, CAST(N'2020-04-17T18:48:20.703' AS DateTime), 7, NULL)
INSERT [dbo].[om_validator] ([om_validator_id], [om_id], [user_id], [valid_date], [state_id], [message]) VALUES (26, 109, 18, CAST(N'2020-04-17T18:53:31.390' AS DateTime), 6, NULL)
INSERT [dbo].[om_validator] ([om_validator_id], [om_id], [user_id], [valid_date], [state_id], [message]) VALUES (27, 109, 6, CAST(N'2020-04-17T18:53:37.350' AS DateTime), 7, NULL)
SET IDENTITY_INSERT [dbo].[om_validator] OFF
SET IDENTITY_INSERT [dbo].[profile] ON 

INSERT [dbo].[profile] ([profile_id], [profile_code], [profile_name]) VALUES (1, N'DT', N'Directeur')
INSERT [dbo].[profile] ([profile_id], [profile_code], [profile_name]) VALUES (2, N'DPT', N'Chef de Département')
INSERT [dbo].[profile] ([profile_id], [profile_code], [profile_name]) VALUES (3, N'SVCE', N'Chef de Service')
INSERT [dbo].[profile] ([profile_id], [profile_code], [profile_name]) VALUES (4, N'USR', N'Utilisateur normal')
SET IDENTITY_INSERT [dbo].[profile] OFF
SET IDENTITY_INSERT [dbo].[role] ON 

INSERT [dbo].[role] ([role_id], [role_name]) VALUES (1, N'Admin')
INSERT [dbo].[role] ([role_id], [role_name]) VALUES (2, N'Validator')
INSERT [dbo].[role] ([role_id], [role_name]) VALUES (3, N'Employee')
SET IDENTITY_INSERT [dbo].[role] OFF
SET IDENTITY_INSERT [dbo].[sector] ON 

INSERT [dbo].[sector] ([sector_id], [sector_name]) VALUES (1, N'IT')
INSERT [dbo].[sector] ([sector_id], [sector_name]) VALUES (2, N'Marketing')
SET IDENTITY_INSERT [dbo].[sector] OFF
SET IDENTITY_INSERT [dbo].[service] ON 

INSERT [dbo].[service] ([service_id], [service_name], [service_budget], [department_id]) VALUES (1, N'Call Center LMT', 1000000, 1)
INSERT [dbo].[service] ([service_id], [service_name], [service_budget], [department_id]) VALUES (2, N'Call Center PCCI', 1000000, 1)
INSERT [dbo].[service] ([service_id], [service_name], [service_budget], [department_id]) VALUES (3, N'INTELCIA', 1000000, 1)
INSERT [dbo].[service] ([service_id], [service_name], [service_budget], [department_id]) VALUES (4, N'BACK OFFICE B2C', 1000000, 2)
INSERT [dbo].[service] ([service_id], [service_name], [service_budget], [department_id]) VALUES (5, N'CARE IN SHOP', 1000000, 2)
INSERT [dbo].[service] ([service_id], [service_name], [service_budget], [department_id]) VALUES (6, N'PRODUCTION', 1000000, 2)
INSERT [dbo].[service] ([service_id], [service_name], [service_budget], [department_id]) VALUES (7, N'BUSINESS OPTIMISATION', 1000000, 3)
INSERT [dbo].[service] ([service_id], [service_name], [service_budget], [department_id]) VALUES (8, N'FIDELISATION ET REF CLIENT', 1000000, 3)
INSERT [dbo].[service] ([service_id], [service_name], [service_budget], [department_id]) VALUES (9, N'GESTION DES GRANDS COMPTES', 1000000, 4)
INSERT [dbo].[service] ([service_id], [service_name], [service_budget], [department_id]) VALUES (10, N'RECOUVREMENT', 1000000, 4)
INSERT [dbo].[service] ([service_id], [service_name], [service_budget], [department_id]) VALUES (11, N'RELATION CLIENT B2B', 1000000, 4)
INSERT [dbo].[service] ([service_id], [service_name], [service_budget], [department_id]) VALUES (12, N'FORMATION ET COMPETENCES', 1000000, 5)
INSERT [dbo].[service] ([service_id], [service_name], [service_budget], [department_id]) VALUES (13, N'METHODE ET CONTROLE QUALITE', 1000000, 5)
INSERT [dbo].[service] ([service_id], [service_name], [service_budget], [department_id]) VALUES (14, N'SUPPORT OPERATIONNEL', 1000000, 5)
INSERT [dbo].[service] ([service_id], [service_name], [service_budget], [department_id]) VALUES (15, N'CANAUX ALTERNATIFS', 1000000, 6)
INSERT [dbo].[service] ([service_id], [service_name], [service_budget], [department_id]) VALUES (16, N'DATA AND DEVICES', 1000000, 6)
INSERT [dbo].[service] ([service_id], [service_name], [service_budget], [department_id]) VALUES (17, N'PROJECT AND TRAINING', 1000000, 6)
INSERT [dbo].[service] ([service_id], [service_name], [service_budget], [department_id]) VALUES (18, N'SHOPS AND FRANCHISES', 1000000, 6)
INSERT [dbo].[service] ([service_id], [service_name], [service_budget], [department_id]) VALUES (19, N'Agence Garoua', 1000000, 7)
INSERT [dbo].[service] ([service_id], [service_name], [service_budget], [department_id]) VALUES (20, N'Agence Kousseri', 1000000, 7)
INSERT [dbo].[service] ([service_id], [service_name], [service_budget], [department_id]) VALUES (21, N'Agence Maroua', 1000000, 7)
INSERT [dbo].[service] ([service_id], [service_name], [service_budget], [department_id]) VALUES (22, N'Agence Ngaoundéré', 1000000, 7)
INSERT [dbo].[service] ([service_id], [service_name], [service_budget], [department_id]) VALUES (23, N'Ventes Distribution AD/NO/EN', 1000000, 7)
INSERT [dbo].[service] ([service_id], [service_name], [service_budget], [department_id]) VALUES (24, N'Ventes Indirectes ADM', 1000000, 7)
INSERT [dbo].[service] ([service_id], [service_name], [service_budget], [department_id]) VALUES (25, N'Ventes Indirectes EN', 1000000, 7)
INSERT [dbo].[service] ([service_id], [service_name], [service_budget], [department_id]) VALUES (26, N'Ventes Indirectes Nord', 1000000, 7)
INSERT [dbo].[service] ([service_id], [service_name], [service_budget], [department_id]) VALUES (27, N'Agence Bastos', 1000000, 8)
INSERT [dbo].[service] ([service_id], [service_name], [service_budget], [department_id]) VALUES (28, N'Agence Bertoua', 1000000, 8)
INSERT [dbo].[service] ([service_id], [service_name], [service_budget], [department_id]) VALUES (29, N'Agence Ebolowa', 1000000, 8)
INSERT [dbo].[service] ([service_id], [service_name], [service_budget], [department_id]) VALUES (30, N'Agence Smart store', 1000000, 8)
INSERT [dbo].[service] ([service_id], [service_name], [service_budget], [department_id]) VALUES (31, N'Agence Yaoundé', 1000000, 8)
INSERT [dbo].[service] ([service_id], [service_name], [service_budget], [department_id]) VALUES (32, N'INDIRECT TRADE Centre/Est', 1000000, 8)
INSERT [dbo].[service] ([service_id], [service_name], [service_budget], [department_id]) VALUES (33, N'REZOM', 1000000, 8)
INSERT [dbo].[service] ([service_id], [service_name], [service_budget], [department_id]) VALUES (34, N'Ventes Distribution CE/ES/SU', 1000000, 8)
INSERT [dbo].[service] ([service_id], [service_name], [service_budget], [department_id]) VALUES (35, N'Ventes Indirectes ES', 1000000, 8)
INSERT [dbo].[service] ([service_id], [service_name], [service_budget], [department_id]) VALUES (36, N'Ventes Indirectes SU', 1000000, 8)
INSERT [dbo].[service] ([service_id], [service_name], [service_budget], [department_id]) VALUES (37, N'VENTES INTERNET Centre/Est', 1000000, 8)
INSERT [dbo].[service] ([service_id], [service_name], [service_budget], [department_id]) VALUES (38, N'Agence Bonapriso', 1000000, 9)
INSERT [dbo].[service] ([service_id], [service_name], [service_budget], [department_id]) VALUES (39, N'Agence Buea', 1000000, 9)
INSERT [dbo].[service] ([service_id], [service_name], [service_budget], [department_id]) VALUES (40, N'Agence de Bonabéri', 1000000, 9)
INSERT [dbo].[service] ([service_id], [service_name], [service_budget], [department_id]) VALUES (41, N'Agence Kumba', 1000000, 9)
INSERT [dbo].[service] ([service_id], [service_name], [service_budget], [department_id]) VALUES (42, N'Agence Soudanaise', 1000000, 9)
INSERT [dbo].[service] ([service_id], [service_name], [service_budget], [department_id]) VALUES (43, N'CORPORATE Littoral/Sud Ouest', 1000000, 9)
INSERT [dbo].[service] ([service_id], [service_name], [service_budget], [department_id]) VALUES (44, N'CVI Littoral 1', 1000000, 9)
INSERT [dbo].[service] ([service_id], [service_name], [service_budget], [department_id]) VALUES (45, N'CVI Littoral 2', 1000000, 9)
INSERT [dbo].[service] ([service_id], [service_name], [service_budget], [department_id]) VALUES (46, N'CVI Sud Ouest', 1000000, 9)
INSERT [dbo].[service] ([service_id], [service_name], [service_budget], [department_id]) VALUES (47, N'INDIRECT TRADE Littoral/Sud Ouest', 1000000, 9)
INSERT [dbo].[service] ([service_id], [service_name], [service_budget], [department_id]) VALUES (48, N'Orange Money Opérationel LT', 1000000, 9)
INSERT [dbo].[service] ([service_id], [service_name], [service_budget], [department_id]) VALUES (49, N'REZOM SW', 1000000, 9)
INSERT [dbo].[service] ([service_id], [service_name], [service_budget], [department_id]) VALUES (50, N'Ventes Distribution LT', 1000000, 9)
INSERT [dbo].[service] ([service_id], [service_name], [service_budget], [department_id]) VALUES (51, N'Ventes Indirectes SW', 1000000, 9)
INSERT [dbo].[service] ([service_id], [service_name], [service_budget], [department_id]) VALUES (52, N'VENTES INTERNET Littoral/Sud Ouest', 1000000, 9)
INSERT [dbo].[service] ([service_id], [service_name], [service_budget], [department_id]) VALUES (53, N'Agence Bafoussam', 1000000, 10)
INSERT [dbo].[service] ([service_id], [service_name], [service_budget], [department_id]) VALUES (54, N'Agence Bamenda', 1000000, 10)
INSERT [dbo].[service] ([service_id], [service_name], [service_budget], [department_id]) VALUES (55, N'Indirect Trade Ouest/Nord Ouest', 1000000, 10)
INSERT [dbo].[service] ([service_id], [service_name], [service_budget], [department_id]) VALUES (56, N'Orange Money ONO', 1000000, 10)
INSERT [dbo].[service] ([service_id], [service_name], [service_budget], [department_id]) VALUES (57, N'Ventes Distribution SW/NW', 1000000, 10)
INSERT [dbo].[service] ([service_id], [service_name], [service_budget], [department_id]) VALUES (58, N'Ventes Indirectes NW', 1000000, 10)
INSERT [dbo].[service] ([service_id], [service_name], [service_budget], [department_id]) VALUES (59, N'Ventes OUEST', 1000000, 10)
INSERT [dbo].[service] ([service_id], [service_name], [service_budget], [department_id]) VALUES (60, N'ACQUISITIONS', 1000000, 11)
INSERT [dbo].[service] ([service_id], [service_name], [service_budget], [department_id]) VALUES (61, N'PRODUITS ET SERVICES', 1000000, 11)
INSERT [dbo].[service] ([service_id], [service_name], [service_budget], [department_id]) VALUES (62, N'TRADE MARKETING', 1000000, 11)
INSERT [dbo].[service] ([service_id], [service_name], [service_budget], [department_id]) VALUES (63, N'CANAL DIRECT', 1000000, 12)
INSERT [dbo].[service] ([service_id], [service_name], [service_budget], [department_id]) VALUES (64, N'CANAL INDIRECT', 1000000, 12)
INSERT [dbo].[service] ([service_id], [service_name], [service_budget], [department_id]) VALUES (65, N'CANAUX ALTERNATIFS', 1000000, 12)
INSERT [dbo].[service] ([service_id], [service_name], [service_budget], [department_id]) VALUES (66, N'DETAILLANT', 1000000, 12)
INSERT [dbo].[service] ([service_id], [service_name], [service_budget], [department_id]) VALUES (67, N'PILOTAGE DU PLAN OPERATIONNEL ET PROJETS', 1000000, 13)
INSERT [dbo].[service] ([service_id], [service_name], [service_budget], [department_id]) VALUES (68, N'REPORTING PERFORMANCE ET ANALYSE DE GESTION', 1000000, 13)
INSERT [dbo].[service] ([service_id], [service_name], [service_budget], [department_id]) VALUES (69, N'FORMATION', 1000000, 14)
INSERT [dbo].[service] ([service_id], [service_name], [service_budget], [department_id]) VALUES (70, N'INSPECTION DES VENTES', 1000000, 14)
INSERT [dbo].[service] ([service_id], [service_name], [service_budget], [department_id]) VALUES (71, N'CONTROLE BUDGETAIRE', 1000000, 15)
INSERT [dbo].[service] ([service_id], [service_name], [service_budget], [department_id]) VALUES (72, N'COMPTA GENE', 1000000, 16)
INSERT [dbo].[service] ([service_id], [service_name], [service_budget], [department_id]) VALUES (73, N'Revenus', 1000000, 16)
INSERT [dbo].[service] ([service_id], [service_name], [service_budget], [department_id]) VALUES (74, N'Achat Domaine IT et N', 1000000, 17)
INSERT [dbo].[service] ([service_id], [service_name], [service_budget], [department_id]) VALUES (75, N'BACK OFFICE ACHAT ET SUPPLY CHAIN', 1000000, 17)
INSERT [dbo].[service] ([service_id], [service_name], [service_budget], [department_id]) VALUES (76, N'LOGISTIC', 1000000, 17)
INSERT [dbo].[service] ([service_id], [service_name], [service_budget], [department_id]) VALUES (77, N'MOYENS GENERAUX', 1000000, 17)
INSERT [dbo].[service] ([service_id], [service_name], [service_budget], [department_id]) VALUES (78, N'PURCHASING', 1000000, 17)
INSERT [dbo].[service] ([service_id], [service_name], [service_budget], [department_id]) VALUES (79, N'REPORTING AND COMPLIANCE', 1000000, 17)
INSERT [dbo].[service] ([service_id], [service_name], [service_budget], [department_id]) VALUES (80, N'SUPPLY CHAIN', 1000000, 17)
INSERT [dbo].[service] ([service_id], [service_name], [service_budget], [department_id]) VALUES (81, N'TRANSPORT', 1000000, 17)
INSERT [dbo].[service] ([service_id], [service_name], [service_budget], [department_id]) VALUES (82, N'DELIVERY RAN', 1000000, 18)
INSERT [dbo].[service] ([service_id], [service_name], [service_budget], [department_id]) VALUES (83, N'OPTIMISATION RAN', 1000000, 18)
INSERT [dbo].[service] ([service_id], [service_name], [service_budget], [department_id]) VALUES (84, N'DEPLOIEMENT', 1000000, 19)
INSERT [dbo].[service] ([service_id], [service_name], [service_budget], [department_id]) VALUES (85, N'DEPLOIEMENT ET ENVIRONNEMENT TECHNIQUE', 1000000, 19)
INSERT [dbo].[service] ([service_id], [service_name], [service_budget], [department_id]) VALUES (86, N'INGENIERIE CORE NETWORK', 1000000, 20)
INSERT [dbo].[service] ([service_id], [service_name], [service_budget], [department_id]) VALUES (87, N'INGENIERIE IP', 1000000, 21)
INSERT [dbo].[service] ([service_id], [service_name], [service_budget], [department_id]) VALUES (88, N'INGENIERIE TRANSMISSIONS', 1000000, 22)
INSERT [dbo].[service] ([service_id], [service_name], [service_budget], [department_id]) VALUES (89, N'GEO MARKETING', 1000000, 23)
INSERT [dbo].[service] ([service_id], [service_name], [service_budget], [department_id]) VALUES (90, N'BRAND EVENTS AND SPONSORING', 1000000, 24)
INSERT [dbo].[service] ([service_id], [service_name], [service_budget], [department_id]) VALUES (91, N'GTM TRADE MARKETING', 1000000, 24)
INSERT [dbo].[service] ([service_id], [service_name], [service_budget], [department_id]) VALUES (92, N'Public Relation', 1000000, 24)
INSERT [dbo].[service] ([service_id], [service_name], [service_budget], [department_id]) VALUES (93, N'DEVICE AND DEMAND', 1000000, 25)
INSERT [dbo].[service] ([service_id], [service_name], [service_budget], [department_id]) VALUES (94, N'DIGITAL AND INNOVATION', 1000000, 25)
INSERT [dbo].[service] ([service_id], [service_name], [service_budget], [department_id]) VALUES (95, N'SEGMENTS', 1000000, 25)
INSERT [dbo].[service] ([service_id], [service_name], [service_budget], [department_id]) VALUES (96, N'CBM', 1000000, 26)
INSERT [dbo].[service] ([service_id], [service_name], [service_budget], [department_id]) VALUES (97, N'PRODUCT MANAGEMENT', 1000000, 26)
INSERT [dbo].[service] ([service_id], [service_name], [service_budget], [department_id]) VALUES (98, N'VAS AND SMS', 1000000, 26)
INSERT [dbo].[service] ([service_id], [service_name], [service_budget], [department_id]) VALUES (99, N'OPERATIONS', 1000000, 27)
GO
INSERT [dbo].[service] ([service_id], [service_name], [service_budget], [department_id]) VALUES (100, N'PM HUAWEI', 1000000, 28)
INSERT [dbo].[service] ([service_id], [service_name], [service_budget], [department_id]) VALUES (101, N'Delivery B2B', 1000000, 29)
INSERT [dbo].[service] ([service_id], [service_name], [service_budget], [department_id]) VALUES (102, N'OPERATIONS', 1000000, 29)
INSERT [dbo].[service] ([service_id], [service_name], [service_budget], [department_id]) VALUES (103, N'SME SOHO', 1000000, 30)
INSERT [dbo].[service] ([service_id], [service_name], [service_budget], [department_id]) VALUES (104, N'SME SOHO CENTRE', 1000000, 30)
INSERT [dbo].[service] ([service_id], [service_name], [service_budget], [department_id]) VALUES (105, N'Pre sales', 1000000, 31)
INSERT [dbo].[service] ([service_id], [service_name], [service_budget], [department_id]) VALUES (106, N'Vente CORPORATE Centre/Est/Sud', 1000000, 31)
INSERT [dbo].[service] ([service_id], [service_name], [service_budget], [department_id]) VALUES (107, N'Vente CORPORATE Littoral', 1000000, 31)
INSERT [dbo].[service] ([service_id], [service_name], [service_budget], [department_id]) VALUES (108, N'Ventes CORPORATE', 1000000, 31)
INSERT [dbo].[service] ([service_id], [service_name], [service_budget], [department_id]) VALUES (109, N'BACK OFFICE CORPORATE', 1000000, 32)
INSERT [dbo].[service] ([service_id], [service_name], [service_budget], [department_id]) VALUES (110, N'ENTREPRISES ONG ET ETAT', 1000000, 32)
INSERT [dbo].[service] ([service_id], [service_name], [service_budget], [department_id]) VALUES (111, N'GESTION DES MARCHANDS', 1000000, 32)
INSERT [dbo].[service] ([service_id], [service_name], [service_budget], [department_id]) VALUES (112, N'MONITORING ET PILOTAGE', 1000000, 33)
INSERT [dbo].[service] ([service_id], [service_name], [service_budget], [department_id]) VALUES (113, N'SUPPORT ANIMATION ET VISIBILITE', 1000000, 33)
INSERT [dbo].[service] ([service_id], [service_name], [service_budget], [department_id]) VALUES (114, N'BACK OFFICE CLIENTS', 1000000, 34)
INSERT [dbo].[service] ([service_id], [service_name], [service_budget], [department_id]) VALUES (115, N'KYA KYC', 1000000, 34)
INSERT [dbo].[service] ([service_id], [service_name], [service_budget], [department_id]) VALUES (116, N'SERVICE CLIENT NIVEAU 3', 1000000, 34)
INSERT [dbo].[service] ([service_id], [service_name], [service_budget], [department_id]) VALUES (117, N'MARKETING OPERATIONNEL', 1000000, 35)
INSERT [dbo].[service] ([service_id], [service_name], [service_budget], [department_id]) VALUES (118, N'MARKETING PRODUIT ET COMMUNICATION', 1000000, 35)
INSERT [dbo].[service] ([service_id], [service_name], [service_budget], [department_id]) VALUES (119, N'PROJET ET INNOVATION', 1000000, 35)
INSERT [dbo].[service] ([service_id], [service_name], [service_budget], [department_id]) VALUES (120, N'PROJETS ET INNOVATION', 1000000, 35)
INSERT [dbo].[service] ([service_id], [service_name], [service_budget], [department_id]) VALUES (121, N'TRADE MARKETING', 1000000, 35)
INSERT [dbo].[service] ([service_id], [service_name], [service_budget], [department_id]) VALUES (122, N'Opérations et Confirmités', 1000000, 36)
INSERT [dbo].[service] ([service_id], [service_name], [service_budget], [department_id]) VALUES (123, N'DATA CENTER ET SITES SPECIAUX', 1000000, 37)
INSERT [dbo].[service] ([service_id], [service_name], [service_budget], [department_id]) VALUES (124, N'Support Environnement Technique', 1000000, 38)
INSERT [dbo].[service] ([service_id], [service_name], [service_budget], [department_id]) VALUES (125, N'Support et Pilotage BSS', 1000000, 38)
INSERT [dbo].[service] ([service_id], [service_name], [service_budget], [department_id]) VALUES (126, N'Support et Pilotage Transmission', 1000000, 38)
INSERT [dbo].[service] ([service_id], [service_name], [service_budget], [department_id]) VALUES (127, N'TMC BSS', 1000000, 38)
INSERT [dbo].[service] ([service_id], [service_name], [service_budget], [department_id]) VALUES (128, N'TMC Core Network IP', 1000000, 38)
INSERT [dbo].[service] ([service_id], [service_name], [service_budget], [department_id]) VALUES (129, N'TMC Environnement Technique', 1000000, 38)
INSERT [dbo].[service] ([service_id], [service_name], [service_budget], [department_id]) VALUES (130, N'AFFAIRES GENERALES', 1000000, 39)
INSERT [dbo].[service] ([service_id], [service_name], [service_budget], [department_id]) VALUES (131, N'COMMUNICATION INTERNE', 1000000, 40)
INSERT [dbo].[service] ([service_id], [service_name], [service_budget], [department_id]) VALUES (132, N'DEVELOPPEMENT CARRIERE', 1000000, 41)
INSERT [dbo].[service] ([service_id], [service_name], [service_budget], [department_id]) VALUES (133, N'EXPERTISE RESEAUX ET SERVICES', 1000000, 42)
INSERT [dbo].[service] ([service_id], [service_name], [service_budget], [department_id]) VALUES (134, N'Management des Services', 1000000, 42)
INSERT [dbo].[service] ([service_id], [service_name], [service_budget], [department_id]) VALUES (135, N'OUTILS QUALITE ET PROCESS', 1000000, 42)
INSERT [dbo].[service] ([service_id], [service_name], [service_budget], [department_id]) VALUES (136, N'Maintenance des Services', 1000000, 43)
INSERT [dbo].[service] ([service_id], [service_name], [service_budget], [department_id]) VALUES (137, N'Maintenance Plateformes SI', 1000000, 43)
INSERT [dbo].[service] ([service_id], [service_name], [service_budget], [department_id]) VALUES (138, N'Maintenance SI Commercial Corporate and BI', 1000000, 43)
INSERT [dbo].[service] ([service_id], [service_name], [service_budget], [department_id]) VALUES (139, N'Maintenance sites strategiques MLL et Tiers', 1000000, 43)
INSERT [dbo].[service] ([service_id], [service_name], [service_budget], [department_id]) VALUES (140, N'MONITORING SERVICES RESEAU ET SI', 1000000, 43)
INSERT [dbo].[service] ([service_id], [service_name], [service_budget], [department_id]) VALUES (141, N'PM ERICSSON', 1000000, 44)
INSERT [dbo].[service] ([service_id], [service_name], [service_budget], [department_id]) VALUES (142, N'PM HUAWEI', 1000000, 44)
INSERT [dbo].[service] ([service_id], [service_name], [service_budget], [department_id]) VALUES (143, N'PMO DSI', 1000000, 45)
INSERT [dbo].[service] ([service_id], [service_name], [service_budget], [department_id]) VALUES (144, N'ARCHITECTURE TECHNIQUE ET LAN', 1000000, 46)
INSERT [dbo].[service] ([service_id], [service_name], [service_budget], [department_id]) VALUES (145, N'SYSTEMES ET BDD', 1000000, 46)
INSERT [dbo].[service] ([service_id], [service_name], [service_budget], [department_id]) VALUES (146, N'PLATEFORME DE SERVICES', 1000000, 47)
INSERT [dbo].[service] ([service_id], [service_name], [service_budget], [department_id]) VALUES (147, N'SOFTWARE FACTORY', 1000000, 47)
INSERT [dbo].[service] ([service_id], [service_name], [service_budget], [department_id]) VALUES (148, N'CANAUX D ACCES COMMANDE ET RELATION CLIENTS', 1000000, 48)
INSERT [dbo].[service] ([service_id], [service_name], [service_budget], [department_id]) VALUES (149, N'DELIVERY FACTURATION RECOUVREMENT ET MEDIATION', 1000000, 48)
INSERT [dbo].[service] ([service_id], [service_name], [service_budget], [department_id]) VALUES (150, N'NOK', 1000000, 49)
INSERT [dbo].[service] ([service_id], [service_name], [service_budget], [department_id]) VALUES (151, N'SECURITE', 1000000, 50)
INSERT [dbo].[service] ([service_id], [service_name], [service_budget], [department_id]) VALUES (152, N'CONTENTIEUX', 1000000, 51)
INSERT [dbo].[service] ([service_id], [service_name], [service_budget], [department_id]) VALUES (153, N'CONTRAT ET SECRETARIAT JURIDIQUE', 1000000, 51)
INSERT [dbo].[service] ([service_id], [service_name], [service_budget], [department_id]) VALUES (154, N'INFORMATION ET DOCUMENTATION', 1000000, 51)
INSERT [dbo].[service] ([service_id], [service_name], [service_budget], [department_id]) VALUES (155, N'SVCE TEST', 1000000, 52)
SET IDENTITY_INSERT [dbo].[service] OFF
SET IDENTITY_INSERT [dbo].[state] ON 

INSERT [dbo].[state] ([state_id], [state_type_id], [om_id], [state_date]) VALUES (42, 1, 57, CAST(N'2020-03-17T15:52:16.267' AS DateTime))
INSERT [dbo].[state] ([state_id], [state_type_id], [om_id], [state_date]) VALUES (43, 1, 58, CAST(N'2020-03-17T15:52:16.847' AS DateTime))
INSERT [dbo].[state] ([state_id], [state_type_id], [om_id], [state_date]) VALUES (44, 1, 59, CAST(N'2020-03-17T15:52:17.607' AS DateTime))
INSERT [dbo].[state] ([state_id], [state_type_id], [om_id], [state_date]) VALUES (45, 1, 60, CAST(N'2020-03-17T15:52:18.227' AS DateTime))
INSERT [dbo].[state] ([state_id], [state_type_id], [om_id], [state_date]) VALUES (47, 1, 69, CAST(N'2020-03-18T14:26:23.567' AS DateTime))
INSERT [dbo].[state] ([state_id], [state_type_id], [om_id], [state_date]) VALUES (48, 1, 78, CAST(N'2020-03-19T09:08:28.627' AS DateTime))
INSERT [dbo].[state] ([state_id], [state_type_id], [om_id], [state_date]) VALUES (57, 2, 89, CAST(N'2020-04-15T19:40:29.793' AS DateTime))
INSERT [dbo].[state] ([state_id], [state_type_id], [om_id], [state_date]) VALUES (58, 2, 92, CAST(N'2020-04-15T19:43:39.947' AS DateTime))
INSERT [dbo].[state] ([state_id], [state_type_id], [om_id], [state_date]) VALUES (66, 2, 100, CAST(N'2020-04-15T20:12:50.197' AS DateTime))
INSERT [dbo].[state] ([state_id], [state_type_id], [om_id], [state_date]) VALUES (67, 2, 101, CAST(N'2020-04-15T20:14:25.743' AS DateTime))
INSERT [dbo].[state] ([state_id], [state_type_id], [om_id], [state_date]) VALUES (68, 2, 104, CAST(N'2020-04-16T07:39:40.303' AS DateTime))
INSERT [dbo].[state] ([state_id], [state_type_id], [om_id], [state_date]) VALUES (69, 2, 105, CAST(N'2020-04-16T09:36:55.227' AS DateTime))
INSERT [dbo].[state] ([state_id], [state_type_id], [om_id], [state_date]) VALUES (70, 2, 106, CAST(N'2020-04-16T09:45:59.790' AS DateTime))
INSERT [dbo].[state] ([state_id], [state_type_id], [om_id], [state_date]) VALUES (71, 2, 107, CAST(N'2020-04-16T09:49:27.850' AS DateTime))
INSERT [dbo].[state] ([state_id], [state_type_id], [om_id], [state_date]) VALUES (72, 2, 108, CAST(N'2020-04-16T10:00:15.867' AS DateTime))
INSERT [dbo].[state] ([state_id], [state_type_id], [om_id], [state_date]) VALUES (73, 2, 109, CAST(N'2020-04-16T10:11:53.657' AS DateTime))
INSERT [dbo].[state] ([state_id], [state_type_id], [om_id], [state_date]) VALUES (95, 6, 111, CAST(N'2020-04-16T19:18:07.000' AS DateTime))
INSERT [dbo].[state] ([state_id], [state_type_id], [om_id], [state_date]) VALUES (96, 6, 111, CAST(N'2020-04-16T19:18:08.727' AS DateTime))
INSERT [dbo].[state] ([state_id], [state_type_id], [om_id], [state_date]) VALUES (97, 6, 111, CAST(N'2020-04-16T19:18:10.137' AS DateTime))
INSERT [dbo].[state] ([state_id], [state_type_id], [om_id], [state_date]) VALUES (98, 7, 111, CAST(N'2020-04-16T19:18:10.167' AS DateTime))
INSERT [dbo].[state] ([state_id], [state_type_id], [om_id], [state_date]) VALUES (99, 6, 109, CAST(N'2020-04-17T16:46:18.687' AS DateTime))
INSERT [dbo].[state] ([state_id], [state_type_id], [om_id], [state_date]) VALUES (101, 6, 108, CAST(N'2020-04-17T16:48:18.307' AS DateTime))
INSERT [dbo].[state] ([state_id], [state_type_id], [om_id], [state_date]) VALUES (102, 6, 108, CAST(N'2020-04-17T16:48:19.567' AS DateTime))
INSERT [dbo].[state] ([state_id], [state_type_id], [om_id], [state_date]) VALUES (103, 6, 108, CAST(N'2020-04-17T16:48:20.693' AS DateTime))
INSERT [dbo].[state] ([state_id], [state_type_id], [om_id], [state_date]) VALUES (104, 7, 108, CAST(N'2020-04-17T16:48:20.707' AS DateTime))
INSERT [dbo].[state] ([state_id], [state_type_id], [om_id], [state_date]) VALUES (105, 6, 109, CAST(N'2020-04-17T16:53:21.000' AS DateTime))
INSERT [dbo].[state] ([state_id], [state_type_id], [om_id], [state_date]) VALUES (106, 6, 109, CAST(N'2020-04-17T16:53:31.377' AS DateTime))
INSERT [dbo].[state] ([state_id], [state_type_id], [om_id], [state_date]) VALUES (107, 6, 109, CAST(N'2020-04-17T16:53:37.337' AS DateTime))
INSERT [dbo].[state] ([state_id], [state_type_id], [om_id], [state_date]) VALUES (108, 7, 109, CAST(N'2020-04-17T16:53:37.353' AS DateTime))
SET IDENTITY_INSERT [dbo].[state] OFF
SET IDENTITY_INSERT [dbo].[state_type] ON 

INSERT [dbo].[state_type] ([state_type_id], [state_type_name]) VALUES (1, N'Created')
INSERT [dbo].[state_type] ([state_type_id], [state_type_name]) VALUES (2, N'Submitted')
INSERT [dbo].[state_type] ([state_type_id], [state_type_name]) VALUES (3, N'Cancelled')
INSERT [dbo].[state_type] ([state_type_id], [state_type_name]) VALUES (4, N'Started')
INSERT [dbo].[state_type] ([state_type_id], [state_type_name]) VALUES (5, N'Rejected')
INSERT [dbo].[state_type] ([state_type_id], [state_type_name]) VALUES (6, N'Validated')
INSERT [dbo].[state_type] ([state_type_id], [state_type_name]) VALUES (7, N'Approved')
SET IDENTITY_INSERT [dbo].[state_type] OFF
SET IDENTITY_INSERT [dbo].[town] ON 

INSERT [dbo].[town] ([town_id], [town_name]) VALUES (1, N'Yaoundé')
INSERT [dbo].[town] ([town_id], [town_name]) VALUES (2, N'Douala')
INSERT [dbo].[town] ([town_id], [town_name]) VALUES (3, N'Bafoussam')
INSERT [dbo].[town] ([town_id], [town_name]) VALUES (4, N'Bertoua')
INSERT [dbo].[town] ([town_id], [town_name]) VALUES (5, N'Maroua')
INSERT [dbo].[town] ([town_id], [town_name]) VALUES (7, N'Limbé')
INSERT [dbo].[town] ([town_id], [town_name]) VALUES (8, N'Batouri')
INSERT [dbo].[town] ([town_id], [town_name]) VALUES (9, N'Garoua')
INSERT [dbo].[town] ([town_id], [town_name]) VALUES (10, NULL)
INSERT [dbo].[town] ([town_id], [town_name]) VALUES (11, NULL)
INSERT [dbo].[town] ([town_id], [town_name]) VALUES (12, NULL)
INSERT [dbo].[town] ([town_id], [town_name]) VALUES (13, NULL)
INSERT [dbo].[town] ([town_id], [town_name]) VALUES (14, NULL)
INSERT [dbo].[town] ([town_id], [town_name]) VALUES (15, NULL)
INSERT [dbo].[town] ([town_id], [town_name]) VALUES (16, NULL)
INSERT [dbo].[town] ([town_id], [town_name]) VALUES (17, NULL)
INSERT [dbo].[town] ([town_id], [town_name]) VALUES (18, NULL)
INSERT [dbo].[town] ([town_id], [town_name]) VALUES (19, NULL)
INSERT [dbo].[town] ([town_id], [town_name]) VALUES (20, NULL)
INSERT [dbo].[town] ([town_id], [town_name]) VALUES (21, NULL)
INSERT [dbo].[town] ([town_id], [town_name]) VALUES (22, NULL)
INSERT [dbo].[town] ([town_id], [town_name]) VALUES (23, NULL)
INSERT [dbo].[town] ([town_id], [town_name]) VALUES (24, NULL)
INSERT [dbo].[town] ([town_id], [town_name]) VALUES (25, NULL)
INSERT [dbo].[town] ([town_id], [town_name]) VALUES (26, NULL)
INSERT [dbo].[town] ([town_id], [town_name]) VALUES (27, NULL)
INSERT [dbo].[town] ([town_id], [town_name]) VALUES (28, NULL)
INSERT [dbo].[town] ([town_id], [town_name]) VALUES (29, NULL)
INSERT [dbo].[town] ([town_id], [town_name]) VALUES (30, NULL)
INSERT [dbo].[town] ([town_id], [town_name]) VALUES (31, NULL)
INSERT [dbo].[town] ([town_id], [town_name]) VALUES (32, NULL)
INSERT [dbo].[town] ([town_id], [town_name]) VALUES (33, NULL)
INSERT [dbo].[town] ([town_id], [town_name]) VALUES (34, NULL)
INSERT [dbo].[town] ([town_id], [town_name]) VALUES (35, NULL)
INSERT [dbo].[town] ([town_id], [town_name]) VALUES (36, NULL)
INSERT [dbo].[town] ([town_id], [town_name]) VALUES (37, NULL)
INSERT [dbo].[town] ([town_id], [town_name]) VALUES (38, NULL)
INSERT [dbo].[town] ([town_id], [town_name]) VALUES (39, N'Buéa')
INSERT [dbo].[town] ([town_id], [town_name]) VALUES (40, N'Belabo')
INSERT [dbo].[town] ([town_id], [town_name]) VALUES (41, N'Belabo')
SET IDENTITY_INSERT [dbo].[town] OFF
SET IDENTITY_INSERT [dbo].[type_om] ON 

INSERT [dbo].[type_om] ([type_om_id], [type_om_name]) VALUES (1, N'Correction')
INSERT [dbo].[type_om] ([type_om_id], [type_om_name]) VALUES (2, N'Déploiement')
INSERT [dbo].[type_om] ([type_om_id], [type_om_name]) VALUES (3, N'Installation')
INSERT [dbo].[type_om] ([type_om_id], [type_om_name]) VALUES (4, N'Maintenance')
SET IDENTITY_INSERT [dbo].[type_om] OFF
SET IDENTITY_INSERT [dbo].[user] ON 

INSERT [dbo].[user] ([user_id], [login], [name], [email], [phone], [is_active], [profile_id], [direction_id], [department_id], [service_id], [birth_date], [role_id]) VALUES (1, N'user', N'monsieur', N'test@orange.com', 690900909, 1, 4, 5, 4, 7, CAST(N'1999-07-19T00:00:00.000' AS DateTime), 3)
INSERT [dbo].[user] ([user_id], [login], [name], [email], [phone], [is_active], [profile_id], [direction_id], [department_id], [service_id], [birth_date], [role_id]) VALUES (6, N'admin', N'admin', N'admin@orange.com', 699999999, 1, 1, 5, 4, 7, CAST(N'2001-05-12T00:00:00.000' AS DateTime), 1)
INSERT [dbo].[user] ([user_id], [login], [name], [email], [phone], [is_active], [profile_id], [direction_id], [department_id], [service_id], [birth_date], [role_id]) VALUES (8, N'JPB2929', N'GADJI Ismael', N'ismael.gadji@orange.com', 699999999, 1, 3, 5, 4, 7, CAST(N'2020-03-13T18:12:07.323' AS DateTime), 2)
INSERT [dbo].[user] ([user_id], [login], [name], [email], [phone], [is_active], [profile_id], [direction_id], [department_id], [service_id], [birth_date], [role_id]) VALUES (18, N'CJRX2535', N'KENGNE KAMDOUM Lareine', N'lareine.kengne@orange.com', 699999999, 1, 2, 5, 4, 7, CAST(N'2020-03-19T11:49:57.340' AS DateTime), 2)
INSERT [dbo].[user] ([user_id], [login], [name], [email], [phone], [is_active], [profile_id], [direction_id], [department_id], [service_id], [birth_date], [role_id]) VALUES (28, N'FLRH1709', N'DELBA GADJI Halimatou', N'delba.halimatou@orange.com', 699949106, 1, 4, 5, 4, 7, CAST(N'2020-03-19T16:06:53.543' AS DateTime), 3)
INSERT [dbo].[user] ([user_id], [login], [name], [email], [phone], [is_active], [profile_id], [direction_id], [department_id], [service_id], [birth_date], [role_id]) VALUES (30, N'JPB2929', N'Ismael GADJI', N'ismael.gadji@orange.com', 699947207, 1, 4, 5, 4, 7, CAST(N'2020-03-19T15:56:59.560' AS DateTime), 3)
SET IDENTITY_INSERT [dbo].[user] OFF
ALTER TABLE [dbo].[om] ADD  CONSTRAINT [DF_om_total_fees]  DEFAULT ((0)) FOR [total_fees]
GO
ALTER TABLE [dbo].[attach_file_om]  WITH NOCHECK ADD  CONSTRAINT [FK_AttachFileOM_attach_file] FOREIGN KEY([attach_file_id])
REFERENCES [dbo].[attach_file] ([attach_file_id])
GO
ALTER TABLE [dbo].[attach_file_om] NOCHECK CONSTRAINT [FK_AttachFileOM_attach_file]
GO
ALTER TABLE [dbo].[attach_file_om]  WITH NOCHECK ADD  CONSTRAINT [FK_AttachFileOM_Om] FOREIGN KEY([om_id])
REFERENCES [dbo].[om] ([om_id])
ON UPDATE CASCADE
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[attach_file_om] NOCHECK CONSTRAINT [FK_AttachFileOM_Om]
GO
ALTER TABLE [dbo].[department]  WITH NOCHECK ADD  CONSTRAINT [FK_department_direction] FOREIGN KEY([direction_id])
REFERENCES [dbo].[direction] ([direction_id])
ON UPDATE CASCADE
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[department] NOCHECK CONSTRAINT [FK_department_direction]
GO
ALTER TABLE [dbo].[destination]  WITH NOCHECK ADD  CONSTRAINT [FK_destination_om_hosting] FOREIGN KEY([hosting_id])
REFERENCES [dbo].[hosting] ([hosting_id])
GO
ALTER TABLE [dbo].[destination] NOCHECK CONSTRAINT [FK_destination_om_hosting]
GO
ALTER TABLE [dbo].[destination_om]  WITH NOCHECK ADD  CONSTRAINT [FK_destination_om_destination] FOREIGN KEY([destination_id])
REFERENCES [dbo].[destination] ([destination_id])
GO
ALTER TABLE [dbo].[destination_om] NOCHECK CONSTRAINT [FK_destination_om_destination]
GO
ALTER TABLE [dbo].[destination_om]  WITH NOCHECK ADD  CONSTRAINT [FK_destination_om_om1] FOREIGN KEY([om_id])
REFERENCES [dbo].[om] ([om_id])
ON UPDATE CASCADE
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[destination_om] NOCHECK CONSTRAINT [FK_destination_om_om1]
GO
ALTER TABLE [dbo].[hosting]  WITH NOCHECK ADD  CONSTRAINT [FK_hosting_town] FOREIGN KEY([town_id])
REFERENCES [dbo].[town] ([town_id])
ON UPDATE CASCADE
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[hosting] NOCHECK CONSTRAINT [FK_hosting_town]
GO
ALTER TABLE [dbo].[om]  WITH NOCHECK ADD  CONSTRAINT [FK_Om_CarOcm] FOREIGN KEY([car_ocm_id])
REFERENCES [dbo].[car_ocm] ([car_ocm_id])
GO
ALTER TABLE [dbo].[om] NOCHECK CONSTRAINT [FK_Om_CarOcm]
GO
ALTER TABLE [dbo].[om]  WITH NOCHECK ADD  CONSTRAINT [FK_Om_DriverOcm] FOREIGN KEY([driver_ocm_id])
REFERENCES [dbo].[driver_ocm] ([driver_ocm_id])
GO
ALTER TABLE [dbo].[om] NOCHECK CONSTRAINT [FK_Om_DriverOcm]
GO
ALTER TABLE [dbo].[om]  WITH NOCHECK ADD  CONSTRAINT [FK_om_user] FOREIGN KEY([user_id])
REFERENCES [dbo].[user] ([user_id])
GO
ALTER TABLE [dbo].[om] NOCHECK CONSTRAINT [FK_om_user]
GO
ALTER TABLE [dbo].[om_comments]  WITH CHECK ADD  CONSTRAINT [FK_om_comments_om] FOREIGN KEY([om_id])
REFERENCES [dbo].[om] ([om_id])
GO
ALTER TABLE [dbo].[om_comments] CHECK CONSTRAINT [FK_om_comments_om]
GO
ALTER TABLE [dbo].[om_validator]  WITH NOCHECK ADD  CONSTRAINT [FK_om_validator_om] FOREIGN KEY([om_id])
REFERENCES [dbo].[om] ([om_id])
GO
ALTER TABLE [dbo].[om_validator] NOCHECK CONSTRAINT [FK_om_validator_om]
GO
ALTER TABLE [dbo].[om_validator]  WITH NOCHECK ADD  CONSTRAINT [FK_om_validator_state_type] FOREIGN KEY([state_id])
REFERENCES [dbo].[state_type] ([state_type_id])
GO
ALTER TABLE [dbo].[om_validator] NOCHECK CONSTRAINT [FK_om_validator_state_type]
GO
ALTER TABLE [dbo].[om_validator]  WITH NOCHECK ADD  CONSTRAINT [FK_om_validator_user] FOREIGN KEY([user_id])
REFERENCES [dbo].[user] ([user_id])
GO
ALTER TABLE [dbo].[om_validator] NOCHECK CONSTRAINT [FK_om_validator_user]
GO
ALTER TABLE [dbo].[service]  WITH NOCHECK ADD  CONSTRAINT [FK_service_department] FOREIGN KEY([department_id])
REFERENCES [dbo].[department] ([department_id])
ON UPDATE CASCADE
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[service] NOCHECK CONSTRAINT [FK_service_department]
GO
ALTER TABLE [dbo].[state]  WITH NOCHECK ADD  CONSTRAINT [FK_State_Om1] FOREIGN KEY([om_id])
REFERENCES [dbo].[om] ([om_id])
ON UPDATE CASCADE
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[state] NOCHECK CONSTRAINT [FK_State_Om1]
GO
ALTER TABLE [dbo].[state]  WITH NOCHECK ADD  CONSTRAINT [FK_State_StateType] FOREIGN KEY([state_type_id])
REFERENCES [dbo].[state_type] ([state_type_id])
GO
ALTER TABLE [dbo].[state] NOCHECK CONSTRAINT [FK_State_StateType]
GO
ALTER TABLE [dbo].[user]  WITH NOCHECK ADD  CONSTRAINT [FK_user_department] FOREIGN KEY([department_id])
REFERENCES [dbo].[department] ([department_id])
GO
ALTER TABLE [dbo].[user] NOCHECK CONSTRAINT [FK_user_department]
GO
ALTER TABLE [dbo].[user]  WITH NOCHECK ADD  CONSTRAINT [FK_user_direction] FOREIGN KEY([direction_id])
REFERENCES [dbo].[direction] ([direction_id])
GO
ALTER TABLE [dbo].[user] NOCHECK CONSTRAINT [FK_user_direction]
GO
ALTER TABLE [dbo].[user]  WITH NOCHECK ADD  CONSTRAINT [FK_user_profile] FOREIGN KEY([profile_id])
REFERENCES [dbo].[profile] ([profile_id])
GO
ALTER TABLE [dbo].[user] NOCHECK CONSTRAINT [FK_user_profile]
GO
ALTER TABLE [dbo].[user]  WITH NOCHECK ADD  CONSTRAINT [FK_user_role] FOREIGN KEY([role_id])
REFERENCES [dbo].[role] ([role_id])
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[user] NOCHECK CONSTRAINT [FK_user_role]
GO
ALTER TABLE [dbo].[user]  WITH NOCHECK ADD  CONSTRAINT [FK_user_service] FOREIGN KEY([service_id])
REFERENCES [dbo].[service] ([service_id])
GO
ALTER TABLE [dbo].[user] NOCHECK CONSTRAINT [FK_user_service]
GO
