﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using GESOM_V2.Models;
using Microsoft.AspNetCore.Authorization;

namespace GESOM_V2.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class CarOcmController : ControllerBase
    {
        private readonly DataContext _context;

        public CarOcmController(DataContext context)
        {
            _context = context;
        }

        // GET: api/CarOcm
        [HttpGet]
        [Authorize]
        public async Task<ActionResult<IEnumerable<CarOcm>>> GetCarOcm()
        {
            return await _context.CarOcm.ToListAsync();
        }

        // GET: api/CarOcm/5
        [HttpGet("{id}")]
        [Authorize]
        public async Task<ActionResult<CarOcm>> GetCarOcm(string id)
        {
            var carOcm = await _context.CarOcm.FindAsync(id);

            if (carOcm == null)
            {
                return NotFound();
            }

            return carOcm;
        }

        // PUT: api/CarOcm/5
        [HttpPut("{id}")]
        [Authorize(Roles="Admin")]
        public async Task<IActionResult> PutCarOcm(string id, CarOcm carOcm)
        {
            if (id != carOcm.CarOcmId)
            {
                return BadRequest();
            }

            _context.Entry(carOcm).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!CarOcmExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return NoContent();
        }

        // POST: api/CarOcm
        [HttpPost]
        [Authorize(Roles ="Admin")]
        public async Task<ActionResult<CarOcm>> PostCarOcm(CarOcm carOcm)
        {
            _context.CarOcm.Add(carOcm);
            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateException)
            {
                if (CarOcmExists(carOcm.CarOcmId))
                {
                    return Conflict();
                }
                else
                {
                    throw;
                }
            }

            return CreatedAtAction("GetCarOcm", new { id = carOcm.CarOcmId }, carOcm);
        }

        // DELETE: api/CarOcm/5
        [HttpDelete("{id}")]
        [Authorize(Roles ="Admin")]
        public async Task<ActionResult<CarOcm>> DeleteCarOcm(string id)
        {
            var carOcm = await _context.CarOcm.FindAsync(id);
            if (carOcm == null)
            {
                return NotFound();
            }

            _context.CarOcm.Remove(carOcm);
            await _context.SaveChangesAsync();

            return carOcm;
        }

        private bool CarOcmExists(string id)
        {
            return _context.CarOcm.Any(e => e.CarOcmId == id);
        }
    }
}
