﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using GESOM_V2.Models;
using Newtonsoft.Json.Linq;
using Microsoft.AspNetCore.JsonPatch;
using Microsoft.AspNetCore.Authorization;

namespace GESOM_V2.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class OmValidatorController : ControllerBase
    {
        private readonly DataContext _context;

        public OmValidatorController(DataContext context)
        {
            _context = context;
        }

        // GET: api/OmValidator
        [HttpGet]
        [Authorize(Roles = "Admin")]
        public async Task<ActionResult<IEnumerable<OmValidator>>> GetOmValidator()
        {
            return await _context.OmValidator.ToListAsync();
        }

        // GET: api/OmValidator/5
        [HttpGet("{id}")]
        [Authorize]
        //public async Task<ActionResult<OmValidator>> GetOmValidator(int id)
        public OmValidator GetOmValidator(int id)
        {
            var omValidator = _context.OmValidator.Where(u => u.OmId == id && u.ValidDate == null).FirstOrDefault();

            if (omValidator == null)
            {
                return null;
            }

            return omValidator;
        }

        [HttpGet("{omid}/messages")]
        [Authorize]
        //public async Task<ActionResult<OmValidator>> GetOmValidator(int id)
        public List<OmValidator> GetOmValidatorsMessages(int omid)
        {
            var omValidator = _context.OmValidator.Where(u => u.OmId == omid).ToList();

            if (omValidator == null)
            {
                return null;
            }

            return omValidator;
        }


        // PUT: api/OmValidator/5
        [HttpPatch("{id}/{userid}")]
        [Authorize]
        public async Task<IActionResult> PutOmValidator(int id, int userid, [FromBody] JObject message)
        {

            var omvalidator = await _context.OmValidator.Where(o => o.OmId == id && o.UserId == userid).FirstOrDefaultAsync();

            omvalidator.Message = (string)message["Message"];

            _context.Entry(omvalidator).Property("Message").IsModified = true;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                
                    throw;
                
            }

            return NoContent();
        }

        // POST: api/OmValidator
        [HttpPost]
        [Authorize]
        public async Task<ActionResult<OmValidator>> PostOmValidator(OmValidator omValidator)
        {
            _context.OmValidator.Add(omValidator);
            await _context.SaveChangesAsync();

            return CreatedAtAction("GetOmValidator", new { id = omValidator.OmValidatorId }, omValidator);
        }

        // DELETE: api/OmValidator/5
        [HttpDelete("{id}")]
        [Authorize]
        public async Task<ActionResult<OmValidator>> DeleteOmValidator(int id)
        {
            var omValidator = await _context.OmValidator.FindAsync(id);
            if (omValidator == null)
            {
                return NotFound();
            }

            _context.OmValidator.Remove(omValidator);
            await _context.SaveChangesAsync();

            return omValidator;
        }

        private bool OmValidatorExists(int id)
        {
            return _context.OmValidator.Any(e => e.OmValidatorId == id);
        }
    }
}
