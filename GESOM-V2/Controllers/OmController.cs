﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using GESOM_V2.Models;
using Newtonsoft.Json.Linq;
using Workflow.service.Controllers;
using GESOM_V2.ModelDTO;
using Microsoft.AspNetCore.Authorization;

namespace GESOM_V2.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class OmController : ControllerBase
    {
        private readonly DataContext _context;

        public OmController(DataContext context)
        {
            _context = context;
        }

        // GET: api/Om
        [HttpGet]
        [Authorize(Roles = "Admin")]
        public async Task<ActionResult<IEnumerable<Om>>> GetOm()
        {
            return await _context.Om.ToListAsync();
        }



        [HttpGet("/tovalidate/{id}")]
        [Authorize(Roles = "Admin,Validator")]
        public ActionResult<JArray> GetOmForValidators(int id)
        {
            if(GetOmForValidator(id) == null)
            {
                return NotFound();
            }

            return Ok(GetOmForValidator(id));

        }

        [HttpGet("/omtovalidate/{id}")]
        [Authorize]
        public JArray GetOmForValidator(int id)
        {
            var omids = _context.OmValidator
                .Where(u => u.UserId == id && u.ValidDate == null)
                .Select(v => v.OmId)
                .ToList();
            var oms = new List<Om>();
            foreach (var omid in omids)
            {
                var om = _context.Om.Find(omid);
                oms.Add(om);
            }

            var rsult = new JArray();
            foreach (Om order in oms)
            {
                OmController omController = new OmController(_context);
                var vs = new List<User>();
                vs = omController.GetValidatorForOm(order.OmId);
                var v = new User();
                var u = new User();
                u = _context.User.Find(order.UserId);
                v = vs[vs.Count - 1];
                UserOm userom = new UserOm
                {
                    omId = order.OmId,
                    omMotive = order.OmMotive,
                    omDesc = order.OmDesc,
                    userName = u.Name,
                    userPhone = u.Phone,
                    userId = order.UserId,
                    createDate = order.CreateDate,
                    validatorName = v.Name,
                    validatorNumber = v.Phone,
                    state = "Validation niveau " + vs.Count
                };
                JObject o = (JObject)JToken.FromObject(userom);
                rsult.Add(o);
            }
            return rsult;
        }

        [HttpGet("/omvalidators/{id}")]
        [Authorize]
        public List<User> GetValidatorForOm(int id)
        {

            var userids = _context.OmValidator
                .Where(u => u.OmId == id && u.ValidDate == null)
                .Select(v => v.UserId)
                .ToList();
            var users = new List<User>();
            foreach (var userid in userids)
            {
                var user = _context.User.Find(userid);
                users.Add(user);
            }
            return users;

        }

        [HttpGet("/allomvalidators/{id}")]
        [Authorize]
        public List<User> GetAllValidatorsForOm(int id)
        {

            var userids = _context.OmValidator
                .Where(u => u.OmId == id)
                .Select(v => v.UserId)
                .ToList();
            var users = new List<User>();
            foreach (var userid in userids)
            {
                var user = _context.User.Find(userid);
                users.Add(user);
            }
            return users;

        }

        private static void ChangePropertiesToLowerCase(JObject jsonObject)
        {
            foreach (var property in jsonObject.Properties().ToList())
            {
                if (property.Value.Type == JTokenType.Object)// replace property names in child object
                    ChangePropertiesToLowerCase((JObject)property.Value);
                property.Replace(new JProperty(Char.ToLowerInvariant(property.Name[0]) + property.Name.Substring(1), property.Value));// properties are read-only, so we have to replace them
            }
        }

        // GET: api/Om/5
        [HttpGet("{id}")]
        [Authorize]
        public ActionResult<AllInfo> GetInfoOM(int id)
        {
            var omInfo = _context.Om.Select(om => new ModelDTO.DetailsOM
            {
                OmId = om.OmId,
                BusFees = om.BusFees,
                OmDesc = om.OmDesc,
                CreateDate = om.CreateDate,

                UserId = om.UserId,
                OmMotive = om.OmMotive,
                OmTypeId = om.OmTypeId,
                SectorId = om.SectorId,
                TaxiFees = om.TaxiFees,
                TrainFees = om.TrainFees,
                MileAllowance = om.MileAllowance,
                RoadToll = om.RoadToll,
                CarOcmId = om.CarOcmId,
                DriverOcmId = om.DriverOcmId,
                EndHour = om.EndHour,
                AllowedFees = om.AllowedFees,
                DailyPlan = om.DailyPlan
            }).FirstOrDefault(om => om.OmId == id);

            var carInfos = _context.CarOcm.Select(car => new ModelDTO.CarOcm
            {
                CarOcmId = car.CarOcmId,
                CarOcmDesc = car.CarOcmDesc
            }).FirstOrDefault(car => car.CarOcmId == omInfo.CarOcmId);

            var driverInfos = _context.DriverOcm.Select(driver => new ModelDTO.DriverOcm
            {
                DriverOcmId = driver.DriverOcmId,
                DriverFirstName = driver.DriverFirstName,
                DriverLastName = driver.DriverLastName,
                DriverAvailability = driver.DriverAvailability
            }).FirstOrDefault(driver => driver.DriverOcmId == omInfo.DriverOcmId);

            var destinationInfo = _context.DestinationOm.Select(dest => new ModelDTO.Destin
            {
                OmId = dest.OmId,
                HostingId = dest.Destination.HostingId,
                TownId = dest.Destination.Hosting.TownId,
                destinationId = dest.DestinationId,
                StartDate = dest.Destination.StartDate,
                EndDate = dest.Destination.EndDate,
                FeesFood = dest.Destination.Hosting.FeesFood,
                FeesHost = dest.Destination.Hosting.FeesHost,
                TownName = dest.Destination.Hosting.Town.TownName,
                HostName = dest.Destination.Hosting.HostingName
            }).Where(dest => dest.OmId == id).ToList();

            
            var states = _context.OmValidator.Select(val => new ModelDTO.Stat
            {
                OmId = val.OmId,
                StateDate = val.ValidDate,
                UserName = val.User.Name,
                UserPhone = val.User.Phone,
                StateName = val.ValidDate == null ? "Non validé" : "Validé"
            }).Where(val => val.OmId == id).ToList();

            return new AllInfo { detailsOM = omInfo, Destinations = destinationInfo, States = states , DriverOcm = driverInfos, CarOcm = carInfos};
        }

        [HttpGet("{id}/{userid}/sold")]
        [Authorize(Roles = "Admin,Validator")]
        public string GetRemainingFounds(int id, int userid)
        {
            var validator = _context.User.Find(userid); // get validator
            var user = _context.Om.Where(ord => ord.OmId == id).Select(o => o.User).First();//get user who created om
            int? total = 0;
            int? result = 0;
            if (user.ServiceId != null) //l'utilisateur appartient à un service
            {
                if(validator.ServiceId == user.ServiceId && validator.ProfileId <= 3) // le validateur est du même service que l'utilisateur
                {
                    var oms = _context.Om
                        //.Include(s => s.State)
                        .Include(u => u.User)
                            .ThenInclude(s => s.Service)
                        .Where(o => o.User.ServiceId == user.ServiceId).ToList();
                    if (oms != null)
                    {
                        foreach(var om in oms)
                        {
                            var omvalid = _context.OmValidator.FirstOrDefault(ov => ov.OmId == om.OmId && ov.StateId == 7);
                            if(omvalid != null)
                            {
                                total = total + om.TotalFees;
                            }
                        }
                        int? budget_service = _context.Service.Where(svc => svc.ServiceId == validator.ServiceId).Select(s => s.ServiceBudget).First();
                        result = budget_service - total;
                    }
                }
                else
                {
                    return ("Vous n'avez pas de visibilité sur les finances de ce service");
                }
            } else if(user.DepartmentId != null)    //l'utilisateur n'a pas de service mais appartient à un département
            {
                if (validator.DepartmentId == user.DepartmentId && validator.ProfileId <= 2) // le validateur est du même département que l'utilisateur
                {
                    var oms = _context.Om.Include(u => u.User).ThenInclude(s => s.Department).Where(o => o.User.DepartmentId == user.DepartmentId).ToList();
                    if (oms != null)
                    {
                        foreach (var om in oms)
                        {
                            var omvalid = _context.OmValidator.Where(ov => ov.OmId == om.OmId && ov.StateId == 7).First();
                            if (omvalid != null)
                            {
                                total = total + om.TotalFees;
                            }
                        }
                        int? budget_dep = _context.Department.Where(svc => svc.DepartmentId == validator.DepartmentId).Select(s => s.DepartmentBudget).First();
                        result = budget_dep - total;
                    }
                }
                else
                {
                    return ("Vous n'avez pas de visibilité sur les finances de ce département");
                }
            }
            else   //l'utilisateur n'a pas de département mais appartient à une direction
            {
                if (validator.DirectionId == user.DirectionId && validator.ProfileId == 1) // le validateur est dans la même direction que l'utilisateur
                {
                    var oms = _context.Om.Include(u => u.User).ThenInclude(s => s.Direction).Where(o => o.User.DirectionId == user.DirectionId).ToList();
                    if (oms != null)
                    {
                        foreach (var om in oms)
                        {
                            var omvalid = _context.OmValidator.Where(ov => ov.OmId == om.OmId && ov.StateId == 7).First();
                            if (omvalid != null)
                            {
                                total = total + om.TotalFees;
                            }
                        }
                        int? budget_dir = _context.Direction.Where(svc => svc.DirectionId == validator.DepartmentId).Select(s => s.DirectionBudget).First();
                        result = budget_dir - total;
                    }
                }
                else
                {
                    return ("Vous n'avez pas de visibilité sur les finances de cette direction");
                }
            }

            return result.ToString();
        }

        // POST: api/Om/updateom/id/statetype
        [HttpPost("updateom/{id}/{statetype}")]
        [Authorize]
        public string ChangeStateOm(int id, int statetype)
        {
            using (var transaction = _context.Database.BeginTransaction())
            {
                using (_context)
                {
                    try
                    {
                        
                        var om = _context.Om.Find(id);
                        var statevalid = _context.State.FirstOrDefault(s => s.StateTypeId == 7 && s.OmId == id);
                        if (om == null)
                        {
                            return "OM not found";
                        }

                        if (statevalid != null) //om déjà validé de bout en bout
                        {
                            return "OM already completely validated";
                        }
                        else
                        {
                            State state = new State()
                            {
                                StateTypeId = statetype,
                                OmId = om.OmId,
                                StateDate = DateTime.UtcNow
                            };
                            _context.State.Add(state);
                            _context.SaveChanges();

                            UserController user_ctrl = new UserController(_context);
                            OmValidatorController ctrl = new OmValidatorController(_context);
                            if (statetype == 6) //validation
                            {
                                //Mise à jour du validateur actuel
                                var current_validator = ctrl.GetOmValidator(id);
                                current_validator.ValidDate = DateTime.Now;
                                _context.Entry(current_validator).Property("ValidDate").IsModified = true;
                                current_validator.StateId = 6;
                                _context.Entry(current_validator).Property("StateId").IsModified = true;
                                _context.SaveChanges();

                                //ajout du validateur suivant
                                var prev_val_id = current_validator.UserId;
                                User prev_val_user = _context.User.Find(prev_val_id);
                                if (prev_val_user.ProfileId == 3)
                                {
                                    var dep_id = prev_val_user.DepartmentId;
                                    var next_val = user_ctrl.GetDepartmentHead(dep_id);

                                    OmValidator validator = new OmValidator()
                                    {
                                        OmId = id,
                                        UserId = next_val.UserId
                                    };
                                    _context.OmValidator.Add(validator);
                                    _context.SaveChanges();
                                }
                                else if (prev_val_user.ProfileId == 2)
                                {
                                    var dir_id = prev_val_user.DirectionId;
                                    var next_val = user_ctrl.GetDirectionHead(dir_id);

                                    OmValidator validator = new OmValidator()
                                    {
                                        OmId = id,
                                        UserId = next_val.UserId
                                    };
                                    _context.OmValidator.Add(validator);
                                    _context.SaveChanges();
                                }
                                else if (prev_val_user.ProfileId == 1)
                                {
                                    current_validator.ValidDate = DateTime.Now;
                                    _context.Entry(current_validator).Property("ValidDate").IsModified = true;
                                    current_validator.StateId = 7;
                                    _context.Entry(current_validator).Property("StateId").IsModified = true;
                                    _context.SaveChanges();

                                    State state_new = new State()
                                    {
                                        StateTypeId = 7,
                                        OmId = om.OmId,
                                        StateDate = DateTime.UtcNow
                                    };
                                    _context.State.Add(state_new);
                                    _context.SaveChanges();
                                }

                            }
                            else if (statetype == 3)    //Suppression
                            {
                                var current_validator = ctrl.GetOmValidator(id);
                                current_validator.UserId = om.UserId;
                                _context.Entry(current_validator).Property("UserId").IsModified = true;
                                current_validator.ValidDate = DateTime.Now;
                                _context.Entry(current_validator).Property("ValidDate").IsModified = true;
                                current_validator.StateId = 3;
                                _context.Entry(current_validator).Property("StateId").IsModified = true;
                                _context.SaveChanges();

                                State state_new = new State()
                                {
                                    StateTypeId = 3,
                                    OmId = om.OmId,
                                    StateDate = DateTime.UtcNow
                                };
                                _context.State.Add(state_new);
                                _context.SaveChanges();
                            }
                            else if (statetype == 5)    //Rejet
                            {
                                var current_validator = ctrl.GetOmValidator(id);
                                current_validator.ValidDate = DateTime.Now;
                                _context.Entry(current_validator).Property("ValidDate").IsModified = true;
                                current_validator.StateId = 5;
                                _context.Entry(current_validator).Property("StateId").IsModified = true;
                                _context.SaveChanges();

                                State state_new = new State()
                                {
                                    StateTypeId = 5,
                                    OmId = om.OmId,
                                    StateDate = DateTime.UtcNow
                                };
                                _context.State.Add(state_new);
                                _context.SaveChanges();
                            }
                            transaction.Commit();
                            return "Okay";
                        }
                        
                        
                    }
                    catch (Exception ex)
                    {
                        transaction.Rollback();
                        return ex.ToString();
                    }

                }
            }
        }

        
        // POST: api/Om/create
        [HttpPost("create")]
        [Authorize]
        public async Task<string> PostOmAsync([FromBody] JObject jObjects)
        {
            using (var transaction = _context.Database.BeginTransaction())
            {
                using (_context)
                {
                    try
                    {
                        var om = jObjects["infoOM"];
                        Om order = new Om()
                        {
                            UserId = (int?)om["UserId"],
                            OmMotive = (string)om["OmMotive"],
                            OmDesc = (string)om["OmDesc"],
                            //OmTypeId = (int?)om["OmTypeId"],
                            //SectorId = (int?)om["SectorId"],
                            CreateDate = DateTime.UtcNow,
                            BusFees = (int?)om["BusFees"],
                            TaxiFees = (int?)om["TaxiFees"],
                            TrainFees = (int?)om["TrainFees"],
                            MileAllowance = (int?)om["MileAllowance"],
                            RoadToll = (int?)om["RoadToll"],
                            CarOcmId = (string)om["CarOcmId"],
                            DriverOcmId = (int?)om["DriverOcmId"],
                            EndHour = (string)om["EndHour"],
                            AllowedFees = (int?)om["allowedFees"],
                            DailyPlan = (bool?)om["dailyPlan"],
                        };

                        if (!om["OmTypeId"].ToString().Equals(""))
                        {
                            order.OmTypeId = (int)om["OmTypeId"];
                        }

                        if (!om["SectorId"].ToString().Equals(""))
                        {
                            order.SectorId = (int)om["SectorId"];
                        }

                        _context.Om.Add(order);
                        _context.SaveChanges();


                        AttachFile file = new AttachFile()
                        {
                            Blob = (string)om["Blob"]
                        };
                        _context.AttachFile.Add(file);
                        _context.SaveChanges();

                        AttachFileOm fileom = new AttachFileOm()
                        {
                            AttachFileId = file.AttachFileId,
                            OmId = order.OmId,
                            UploadDate = DateTime.UtcNow
                        };
                        _context.AttachFileOm.Add(fileom);
                        _context.SaveChanges();

                        var dest = jObjects["destinations"];
                        foreach (JObject json in dest)
                        {
                            Destination destination = new Destination()
                            {
                                HostingId = (int)json["HostingId"],
                                StartDate = (DateTime)json["StartDate"],
                                EndDate = (DateTime)json["EndDate"],
                            };
                            _context.Destination.Add(destination);
                            _context.SaveChanges();

                            DestinationOm destination_om = new DestinationOm()
                            {
                                DestinationId = destination.DestinationId,
                                OmId = order.OmId
                            };
                            _context.DestinationOm.Add(destination_om);
                            _context.SaveChanges();
                        }

                        State state = new State()
                        {
                            StateTypeId = 2,
                            OmId = order.OmId,
                            StateDate = DateTime.UtcNow
                        };
                        _context.State.Add(state);
                        _context.SaveChanges();

                        var user = await _context.User.FindAsync(order.UserId);
                        var valid = new User();
                        valid = await _context.User.Where(u => u.ServiceId == user.ServiceId && u.ProfileId == 3).FirstOrDefaultAsync();
                        if (valid != null)
                        {
                            OmValidator validator = new OmValidator()
                            {
                                OmId = (int?)order.OmId,
                                UserId = (int?)valid.UserId
                            };
                            _context.OmValidator.Add(validator);
                            _context.SaveChanges();
                        }
                        else
                        {
                            valid = await _context.User.Where(u => u.DepartmentId == user.DepartmentId && u.ProfileId == 2).FirstOrDefaultAsync();
                            if(valid != null)
                            {
                                OmValidator validator = new OmValidator()
                                {
                                    OmId = (int?)order.OmId,
                                    UserId = (int?)valid.UserId
                                };
                                _context.OmValidator.Add(validator);
                                _context.SaveChanges();
                            } else
                            {
                                valid = await _context.User.Where(u => u.DirectionId == user.DirectionId && u.ProfileId == 1).FirstOrDefaultAsync();
                                if(valid != null)
                                {
                                    OmValidator validator = new OmValidator()
                                    {
                                        OmId = (int?)order.OmId,
                                        UserId = (int?)valid.UserId
                                    };
                                    _context.OmValidator.Add(validator);
                                    _context.SaveChanges();
                                }
                                else
                                {
                                    throw new Exception();
                                }
                            }
                        }

                        transaction.Commit();

                        return order.ToString();
                    }
                    catch (Exception ex)
                    {
                        transaction.Rollback();
                        return ex.ToString();
                    }

                }
            }
        }

        // PUT: api/Om/5
        [HttpPut("{id}")]
        [Authorize]
        public async Task<string> PutOm(int id, [FromBody]JObject jObjects)
        {
            using (var transaction = _context.Database.BeginTransaction())
            {
                using (_context)
                {
                    try
                    {
                        var om = jObjects["infoOM"];

                        Om order = await _context.Om.FindAsync(id);

                        if (order == null)
                        {
                            return "OM " + id + " not found.";
                        }

                        order.OmMotive = (string)om["OmMotive"];
                        order.OmDesc = (string)om["OmDesc"];
                        order.OmTypeId = (int)om["OmTypeId"];
                        order.SectorId = (int)om["SectorId"];
                        order.BusFees = (int)om["BusFees"];
                        order.TaxiFees = (int)om["TaxiFees"];
                        order.TrainFees = (int)om["TrainFees"];
                        order.MileAllowance = (int)om["MileAllowance"];
                        order.RoadToll = (int)om["RoadToll"];
                        order.CarOcmId = (string)om["CarOcmId"];
                        order.DriverOcmId = (int)om["DriverOcmId"];
                        order.EndHour = (string)om["EndHour"];
                        order.AllowedFees = (int)om["AllowedFees"];
                        order.DailyPlan = (bool)om["DailyPlan"];

                        _context.Entry(order).State = EntityState.Modified;

                        await _context.SaveChangesAsync();

                        var attachoms = await _context.AttachFileOm.Where(d => d.OmId == id).ToListAsync();
                        if (attachoms != null)
                        {
                            foreach (var attachom in attachoms)
                            {
                                AttachFile file = await _context.AttachFile.FindAsync(attachom.AttachFileId);

                                file.Blob = (string)om["Blob"];

                                _context.Entry(file).State = EntityState.Modified;

                                await _context.SaveChangesAsync();
                            }
                        }
                        var dest = jObjects["destinations"];
                        var destoms = await _context.DestinationOm.Where(d => d.OmId == id).ToListAsync();
                        if (destoms != null)
                        {
                            foreach (var destom in destoms)
                            {
                                foreach(var json in dest)
                                {
                                    Destination destination = await _context.Destination.FindAsync(destom.DestinationId);
                                    destination.HostingId = (int)json["HostingId"];
                                    destination.StartDate = (DateTime)json["StartDate"];
                                    destination.EndDate = (DateTime)json["EndDate"];

                                    _context.Entry(destination).State = EntityState.Modified;

                                    await _context.SaveChangesAsync();
                                }
                    
                            }
                        }
                        transaction.Commit();

                        return "Update succeeded!";
                    }
                    catch (Exception ex)
                    {
                        transaction.Rollback();
                        return ex.ToString();
                    }

                }
            }

        }

        // POST: api/Om
        [HttpPost]
        [Authorize]
        public async Task<ActionResult<Om>> PostOm(Om om)
        {
            _context.Om.Add(om);
            await _context.SaveChangesAsync();

            return CreatedAtAction("GetOm", new { id = om.OmId }, om);
        }

        // DELETE: api/Om/5
        [HttpDelete("{id}")]
        [Authorize]
        public async Task<ActionResult<Om>> DeleteOm(int id)
        {
            var om = await _context.Om.FindAsync(id);
            if (om == null)
            {
                return NotFound();
            }

            _context.Om.Remove(om);
            await _context.SaveChangesAsync();

            return om;
        }

        private bool OmExists(int id)
        {
            return _context.Om.Any(e => e.OmId == id);
        }
    }
}
