﻿using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.IO;
using System.Linq;
using System.Net.Http;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;
using GESOM_V2.Controllers;
using GESOM_V2.ModelDTO;
using GESOM_V2.Models;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Cors;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.IdentityModel.Tokens;
using Newtonsoft.Json.Linq;

namespace Workflow.service.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class UserController : ControllerBase
    {
        private readonly DataContext _context;

        public UserController(DataContext context)
        {
            _context = context;
        }

        // GET: api/User
        [HttpGet]
        [Authorize]
        public async Task<ActionResult<IEnumerable<User>>> GetUser()
        {
            return await _context.User.ToListAsync();
        }

        [HttpGet("servicehead/{id}")]
        //public async Task<ActionResult<User>> GetServiceHead(int? id)
        public User GetServiceHead(int? id)
        {
            var user = _context.User.Where(u => u.ServiceId == id && u.ProfileId==3).FirstOrDefault();
            return user;
        }

        [HttpGet("departmenthead/{id}")]
        //public async Task<ActionResult<User>> GetDepartmentHead(int? id)
        public User GetDepartmentHead(int? id)
        {
            var user = _context.User.Where(u => u.DepartmentId == id && u.ProfileId == 2).FirstOrDefault();
            return user;
        }

        [HttpGet("directionhead/{id}")]
        //public async Task<ActionResult<User>> GetDirectionHead(int? id)
        public User GetDirectionHead(int? id)
        {
            var user = _context.User.Where(u => u.DirectionId == id && u.ProfileId == 1).FirstOrDefault();
            return user;
        }

        [HttpGet("{id}/om")]
        [Authorize]
        public ActionResult<JArray> GetAllUserOms(int id)
        {
            return Ok(GetUserOms(id));
        }
        // GET: api/User/5/om
        [HttpGet("{id}/oms")]
        [Authorize]
        public JArray GetUserOms(int id)
        {
            var om = _context.Om
                .Include(order => order.State)
                .ThenInclude(state => state.StateType)
                .Where(user => user.UserId == id).ToList();
            if (om == null)
            {
                return new JArray();
            }

            var rsult = new JArray();
            foreach(Om order in om)
            {
                OmController omController = new OmController(_context);
                var vs = new List<User>();
                vs = omController.GetAllValidatorsForOm(order.OmId);
                
                var v = new User();
                var u = new User();
                u = _context.User.Find(order.UserId);
                try
                {
                    v = vs[vs.Count - 1];
                }
                catch (ArgumentOutOfRangeException)
                {
                    return new JArray();
                }
                
                UserOm userom = new UserOm
                {
                    omId = order.OmId,
                    omMotive = order.OmMotive,
                    omDesc = order.OmDesc,
                    userName = u.Name,
                    userPhone = u.Phone,
                    userId = order.UserId,
                    createDate = order.CreateDate,
                    validatorName = v.Name,
                    validatorNumber = v.Phone,
                    state = "Validation niveau "+vs.Count
                };
                JObject o = (JObject)JToken.FromObject(userom);
                rsult.Add(o);

            }
            return rsult;

        }


        // GET: api/User/jfdh5469


        [HttpGet("{uid}")]
        public async Task<ActionResult<User>> GetUser(string uid)
        {
            string baseUrl = "http://172.21.55.39/uaconsole/index.php/UserAccessConsole/search?cuid=" + uid + "&key=af3baf6222092e4b6ee0a04c1a076b19";

            using (HttpClient client = new HttpClient())
            {
                //In the next using statement you will initiate the Get Request, use the await keyword so it will execute the using statement in order.
                using (HttpResponseMessage res = await client.GetAsync(baseUrl))
                {
                    //Then get the content from the response in the next using statement, then within it you will get the data, and convert it to a c# object.
                    using (HttpContent content = res.Content)
                    {
                        //Now assign your content to your data variable, by converting into a string using the await keyword.
                        var data = await content.ReadAsStringAsync();
                        var dataObj = JObject.Parse(data);
                        //If the data isn't null return log convert the data using newtonsoft JObject Parse class method on the data.
                        if (data != null)
                        {

                            if (!_context.User.Any(u => u.Login == uid))
                            {
                                User user = new User()
                                {
                                    Login = dataObj["8"].ToString(),
                                    Name = dataObj["1"].ToString(),
                                    Email = dataObj["25"].ToString(),
                                    Phone = (int)dataObj["50"],
                                    IsActive = true,
                                    BirthDate = DateTime.Now,
                                };
                                _context.User.Add(user);
                                await _context.SaveChangesAsync();
                                return user;
                            }
                            else
                            {
                                return await _context.User.Where(u => u.Login == uid).FirstOrDefaultAsync();

                            }


                        }
                        else
                        {
                            return NotFound();
                        }

                    }
                }
            }

        }

        // POST: api/User/login
        [HttpPost("login")]
        public string LoginUser(JObject user)
        {

            var exist = _context.User.Any(e => e.Login == user["Login"].ToString());
            if (exist)
            {
                //var url = "http://172.21.55.39/uaconsole/index.php/UserAccessConsole/authentify?cuid=" + user["Login"].ToString() + "&password=" + user["Password"].ToString() + "&key=6834026871de5083fabeb366693a6ec2";
                //using (HttpClient client = new HttpClient())
                //{
                //    using (HttpResponseMessage res = client.GetAsync(url))
                //    {
                //        using (HttpContent content = res.Content)
                //        {
                //            var data = content.ReadAsStringAsync();
                //            if (data == "ACCESS DENIED")
                //            {
                //                var t = JObject.FromObject(new
                //                {
                //                    Message = "Account suspended or password expired",
                //                });
                //                return t.ToString();
                //            }
                //            var dataObj = JObject.Parse(data);
                //            if ((bool)dataObj["AUTH"] == false)
                //            {
                //                var test = JObject.FromObject(new
                //                {
                //                    user = 0,
                //                    toke = 0
                //                });
                //                return test.ToString();
                //            }
                //            else
                //            {
                IdentityOptions _options = new IdentityOptions();
                var usr = _context.User.Where(u => u.Login == user["Login"].ToString()).FirstOrDefault();
                var role = _context.Role.FirstOrDefault(r => r.RoleId == usr.RoleId);
                var tokenDescriptor = new SecurityTokenDescriptor
                {
                    Subject = new ClaimsIdentity(new Claim[]
                    {
                                        new Claim("UserId", usr.UserId.ToString()),
                                        new Claim(_options.ClaimsIdentity.RoleClaimType,role.RoleName.ToString()),
                                        new Claim("Expire", DateTime.UtcNow.AddMinutes(5).ToString())
                                    }),
                                    Expires = DateTime.UtcNow.AddMinutes(15),
                                    SigningCredentials = new SigningCredentials(new SymmetricSecurityKey(Encoding.UTF8.GetBytes("G350M_AP1_F0R_0CM")), SecurityAlgorithms.HmacSha256Signature)
                                };
                                var tokenHandler = new JwtSecurityTokenHandler();
                                var securityToken = tokenHandler.CreateToken(tokenDescriptor);
                                var token = tokenHandler.WriteToken(securityToken);

                                return token.ToString();
                //            }
                //        }
                //    }
                //}
            }
            else
            {
                //var use = GetUser(user["Login"].ToString());
                return Unauthorized().ToString();

            }
        }

        [HttpGet("userprofile")]
        [Authorize]
        public async Task<TokenReturn> GetUserDetails()
        {
            int userId = Int32.Parse(User.Claims.First(c => c.Type == "UserId").Value);
            DateTime expireDate = DateTime.Parse(User.Claims.First(c => c.Type == "Expire").Value);
            string role = User.Claims.First(c => c.Type == ClaimTypes.Role).Value;
            var user = await _context.User.FindAsync(userId);

            var result = new TokenReturn() { User = user, ExpireDate = expireDate};
            return result;
        }

        // PUT: api/User/5
        [HttpPut("{id}")]
        [Authorize]
        public async Task<IActionResult> PutUser(int id, User user)
        {
            if (id != user.UserId)
            {
                return BadRequest();
            }

            _context.Entry(user).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!UserExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return NoContent();
        }

        // POST: api/User
        [HttpPost]
        [Authorize(Roles = "Admin")]
        public async Task<ActionResult<User>> PostUser(User user)
        {
            _context.User.Add(user);
            await _context.SaveChangesAsync();

            return CreatedAtAction("GetUser", new { id = user.UserId }, user);
        }

        // DELETE: api/User/5
        [HttpDelete("{id}")]
        [Authorize(Roles = "Admin")]
        public async Task<ActionResult<User>> DeleteUser(int id)
        {
            var user = await _context.User.FindAsync(id);
            if (user == null)
            {
                return NotFound();
            }

            _context.User.Remove(user);
            await _context.SaveChangesAsync();

            return user;
        }

        private bool UserExists(int id)
        {
            return _context.User.Any(e => e.UserId == id);
        }
    }
}