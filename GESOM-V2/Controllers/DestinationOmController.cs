﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using GESOM_V2.Models;
using Microsoft.AspNetCore.Authorization;

namespace GESOM_V2.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class DestinationOmController : ControllerBase
    {
        private readonly DataContext _context;

        public DestinationOmController(DataContext context)
        {
            _context = context;
        }

        // GET: api/DestinationOm
        [HttpGet]
        [Authorize]
        public async Task<ActionResult<IEnumerable<DestinationOm>>> GetDestinationOm()
        {
            return await _context.DestinationOm.ToListAsync();
        }

        // GET: api/DestinationOm/5
        [HttpGet("{id}")]
        [Authorize]
        public async Task<ActionResult<DestinationOm>> GetDestinationOm(int id)
        {
            var destinationOm = await _context.DestinationOm.FindAsync(id);

            if (destinationOm == null)
            {
                return NotFound();
            }

            return destinationOm;
        }

        // PUT: api/DestinationOm/5
        [HttpPut("{id}")]
        [Authorize]
        public async Task<IActionResult> PutDestinationOm(int id, DestinationOm destinationOm)
        {
            if (id != destinationOm.DestinationOmId)
            {
                return BadRequest();
            }

            _context.Entry(destinationOm).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!DestinationOmExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return NoContent();
        }

        // POST: api/DestinationOm
        [HttpPost]
        [Authorize]
        public async Task<ActionResult<DestinationOm>> PostDestinationOm(DestinationOm destinationOm)
        {
            _context.DestinationOm.Add(destinationOm);
            await _context.SaveChangesAsync();

            return CreatedAtAction("GetDestinationOm", new { id = destinationOm.DestinationOmId }, destinationOm);
        }

        // DELETE: api/DestinationOm/5
        [HttpDelete("{id}")]
        [Authorize]
        public async Task<ActionResult<DestinationOm>> DeleteDestinationOm(int id)
        {
            var destinationOm = await _context.DestinationOm.FindAsync(id);
            if (destinationOm == null)
            {
                return NotFound();
            }

            _context.DestinationOm.Remove(destinationOm);
            await _context.SaveChangesAsync();

            return destinationOm;
        }

        private bool DestinationOmExists(int id)
        {
            return _context.DestinationOm.Any(e => e.DestinationOmId == id);
        }
    }
}
