﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using GESOM_V2.Models;
using Newtonsoft.Json.Linq;
using Microsoft.AspNetCore.Authorization;

namespace GESOM_V2.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class OmCommentController : ControllerBase
    {
        private readonly DataContext _context;

        public OmCommentController(DataContext context)
        {
            _context = context;
        }

        // GET: api/OmComment
        [HttpGet]
        public async Task<ActionResult<IEnumerable<OmComments>>> GetOmComments()
        {
            return await _context.OmComments.ToListAsync();
        }

        // GET: api/OmComment/5
        [HttpGet("{id}")]
        [Authorize]
        public async Task<ActionResult<IEnumerable<OmComments>>> GetOmComments(int id)
        {
            var omComments = await _context.OmComments.Where(o => o.OmId == id).ToListAsync();

            if (omComments == null)
            {
                return NotFound();
            }

            return omComments;
        }

        [HttpPost("{id}")]
        [Authorize]
        public async Task<ActionResult<OmComments>> PostComment(int id, [FromBody] JObject message)
        {
            var om = await _context.Om.FindAsync(id);
            if (om == null)
            {
                return NotFound();
            }
            var current_user_id = Int32.Parse(User.Claims.First(c => c.Type == "UserId").Value);
            //var om_user = om.User;
            var omvalidator = await _context.OmValidator.Where(o => o.OmId == id && o.StateId == null).FirstAsync();

            var comment = new OmComments();
            comment.OmId = id;

            if (om.UserId == current_user_id)
            {
                comment.SenderId = om.UserId;
                comment.ReceiverId = omvalidator.UserId;
                comment.Comment = (string)message["Comment"];
                comment.CommentDate = DateTime.Now;
                _context.Add(comment);

                await _context.SaveChangesAsync();
            }
            else
            {
                comment.SenderId = omvalidator.UserId;
                comment.ReceiverId = om.UserId;
                comment.Comment = (string)message["Comment"];
                comment.CommentDate = DateTime.Now;
                _context.Add(comment);

                await _context.SaveChangesAsync();
            }

            return comment;

        }

        // PUT: api/OmComment/5
        [HttpPut("{id}")]
        public async Task<IActionResult> PutOmComments(int id, OmComments omComments)
        {
            if (id != omComments.OmCommentsId)
            {
                return BadRequest();
            }

            _context.Entry(omComments).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!OmCommentsExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return NoContent();
        }

        // POST: api/OmComment
        [HttpPost]
        public async Task<ActionResult<OmComments>> PostOmComments(OmComments omComments)
        {
            _context.OmComments.Add(omComments);
            await _context.SaveChangesAsync();

            return CreatedAtAction("GetOmComments", new { id = omComments.OmCommentsId }, omComments);
        }

        // DELETE: api/OmComment/5
        [HttpDelete("{id}")]
        public async Task<ActionResult<OmComments>> DeleteOmComments(int id)
        {
            var omComments = await _context.OmComments.FindAsync(id);
            if (omComments == null)
            {
                return NotFound();
            }

            _context.OmComments.Remove(omComments);
            await _context.SaveChangesAsync();

            return omComments;
        }

        private bool OmCommentsExists(int id)
        {
            return _context.OmComments.Any(e => e.OmCommentsId == id);
        }
    }
}
