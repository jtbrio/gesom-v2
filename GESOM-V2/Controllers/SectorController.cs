﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using GESOM_V2.Models;
using Microsoft.AspNetCore.Authorization;

namespace GESOM_V2.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class SectorController : ControllerBase
    {
        private readonly DataContext _context;

        public SectorController(DataContext context)
        {
            _context = context;
        }

        // GET: api/Sector
        [HttpGet]
        [Authorize]
        public async Task<ActionResult<IEnumerable<Sector>>> GetSector()
        {
            return await _context.Sector.ToListAsync();
        }

        // GET: api/Sector/5
        [HttpGet("{id}")]
        [Authorize]
        public async Task<ActionResult<Sector>> GetSector(int id)
        {
            var sector = await _context.Sector.FindAsync(id);

            if (sector == null)
            {
                return NotFound();
            }

            return sector;
        }

        // PUT: api/Sector/5
        [HttpPut("{id}")]
        [Authorize(Roles ="Admin")]
        public async Task<IActionResult> PutSector(int id, Sector sector)
        {
            if (id != sector.SectorId)
            {
                return BadRequest();
            }

            _context.Entry(sector).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!SectorExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return NoContent();
        }

        // POST: api/Sector
        [HttpPost]
        [Authorize(Roles ="Admin")]
        public async Task<ActionResult<Sector>> PostSector(Sector sector)
        {
            _context.Sector.Add(sector);
            await _context.SaveChangesAsync();

            return CreatedAtAction("GetSector", new { id = sector.SectorId }, sector);
        }

        // DELETE: api/Sector/5
        [HttpDelete("{id}")]
        [Authorize(Roles ="Admin")]
        public async Task<ActionResult<Sector>> DeleteSector(int id)
        {
            var sector = await _context.Sector.FindAsync(id);
            if (sector == null)
            {
                return NotFound();
            }

            _context.Sector.Remove(sector);
            await _context.SaveChangesAsync();

            return sector;
        }

        private bool SectorExists(int id)
        {
            return _context.Sector.Any(e => e.SectorId == id);
        }
    }
}
