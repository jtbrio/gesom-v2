﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using GESOM_V2.Models;
using Microsoft.AspNetCore.Authorization;

namespace GESOM_V2.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class DriverOcmController : ControllerBase
    {
        private readonly DataContext _context;

        public DriverOcmController(DataContext context)
        {
            _context = context;
        }

        // GET: api/DriverOcm
        [HttpGet]
        [Authorize]
        public async Task<ActionResult<IEnumerable<DriverOcm>>> GetDriverOcm()
        {
            return await _context.DriverOcm.ToListAsync();
        }

        // GET: api/DriverOcm/5
        [HttpGet("{id}")]
        [Authorize]
        public async Task<ActionResult<DriverOcm>> GetDriverOcm(int id)
        {
            var driverOcm = await _context.DriverOcm.FindAsync(id);

            if (driverOcm == null)
            {
                return NotFound();
            }

            return driverOcm;
        }

        // PUT: api/DriverOcm/5
        [HttpPut("{id}")]
        [Authorize(Roles ="Admin")]
        public async Task<IActionResult> PutDriverOcm(int id, DriverOcm driverOcm)
        {
            if (id != driverOcm.DriverOcmId)
            {
                return BadRequest();
            }

            _context.Entry(driverOcm).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!DriverOcmExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return NoContent();
        }

        // POST: api/DriverOcm
        [HttpPost]
        [Authorize(Roles ="Admin")]
        public async Task<ActionResult<DriverOcm>> PostDriverOcm(DriverOcm driverOcm)
        {
            _context.DriverOcm.Add(driverOcm);
            await _context.SaveChangesAsync();

            return CreatedAtAction("GetDriverOcm", new { id = driverOcm.DriverOcmId }, driverOcm);
        }

        // DELETE: api/DriverOcm/5
        [HttpDelete("{id}")]
        [Authorize(Roles ="Admin")]
        public async Task<ActionResult<DriverOcm>> DeleteDriverOcm(int id)
        {
            var driverOcm = await _context.DriverOcm.FindAsync(id);
            if (driverOcm == null)
            {
                return NotFound();
            }

            _context.DriverOcm.Remove(driverOcm);
            await _context.SaveChangesAsync();

            return driverOcm;
        }

        private bool DriverOcmExists(int id)
        {
            return _context.DriverOcm.Any(e => e.DriverOcmId == id);
        }
    }
}
