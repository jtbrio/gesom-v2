﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using GESOM_V2.Models;
using Microsoft.AspNetCore.Authorization;

namespace GESOM_V2.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class TypeOmController : ControllerBase
    {
        private readonly DataContext _context;

        public TypeOmController(DataContext context)
        {
            _context = context;
        }

        // GET: api/TypeOm
        [HttpGet]
        [Authorize]
        public async Task<ActionResult<IEnumerable<TypeOm>>> GetTypeOm()
        {
            return await _context.TypeOm.ToListAsync();
        }

        // GET: api/TypeOm/5
        [HttpGet("{id}")]
        [Authorize]
        public async Task<ActionResult<TypeOm>> GetTypeOm(int id)
        {
            var typeOm = await _context.TypeOm.FindAsync(id);

            if (typeOm == null)
            {
                return NotFound();
            }

            return typeOm;
        }

        // PUT: api/TypeOm/5
        [HttpPut("{id}")]
        [Authorize(Roles ="Admin")]
        public async Task<IActionResult> PutTypeOm(int id, TypeOm typeOm)
        {
            if (id != typeOm.TypeOmId)
            {
                return BadRequest();
            }

            _context.Entry(typeOm).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!TypeOmExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return NoContent();
        }

        // POST: api/TypeOm
        [HttpPost]
        [Authorize(Roles ="Admin")]
        public async Task<ActionResult<TypeOm>> PostTypeOm(TypeOm typeOm)
        {
            _context.TypeOm.Add(typeOm);
            await _context.SaveChangesAsync();

            return CreatedAtAction("GetTypeOm", new { id = typeOm.TypeOmId }, typeOm);
        }

        // DELETE: api/TypeOm/5
        [HttpDelete("{id}")]
        [Authorize(Roles ="Admin")]
        public async Task<ActionResult<TypeOm>> DeleteTypeOm(int id)
        {
            var typeOm = await _context.TypeOm.FindAsync(id);
            if (typeOm == null)
            {
                return NotFound();
            }

            _context.TypeOm.Remove(typeOm);
            await _context.SaveChangesAsync();

            return typeOm;
        }

        private bool TypeOmExists(int id)
        {
            return _context.TypeOm.Any(e => e.TypeOmId == id);
        }
    }
}
