﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using GESOM_V2.Models;
using Microsoft.AspNetCore.Authorization;

namespace GESOM_V2.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class TownController : ControllerBase
    {
        private readonly DataContext _context;

        public TownController(DataContext context)
        {
            _context = context;
        }

        // GET: api/Town
        [HttpGet]
        [Authorize]
        public async Task<ActionResult<IEnumerable<Town>>> GetTown()
        {
            return await _context.Town.ToListAsync();
        }

        // GET: api/Town/5
        [HttpGet("{id}")]
        [Authorize]
        public async Task<ActionResult<Town>> GetTown(int id)
        {
            var town = await _context.Town.FindAsync(id);

            if (town == null)
            {
                return NotFound();
            }

            return town;
        }

        // GET: api/Town/5/hostings
        [HttpGet("{id}/hostings")]
        [Authorize]
        public async Task<ActionResult<Town>> GetTownHostings(int id)
        {
            var town = await _context.Town.Include(t => t.Hosting).Where(t => t.TownId == id).FirstOrDefaultAsync();
            return town;
        }

        // PUT: api/Town/5
        [HttpPut("{id}")]
        [Authorize(Roles ="Admin")]
        public async Task<IActionResult> PutTown(int id, Town town)
        {
            if (id != town.TownId)
            {
                return BadRequest();
            }

            _context.Entry(town).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!TownExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return NoContent();
        }

        // POST: api/Town
        [HttpPost]
        [Authorize(Roles ="Admin")]
        public async Task<ActionResult<Town>> PostTown(Town town)
        {
            _context.Town.Add(town);
            await _context.SaveChangesAsync();

            return CreatedAtAction("GetTown", new { id = town.TownId }, town);
        }

        // DELETE: api/Town/5
        [HttpDelete("{id}")]
        [Authorize(Roles ="Admin")]
        public async Task<ActionResult<Town>> DeleteTown(int id)
        {
            var town = await _context.Town.FindAsync(id);
            if (town == null)
            {
                return NotFound();
            }

            _context.Town.Remove(town);
            await _context.SaveChangesAsync();

            return town;
        }

        private bool TownExists(int id)
        {
            return _context.Town.Any(e => e.TownId == id);
        }
    }
}
