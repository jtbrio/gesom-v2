﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using GESOM_V2.Models;
using Microsoft.AspNetCore.Authorization;

namespace GESOM_V2.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class AttachFileController : ControllerBase
    {
        private readonly DataContext _context;

        public AttachFileController(DataContext context)
        {
            _context = context;
        }

        // GET: api/AttachFile
        [HttpGet]
        [Authorize]
        public async Task<ActionResult<IEnumerable<AttachFile>>> GetAttachFile()
        {
            return await _context.AttachFile.ToListAsync();
        }

        // GET: api/AttachFile/5
        [HttpGet("{id}")]
        [Authorize]
        public async Task<ActionResult<AttachFile>> GetAttachFile(int id)
        {
            var attachFile = await _context.AttachFile.FindAsync(id);

            if (attachFile == null)
            {
                return NotFound();
            }

            return attachFile;
        }

        // PUT: api/AttachFile/5
        [HttpPut("{id}")]
        [Authorize]
        public async Task<IActionResult> PutAttachFile(int id, AttachFile attachFile)
        {
            if (id != attachFile.AttachFileId)
            {
                return BadRequest();
            }

            _context.Entry(attachFile).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!AttachFileExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return NoContent();
        }

        // POST: api/AttachFile
        [HttpPost]
        [Authorize]
        public async Task<ActionResult<AttachFile>> PostAttachFile(AttachFile attachFile)
        {
            _context.AttachFile.Add(attachFile);
            await _context.SaveChangesAsync();

            return CreatedAtAction("GetAttachFile", new { id = attachFile.AttachFileId }, attachFile);
        }

        // DELETE: api/AttachFile/5
        [HttpDelete("{id}")]
        [Authorize]
        public async Task<ActionResult<AttachFile>> DeleteAttachFile(int id)
        {
            var attachFile = await _context.AttachFile.FindAsync(id);
            if (attachFile == null)
            {
                return NotFound();
            }

            _context.AttachFile.Remove(attachFile);
            await _context.SaveChangesAsync();

            return attachFile;
        }

        private bool AttachFileExists(int id)
        {
            return _context.AttachFile.Any(e => e.AttachFileId == id);
        }
    }
}
