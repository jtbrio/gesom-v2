﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using GESOM_V2.Models;
using Microsoft.AspNetCore.Authorization;

namespace GESOM_V2.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class DirectionController : ControllerBase
    {
        private readonly DataContext _context;

        public DirectionController(DataContext context)
        {
            _context = context;
        }

        // GET: api/Direction
        [HttpGet]
        [Authorize]
        public async Task<ActionResult<IEnumerable<Direction>>> GetDirection()
        {
            return await _context.Direction.Include(d => d.Department).ThenInclude(d => d.Service).ToListAsync();
        }

        // GET: api/Direction/5
        [HttpGet("{id}")]
        [Authorize]
        public async Task<ActionResult<Direction>> GetDirection(int id)
        {
            var direction = await _context.Direction.Include(d => d.Department).ThenInclude(d => d.Service).Where(d => d.DirectionId == id).FirstOrDefaultAsync();

            if (direction == null)
            {
                return NotFound();
            }

            return direction;
        }

        // PUT: api/Direction/5
        [HttpPut("{id}")]
        [Authorize(Roles ="Admin")]
        public async Task<IActionResult> PutDirection(int id, Direction direction)
        {
            if (id != direction.DirectionId)
            {
                return BadRequest();
            }

            _context.Entry(direction).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!DirectionExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return NoContent();
        }

        // POST: api/Direction
        [HttpPost]
        [Authorize(Roles ="Admin")]
        public async Task<ActionResult<Direction>> PostDirection(Direction direction)
        {
            _context.Direction.Add(direction);
            await _context.SaveChangesAsync();

            return CreatedAtAction("GetDirection", new { id = direction.DirectionId }, direction);
        }

        // DELETE: api/Direction/5
        [HttpDelete("{id}")]
        [Authorize(Roles ="Admin")]
        public async Task<ActionResult<Direction>> DeleteDirection(int id)
        {
            var direction = await _context.Direction.FindAsync(id);
            if (direction == null)
            {
                return NotFound();
            }

            _context.Direction.Remove(direction);
            await _context.SaveChangesAsync();

            return direction;
        }

        private bool DirectionExists(int id)
        {
            return _context.Direction.Any(e => e.DirectionId == id);
        }
    }
}
