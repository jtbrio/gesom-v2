﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using GESOM_V2.Models;
using Microsoft.AspNetCore.Authorization;

namespace GESOM_V2.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class AttachFileOmController : ControllerBase
    {
        private readonly DataContext _context;

        public AttachFileOmController(DataContext context)
        {
            _context = context;
        }

        // GET: api/AttachFileOm
        [HttpGet]
        [Authorize]
        public async Task<ActionResult<IEnumerable<AttachFileOm>>> GetAttachFileOm()
        {
            return await _context.AttachFileOm.ToListAsync();
        }

        // GET: api/AttachFileOm/5
        [HttpGet("{id}")]
        [Authorize]
        public async Task<ActionResult<AttachFileOm>> GetAttachFileOm(int id)
        {
            var attachFileOm = await _context.AttachFileOm.FindAsync(id);

            if (attachFileOm == null)
            {
                return NotFound();
            }

            return attachFileOm;
        }

        // PUT: api/AttachFileOm/5
        [HttpPut("{id}")]
        [Authorize]
        public async Task<IActionResult> PutAttachFileOm(int id, AttachFileOm attachFileOm)
        {
            if (id != attachFileOm.AttachFileOmId)
            {
                return BadRequest();
            }

            _context.Entry(attachFileOm).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!AttachFileOmExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return NoContent();
        }

        // POST: api/AttachFileOm
        [HttpPost]
        [Authorize]
        public async Task<ActionResult<AttachFileOm>> PostAttachFileOm(AttachFileOm attachFileOm)
        {
            _context.AttachFileOm.Add(attachFileOm);
            await _context.SaveChangesAsync();

            return CreatedAtAction("GetAttachFileOm", new { id = attachFileOm.AttachFileOmId }, attachFileOm);
        }

        // DELETE: api/AttachFileOm/5
        [HttpDelete("{id}")]
        [Authorize]
        public async Task<ActionResult<AttachFileOm>> DeleteAttachFileOm(int id)
        {
            var attachFileOm = await _context.AttachFileOm.FindAsync(id);
            if (attachFileOm == null)
            {
                return NotFound();
            }

            _context.AttachFileOm.Remove(attachFileOm);
            await _context.SaveChangesAsync();

            return attachFileOm;
        }

        private bool AttachFileOmExists(int id)
        {
            return _context.AttachFileOm.Any(e => e.AttachFileOmId == id);
        }
    }
}
