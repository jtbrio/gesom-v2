﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using GESOM_V2.Models;
using Microsoft.AspNetCore.Authorization;

namespace GESOM_V2.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class HostingController : ControllerBase
    {
        private readonly DataContext _context;

        public HostingController(DataContext context)
        {
            _context = context;
        }

        // GET: api/Hosting
        [HttpGet]
        [Authorize]
        public async Task<ActionResult<IEnumerable<Hosting>>> GetHosting()
        {
            return await _context.Hosting.ToListAsync();
        }

        // GET: api/Hosting/5
        [HttpGet("{id}")]
        [Authorize]
        public async Task<ActionResult<Hosting>> GetHosting(int id)
        {
            var hosting = await _context.Hosting.FindAsync(id);

            if (hosting == null)
            {
                return NotFound();
            }

            return hosting;
        }

        // PUT: api/Hosting/5
        [HttpPut("{id}")]
        [Authorize(Roles ="Admin")]
        public async Task<IActionResult> PutHosting(int id, Hosting hosting)
        {
            if (id != hosting.HostingId)
            {
                return BadRequest();
            }

            _context.Entry(hosting).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!HostingExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return NoContent();
        }

        // POST: api/Hosting
        [HttpPost]
        [Authorize(Roles ="Admin")]
        public async Task<ActionResult<Hosting>> PostHosting(Hosting hosting)
        {
            _context.Hosting.Add(hosting);
            await _context.SaveChangesAsync();

            return CreatedAtAction("GetHosting", new { id = hosting.HostingId }, hosting);
        }

        // DELETE: api/Hosting/5
        [HttpDelete("{id}")]
        [Authorize(Roles ="Admin")]
        public async Task<ActionResult<Hosting>> DeleteHosting(int id)
        {
            var hosting = await _context.Hosting.FindAsync(id);
            if (hosting == null)
            {
                return NotFound();
            }

            _context.Hosting.Remove(hosting);
            await _context.SaveChangesAsync();

            return hosting;
        }

        private bool HostingExists(int id)
        {
            return _context.Hosting.Any(e => e.HostingId == id);
        }
    }
}
