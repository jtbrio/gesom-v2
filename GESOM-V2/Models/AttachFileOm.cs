﻿using System;
using System.Collections.Generic;

namespace GESOM_V2.Models
{
    public partial class AttachFileOm
    {
        public int AttachFileOmId { get; set; }
        public int AttachFileId { get; set; }
        public int OmId { get; set; }
        public DateTime? UploadDate { get; set; }

        public virtual AttachFile AttachFile { get; set; }
        public virtual Om Om { get; set; }
    }
}
