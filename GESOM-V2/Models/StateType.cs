﻿using System;
using System.Collections.Generic;

namespace GESOM_V2.Models
{
    public partial class StateType
    {
        public StateType()
        {
            OmValidator = new HashSet<OmValidator>();
            State = new HashSet<State>();
        }

        public int StateTypeId { get; set; }
        public string StateTypeName { get; set; }

        public virtual ICollection<OmValidator> OmValidator { get; set; }
        public virtual ICollection<State> State { get; set; }
    }
}
