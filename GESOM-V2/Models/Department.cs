﻿using System;
using System.Collections.Generic;

namespace GESOM_V2.Models
{
    public partial class Department
    {
        public Department()
        {
            Service = new HashSet<Service>();
            User = new HashSet<User>();
        }

        public int DepartmentId { get; set; }
        public string DepartmentName { get; set; }
        public int? DepartmentBudget { get; set; }
        public int? DirectionId { get; set; }

        public virtual Direction Direction { get; set; }
        public virtual ICollection<Service> Service { get; set; }
        public virtual ICollection<User> User { get; set; }
    }
}
