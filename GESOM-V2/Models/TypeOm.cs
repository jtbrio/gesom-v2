﻿using System;
using System.Collections.Generic;

namespace GESOM_V2.Models
{
    public partial class TypeOm
    {
        public int TypeOmId { get; set; }
        public string TypeOmName { get; set; }
    }
}
