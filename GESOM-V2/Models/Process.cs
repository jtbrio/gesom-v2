﻿using System;
using System.Collections.Generic;

namespace GESOM_V2.Models
{
    public partial class Process
    {
        public Process()
        {
            ProcessAdmin = new HashSet<ProcessAdmin>();
            Transition = new HashSet<Transition>();
        }

        public int ProcessId { get; set; }
        public string Name { get; set; }

        public virtual ICollection<ProcessAdmin> ProcessAdmin { get; set; }
        public virtual ICollection<Transition> Transition { get; set; }
    }
}
