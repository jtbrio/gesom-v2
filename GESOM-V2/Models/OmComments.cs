﻿using System;
using System.Collections.Generic;

namespace GESOM_V2.Models
{
    public partial class OmComments
    {
        public int OmCommentsId { get; set; }
        public int? OmId { get; set; }
        public int? SenderId { get; set; }
        public int? ReceiverId { get; set; }
        public string Comment { get; set; }
        public DateTime? CommentDate { get; set; }

        public virtual Om Om { get; set; }
    }
}
