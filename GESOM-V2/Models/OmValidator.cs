﻿using System;
using System.Collections.Generic;

namespace GESOM_V2.Models
{
    public partial class OmValidator
    {
        public int OmValidatorId { get; set; }
        public int? OmId { get; set; }
        public int? UserId { get; set; }
        public DateTime? ValidDate { get; set; }
        public int? StateId { get; set; }
        public string Message { get; set; }

        public virtual Om Om { get; set; }
        public virtual StateType State { get; set; }
        public virtual User User { get; set; }
    }
}
