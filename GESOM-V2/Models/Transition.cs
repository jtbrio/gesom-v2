﻿using System;
using System.Collections.Generic;

namespace GESOM_V2.Models
{
    public partial class Transition
    {
        public int TransitionId { get; set; }
        public int ProcessId { get; set; }
        public int TransitionTypeId { get; set; }
        public int CurrentStateId { get; set; }
        public int NextStateId { get; set; }
        public DateTime? TransitionDate { get; set; }

        public virtual Process Process { get; set; }
        public virtual TransitionType TransitionType { get; set; }
    }
}
