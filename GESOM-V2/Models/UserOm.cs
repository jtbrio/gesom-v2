﻿using System;
using System.Collections.Generic;

namespace GESOM_V2.Models
{
    public partial class UserOm
    {
        public UserOm()
        {
            
        }

        public int omId { get; set; }
        public string omMotive { get; set; }
        public string omDesc { get; set; }
        public string userName { get; set; }
        public int? userPhone { get; set; }
        public int? userId { get; set; }
        public DateTime? createDate { get; set; }
        public string validatorName { get; set; }
        public int? validatorNumber { get; set; }
        public string state { get; set; }
    }
}
