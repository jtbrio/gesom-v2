﻿using System;
using System.Collections.Generic;

namespace GESOM_V2.Models
{
    public partial class Service
    {
        public Service()
        {
            User = new HashSet<User>();
        }

        public int ServiceId { get; set; }
        public string ServiceName { get; set; }
        public int? ServiceBudget { get; set; }
        public int? DepartmentId { get; set; }

        public virtual Department Department { get; set; }
        public virtual ICollection<User> User { get; set; }
    }
}
