﻿using System;
using System.Collections.Generic;

namespace GESOM_V2.Models
{
    public partial class AttachFile
    {
        public AttachFile()
        {
            AttachFileOm = new HashSet<AttachFileOm>();
        }

        public int AttachFileId { get; set; }
        public string Blob { get; set; }

        public virtual ICollection<AttachFileOm> AttachFileOm { get; set; }
    }
}
