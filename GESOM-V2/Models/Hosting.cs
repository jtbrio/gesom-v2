﻿using System;
using System.Collections.Generic;

namespace GESOM_V2.Models
{
    public partial class Hosting
    {
        public Hosting()
        {
            Destination = new HashSet<Destination>();
        }

        public int HostingId { get; set; }
        public int? TownId { get; set; }
        public string HostingName { get; set; }
        public int? FeesHost { get; set; }
        public int? FeesFood { get; set; }

        public virtual Town Town { get; set; }
        public virtual ICollection<Destination> Destination { get; set; }
    }
}
