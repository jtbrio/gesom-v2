﻿using System;
using System.Collections.Generic;

namespace GESOM_V2.Models
{
    public partial class CarOcm
    {
        public CarOcm()
        {
            Om = new HashSet<Om>();
        }

        public string CarOcmId { get; set; }
        public string CarOcmDesc { get; set; }

        public virtual ICollection<Om> Om { get; set; }
    }
}
