﻿using System;
using System.Collections.Generic;

namespace GESOM_V2.Models
{
    public partial class TransitionType
    {
        public TransitionType()
        {
            Transition = new HashSet<Transition>();
        }

        public int TransitionTypeId { get; set; }
        public string TransitionTypeName { get; set; }

        public virtual ICollection<Transition> Transition { get; set; }
    }
}
