﻿using System;
using System.Collections.Generic;

namespace GESOM_V2.Models
{
    public partial class Om
    {
        public Om()
        {
            AttachFileOm = new HashSet<AttachFileOm>();
            DestinationOm = new HashSet<DestinationOm>();
            OmComments = new HashSet<OmComments>();
            OmValidator = new HashSet<OmValidator>();
            State = new HashSet<State>();
        }

        public int OmId { get; set; }
        public int? UserId { get; set; }
        public string OmMotive { get; set; }
        public string OmDesc { get; set; }
        public int? OmTypeId { get; set; }
        public int? SectorId { get; set; }
        public DateTime? CreateDate { get; set; }
        public int? BusFees { get; set; }
        public int? TaxiFees { get; set; }
        public int? TrainFees { get; set; }
        public int? MileAllowance { get; set; }
        public int? RoadToll { get; set; }
        public string CarOcmId { get; set; }
        public int? DriverOcmId { get; set; }
        public int? AllowedFees { get; set; }
        public bool? DailyPlan { get; set; }
        public string EndHour { get; set; }
        public int? TotalFees { get; set; }

        public virtual CarOcm CarOcm { get; set; }
        public virtual DriverOcm DriverOcm { get; set; }
        public virtual User User { get; set; }
        public virtual ICollection<AttachFileOm> AttachFileOm { get; set; }
        public virtual ICollection<DestinationOm> DestinationOm { get; set; }
        public virtual ICollection<OmComments> OmComments { get; set; }
        public virtual ICollection<OmValidator> OmValidator { get; set; }
        public virtual ICollection<State> State { get; set; }
    }
}
