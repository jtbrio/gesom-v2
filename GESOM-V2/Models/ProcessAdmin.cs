﻿using System;
using System.Collections.Generic;

namespace GESOM_V2.Models
{
    public partial class ProcessAdmin
    {
        public int ProcessId { get; set; }
        public int UserId { get; set; }

        public virtual Process Process { get; set; }
        public virtual User User { get; set; }
    }
}
