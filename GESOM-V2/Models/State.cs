﻿using System;
using System.Collections.Generic;

namespace GESOM_V2.Models
{
    public partial class State
    {
        public int StateId { get; set; }
        public int StateTypeId { get; set; }
        public int OmId { get; set; }
        public DateTime? StateDate { get; set; }

        public virtual Om Om { get; set; }
        public virtual StateType StateType { get; set; }
    }
}
