﻿using System;
using System.Collections.Generic;

namespace GESOM_V2.Models
{
    public partial class Destination
    {
        public Destination()
        {
            DestinationOm = new HashSet<DestinationOm>();
        }

        public int DestinationId { get; set; }
        public int HostingId { get; set; }
        public DateTime? StartDate { get; set; }
        public DateTime? EndDate { get; set; }

        public virtual Hosting Hosting { get; set; }
        public virtual ICollection<DestinationOm> DestinationOm { get; set; }
    }
}
