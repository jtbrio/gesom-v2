﻿using System;
using System.Collections.Generic;

namespace GESOM_V2.Models
{
    public partial class Town
    {
        public Town()
        {
            Hosting = new HashSet<Hosting>();
        }

        public int TownId { get; set; }
        public string TownName { get; set; }

        public virtual ICollection<Hosting> Hosting { get; set; }
    }
}
