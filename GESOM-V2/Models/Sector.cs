﻿using System;
using System.Collections.Generic;

namespace GESOM_V2.Models
{
    public partial class Sector
    {
        public int SectorId { get; set; }
        public string SectorName { get; set; }
    }
}
