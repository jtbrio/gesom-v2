﻿using System;
using System.Collections.Generic;

namespace GESOM_V2.Models
{
    public partial class Function
    {
        public Function()
        {
            User = new HashSet<User>();
        }

        public int FunctionId { get; set; }
        public string FunctionName { get; set; }

        public virtual ICollection<User> User { get; set; }
    }
}
