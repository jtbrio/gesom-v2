﻿using System;
using System.Collections.Generic;

namespace GESOM_V2.Models
{
    public partial class DestinationOm
    {
        public int DestinationOmId { get; set; }
        public int DestinationId { get; set; }
        public int OmId { get; set; }

        public virtual Destination Destination { get; set; }
        public virtual Om Om { get; set; }
    }
}
