﻿using System;
using System.Collections.Generic;

namespace GESOM_V2.Models
{
    public partial class DriverOcm
    {
        public DriverOcm()
        {
            Om = new HashSet<Om>();
        }

        public int DriverOcmId { get; set; }
        public string DriverFirstName { get; set; }
        public string DriverLastName { get; set; }
        public bool DriverAvailability { get; set; }

        public virtual ICollection<Om> Om { get; set; }
    }
}
