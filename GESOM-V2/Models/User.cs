﻿using System;
using System.Collections.Generic;

namespace GESOM_V2.Models
{
    public partial class User
    {
        public User()
        {
            Om = new HashSet<Om>();
            OmValidator = new HashSet<OmValidator>();
        }

        public int UserId { get; set; }
        public string Login { get; set; }
        public string Name { get; set; }
        public string Email { get; set; }
        public int? Phone { get; set; }
        public bool? IsActive { get; set; }
        public int? ProfileId { get; set; }
        public int? DirectionId { get; set; }
        public int? DepartmentId { get; set; }
        public int? ServiceId { get; set; }
        public DateTime? BirthDate { get; set; }
        public int? RoleId { get; set; }

        public virtual Department Department { get; set; }
        public virtual Direction Direction { get; set; }
        public virtual Profile Profile { get; set; }
        public virtual Role Role { get; set; }
        public virtual Service Service { get; set; }
        public virtual ICollection<Om> Om { get; set; }
        public virtual ICollection<OmValidator> OmValidator { get; set; }
    }
}
