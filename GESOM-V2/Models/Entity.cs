﻿using System;
using System.Collections.Generic;

namespace GESOM_V2.Models
{
    public partial class Entity
    {
        public int EntityId { get; set; }
        public int? UserId { get; set; }
        public string EntityName { get; set; }
        public string EntityDesc { get; set; }
        public int? EntityParentId { get; set; }
    }
}
