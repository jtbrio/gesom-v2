﻿using System;
using System.Collections.Generic;

namespace GESOM_V2.Models
{
    public partial class Profile
    {
        public Profile()
        {
            User = new HashSet<User>();
        }

        public int ProfileId { get; set; }
        public string ProfileCode { get; set; }
        public string ProfileName { get; set; }

        public virtual ICollection<User> User { get; set; }
    }
}
