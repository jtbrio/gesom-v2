﻿using System;
using System.Collections.Generic;

namespace GESOM_V2.Models
{
    public partial class Direction
    {
        public Direction()
        {
            Department = new HashSet<Department>();
            User = new HashSet<User>();
        }

        public int DirectionId { get; set; }
        public string DirectionName { get; set; }
        public int? DirectionBudget { get; set; }

        public virtual ICollection<Department> Department { get; set; }
        public virtual ICollection<User> User { get; set; }
    }
}
