﻿using System;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata;

namespace GESOM_V2.Models
{
    public partial class DataContext : DbContext
    {
        public DataContext()
        {
        }

        public DataContext(DbContextOptions<DataContext> options)
            : base(options)
        {
        }

        public virtual DbSet<AttachFile> AttachFile { get; set; }
        public virtual DbSet<AttachFileOm> AttachFileOm { get; set; }
        public virtual DbSet<CarOcm> CarOcm { get; set; }
        public virtual DbSet<Department> Department { get; set; }
        public virtual DbSet<Destination> Destination { get; set; }
        public virtual DbSet<DestinationOm> DestinationOm { get; set; }
        public virtual DbSet<Direction> Direction { get; set; }
        public virtual DbSet<DriverOcm> DriverOcm { get; set; }
        public virtual DbSet<Hosting> Hosting { get; set; }
        public virtual DbSet<Om> Om { get; set; }
        public virtual DbSet<OmComments> OmComments { get; set; }
        public virtual DbSet<OmValidator> OmValidator { get; set; }
        public virtual DbSet<Profile> Profile { get; set; }
        public virtual DbSet<Role> Role { get; set; }
        public virtual DbSet<Sector> Sector { get; set; }
        public virtual DbSet<Service> Service { get; set; }
        public virtual DbSet<State> State { get; set; }
        public virtual DbSet<StateType> StateType { get; set; }
        public virtual DbSet<Town> Town { get; set; }
        public virtual DbSet<TypeOm> TypeOm { get; set; }
        public virtual DbSet<User> User { get; set; }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            if (!optionsBuilder.IsConfigured)
            {
                optionsBuilder.UseSqlServer("Data Source=DESKTOP-6CH7ED2;Initial Catalog=GESOM;User ID=gesom;Password=G350M");
            }
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.HasAnnotation("ProductVersion", "2.2.6-servicing-10079");

            modelBuilder.Entity<AttachFile>(entity =>
            {
                entity.ToTable("attach_file");

                entity.Property(e => e.AttachFileId).HasColumnName("attach_file_id");

                entity.Property(e => e.Blob).HasColumnName("blob");
            });

            modelBuilder.Entity<AttachFileOm>(entity =>
            {
                entity.ToTable("attach_file_om");

                entity.Property(e => e.AttachFileOmId).HasColumnName("attach_file_om_id");

                entity.Property(e => e.AttachFileId).HasColumnName("attach_file_id");

                entity.Property(e => e.OmId).HasColumnName("om_id");

                entity.Property(e => e.UploadDate)
                    .HasColumnName("upload_date")
                    .HasColumnType("datetime");

                entity.HasOne(d => d.AttachFile)
                    .WithMany(p => p.AttachFileOm)
                    .HasForeignKey(d => d.AttachFileId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_AttachFileOM_attach_file");

                entity.HasOne(d => d.Om)
                    .WithMany(p => p.AttachFileOm)
                    .HasForeignKey(d => d.OmId)
                    .HasConstraintName("FK_AttachFileOM_Om");
            });

            modelBuilder.Entity<CarOcm>(entity =>
            {
                entity.ToTable("car_ocm");

                entity.Property(e => e.CarOcmId)
                    .HasColumnName("car_ocm_id")
                    .HasMaxLength(50)
                    .ValueGeneratedNever();

                entity.Property(e => e.CarOcmDesc).HasColumnName("car_ocm_desc");
            });

            modelBuilder.Entity<Department>(entity =>
            {
                entity.ToTable("department");

                entity.Property(e => e.DepartmentId).HasColumnName("department_id");

                entity.Property(e => e.DepartmentBudget).HasColumnName("department_budget");

                entity.Property(e => e.DepartmentName)
                    .HasColumnName("department_name")
                    .HasMaxLength(100);

                entity.Property(e => e.DirectionId).HasColumnName("direction_id");

                entity.HasOne(d => d.Direction)
                    .WithMany(p => p.Department)
                    .HasForeignKey(d => d.DirectionId)
                    .OnDelete(DeleteBehavior.Cascade)
                    .HasConstraintName("FK_department_direction");
            });

            modelBuilder.Entity<Destination>(entity =>
            {
                entity.ToTable("destination");

                entity.Property(e => e.DestinationId).HasColumnName("destination_id");

                entity.Property(e => e.EndDate)
                    .HasColumnName("end_date")
                    .HasColumnType("datetime");

                entity.Property(e => e.HostingId).HasColumnName("hosting_id");

                entity.Property(e => e.StartDate)
                    .HasColumnName("start_date")
                    .HasColumnType("datetime");

                entity.HasOne(d => d.Hosting)
                    .WithMany(p => p.Destination)
                    .HasForeignKey(d => d.HostingId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_destination_om_hosting");
            });

            modelBuilder.Entity<DestinationOm>(entity =>
            {
                entity.ToTable("destination_om");

                entity.Property(e => e.DestinationOmId).HasColumnName("destination_om_id");

                entity.Property(e => e.DestinationId).HasColumnName("destination_id");

                entity.Property(e => e.OmId).HasColumnName("om_id");

                entity.HasOne(d => d.Destination)
                    .WithMany(p => p.DestinationOm)
                    .HasForeignKey(d => d.DestinationId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_destination_om_destination");

                entity.HasOne(d => d.Om)
                    .WithMany(p => p.DestinationOm)
                    .HasForeignKey(d => d.OmId)
                    .HasConstraintName("FK_destination_om_om1");
            });

            modelBuilder.Entity<Direction>(entity =>
            {
                entity.ToTable("direction");

                entity.Property(e => e.DirectionId).HasColumnName("direction_id");

                entity.Property(e => e.DirectionBudget).HasColumnName("direction_budget");

                entity.Property(e => e.DirectionName)
                    .HasColumnName("direction_name")
                    .HasMaxLength(100);
            });

            modelBuilder.Entity<DriverOcm>(entity =>
            {
                entity.ToTable("driver_ocm");

                entity.Property(e => e.DriverOcmId).HasColumnName("driver_ocm_id");

                entity.Property(e => e.DriverAvailability).HasColumnName("driver_availability");

                entity.Property(e => e.DriverFirstName)
                    .HasColumnName("driver_first_name")
                    .HasMaxLength(50);

                entity.Property(e => e.DriverLastName)
                    .HasColumnName("driver_last_name")
                    .HasMaxLength(50);
            });

            modelBuilder.Entity<Hosting>(entity =>
            {
                entity.ToTable("hosting");

                entity.Property(e => e.HostingId).HasColumnName("hosting_id");

                entity.Property(e => e.FeesFood).HasColumnName("fees_food");

                entity.Property(e => e.FeesHost).HasColumnName("fees_host");

                entity.Property(e => e.HostingName)
                    .HasColumnName("hosting_name")
                    .HasMaxLength(50);

                entity.Property(e => e.TownId).HasColumnName("town_id");

                entity.HasOne(d => d.Town)
                    .WithMany(p => p.Hosting)
                    .HasForeignKey(d => d.TownId)
                    .OnDelete(DeleteBehavior.Cascade)
                    .HasConstraintName("FK_hosting_town");
            });

            modelBuilder.Entity<Om>(entity =>
            {
                entity.ToTable("om");

                entity.Property(e => e.OmId).HasColumnName("om_id");

                entity.Property(e => e.AllowedFees).HasColumnName("allowed_fees");

                entity.Property(e => e.BusFees).HasColumnName("bus_fees");

                entity.Property(e => e.CarOcmId)
                    .HasColumnName("car_ocm_id")
                    .HasMaxLength(50);

                entity.Property(e => e.CreateDate)
                    .HasColumnName("create_date")
                    .HasColumnType("datetime");

                entity.Property(e => e.DailyPlan).HasColumnName("daily_plan");

                entity.Property(e => e.DriverOcmId).HasColumnName("driver_ocm_id");

                entity.Property(e => e.EndHour)
                    .HasColumnName("end_hour")
                    .HasMaxLength(50);

                entity.Property(e => e.MileAllowance).HasColumnName("mile_allowance");

                entity.Property(e => e.OmDesc).HasColumnName("om_desc");

                entity.Property(e => e.OmMotive)
                    .HasColumnName("om_motive")
                    .HasMaxLength(500);

                entity.Property(e => e.OmTypeId).HasColumnName("om_type_id");

                entity.Property(e => e.RoadToll).HasColumnName("road_toll");

                entity.Property(e => e.SectorId).HasColumnName("sector_id");

                entity.Property(e => e.TaxiFees).HasColumnName("taxi_fees");

                entity.Property(e => e.TotalFees)
                    .HasColumnName("total_fees")
                    .HasDefaultValueSql("((0))");

                entity.Property(e => e.TrainFees).HasColumnName("train_fees");

                entity.Property(e => e.UserId).HasColumnName("user_id");

                entity.HasOne(d => d.CarOcm)
                    .WithMany(p => p.Om)
                    .HasForeignKey(d => d.CarOcmId)
                    .HasConstraintName("FK_Om_CarOcm");

                entity.HasOne(d => d.DriverOcm)
                    .WithMany(p => p.Om)
                    .HasForeignKey(d => d.DriverOcmId)
                    .HasConstraintName("FK_Om_DriverOcm");

                entity.HasOne(d => d.User)
                    .WithMany(p => p.Om)
                    .HasForeignKey(d => d.UserId)
                    .HasConstraintName("FK_om_user");
            });

            modelBuilder.Entity<OmComments>(entity =>
            {
                entity.ToTable("om_comments");

                entity.Property(e => e.OmCommentsId).HasColumnName("om_comments_id");

                entity.Property(e => e.Comment)
                    .HasColumnName("comment")
                    .HasMaxLength(500);

                entity.Property(e => e.CommentDate)
                    .HasColumnName("comment_date")
                    .HasColumnType("datetime");

                entity.Property(e => e.OmId).HasColumnName("om_id");

                entity.Property(e => e.ReceiverId).HasColumnName("receiver_id");

                entity.Property(e => e.SenderId).HasColumnName("sender_id");

                entity.HasOne(d => d.Om)
                    .WithMany(p => p.OmComments)
                    .HasForeignKey(d => d.OmId)
                    .HasConstraintName("FK_om_comments_om");
            });

            modelBuilder.Entity<OmValidator>(entity =>
            {
                entity.ToTable("om_validator");

                entity.Property(e => e.OmValidatorId).HasColumnName("om_validator_id");

                entity.Property(e => e.Message)
                    .HasColumnName("message")
                    .HasMaxLength(500);

                entity.Property(e => e.OmId).HasColumnName("om_id");

                entity.Property(e => e.StateId).HasColumnName("state_id");

                entity.Property(e => e.UserId).HasColumnName("user_id");

                entity.Property(e => e.ValidDate)
                    .HasColumnName("valid_date")
                    .HasColumnType("datetime");

                entity.HasOne(d => d.Om)
                    .WithMany(p => p.OmValidator)
                    .HasForeignKey(d => d.OmId)
                    .HasConstraintName("FK_om_validator_om");

                entity.HasOne(d => d.State)
                    .WithMany(p => p.OmValidator)
                    .HasForeignKey(d => d.StateId)
                    .HasConstraintName("FK_om_validator_state_type");

                entity.HasOne(d => d.User)
                    .WithMany(p => p.OmValidator)
                    .HasForeignKey(d => d.UserId)
                    .HasConstraintName("FK_om_validator_user");
            });

            modelBuilder.Entity<Profile>(entity =>
            {
                entity.ToTable("profile");

                entity.Property(e => e.ProfileId).HasColumnName("profile_id");

                entity.Property(e => e.ProfileCode)
                    .HasColumnName("profile_code")
                    .HasMaxLength(50);

                entity.Property(e => e.ProfileName)
                    .HasColumnName("profile_name")
                    .HasMaxLength(50);
            });

            modelBuilder.Entity<Role>(entity =>
            {
                entity.ToTable("role");

                entity.Property(e => e.RoleId).HasColumnName("role_id");

                entity.Property(e => e.RoleName)
                    .HasColumnName("role_name")
                    .HasMaxLength(50);
            });

            modelBuilder.Entity<Sector>(entity =>
            {
                entity.ToTable("sector");

                entity.Property(e => e.SectorId).HasColumnName("sector_id");

                entity.Property(e => e.SectorName)
                    .HasColumnName("sector_name")
                    .HasMaxLength(50);
            });

            modelBuilder.Entity<Service>(entity =>
            {
                entity.ToTable("service");

                entity.Property(e => e.ServiceId).HasColumnName("service_id");

                entity.Property(e => e.DepartmentId).HasColumnName("department_id");

                entity.Property(e => e.ServiceBudget).HasColumnName("service_budget");

                entity.Property(e => e.ServiceName)
                    .HasColumnName("service_name")
                    .HasMaxLength(100);

                entity.HasOne(d => d.Department)
                    .WithMany(p => p.Service)
                    .HasForeignKey(d => d.DepartmentId)
                    .OnDelete(DeleteBehavior.Cascade)
                    .HasConstraintName("FK_service_department");
            });

            modelBuilder.Entity<State>(entity =>
            {
                entity.ToTable("state");

                entity.Property(e => e.StateId).HasColumnName("state_id");

                entity.Property(e => e.OmId).HasColumnName("om_id");

                entity.Property(e => e.StateDate)
                    .HasColumnName("state_date")
                    .HasColumnType("datetime");

                entity.Property(e => e.StateTypeId).HasColumnName("state_type_id");

                entity.HasOne(d => d.Om)
                    .WithMany(p => p.State)
                    .HasForeignKey(d => d.OmId)
                    .HasConstraintName("FK_State_Om1");

                entity.HasOne(d => d.StateType)
                    .WithMany(p => p.State)
                    .HasForeignKey(d => d.StateTypeId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_State_StateType");
            });

            modelBuilder.Entity<StateType>(entity =>
            {
                entity.ToTable("state_type");

                entity.Property(e => e.StateTypeId).HasColumnName("state_type_id");

                entity.Property(e => e.StateTypeName)
                    .HasColumnName("state_type_name")
                    .HasMaxLength(50);
            });

            modelBuilder.Entity<Town>(entity =>
            {
                entity.ToTable("town");

                entity.Property(e => e.TownId).HasColumnName("town_id");

                entity.Property(e => e.TownName)
                    .HasColumnName("town_name")
                    .HasMaxLength(50);
            });

            modelBuilder.Entity<TypeOm>(entity =>
            {
                entity.ToTable("type_om");

                entity.Property(e => e.TypeOmId).HasColumnName("type_om_id");

                entity.Property(e => e.TypeOmName)
                    .HasColumnName("type_om_name")
                    .HasMaxLength(50);
            });

            modelBuilder.Entity<User>(entity =>
            {
                entity.ToTable("user");

                entity.Property(e => e.UserId).HasColumnName("user_id");

                entity.Property(e => e.BirthDate)
                    .HasColumnName("birth_date")
                    .HasColumnType("datetime");

                entity.Property(e => e.DepartmentId).HasColumnName("department_id");

                entity.Property(e => e.DirectionId).HasColumnName("direction_id");

                entity.Property(e => e.Email)
                    .HasColumnName("email")
                    .HasMaxLength(100);

                entity.Property(e => e.IsActive).HasColumnName("is_active");

                entity.Property(e => e.Login)
                    .IsRequired()
                    .HasColumnName("login")
                    .HasMaxLength(50);

                entity.Property(e => e.Name)
                    .HasColumnName("name")
                    .HasMaxLength(50);

                entity.Property(e => e.Phone).HasColumnName("phone");

                entity.Property(e => e.ProfileId).HasColumnName("profile_id");

                entity.Property(e => e.RoleId).HasColumnName("role_id");

                entity.Property(e => e.ServiceId).HasColumnName("service_id");

                entity.HasOne(d => d.Department)
                    .WithMany(p => p.User)
                    .HasForeignKey(d => d.DepartmentId)
                    .HasConstraintName("FK_user_department");

                entity.HasOne(d => d.Direction)
                    .WithMany(p => p.User)
                    .HasForeignKey(d => d.DirectionId)
                    .HasConstraintName("FK_user_direction");

                entity.HasOne(d => d.Profile)
                    .WithMany(p => p.User)
                    .HasForeignKey(d => d.ProfileId)
                    .HasConstraintName("FK_user_profile");

                entity.HasOne(d => d.Role)
                    .WithMany(p => p.User)
                    .HasForeignKey(d => d.RoleId)
                    .OnDelete(DeleteBehavior.Cascade)
                    .HasConstraintName("FK_user_role");

                entity.HasOne(d => d.Service)
                    .WithMany(p => p.User)
                    .HasForeignKey(d => d.ServiceId)
                    .HasConstraintName("FK_user_service");
            });
        }
    }
}
