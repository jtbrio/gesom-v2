﻿using GESOM_V2.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace GESOM_V2.ModelDTO
{
    public class TokenReturn
    {
        public User User { get; set; }
        public DateTime ExpireDate { get; set; }
    }
}
