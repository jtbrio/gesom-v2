﻿using GESOM_V2.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace GESOM_V2.ModelDTO
{

    public class DetailsOM
    {

        public int OmId { get; set; }
        public string OmMotive { get; set; }
        public string OmDesc { get; set; }
        public DateTime? CreateDate { get; set; }
        public int? BusFees { get; set; }
        public int? TaxiFees { get; set; }
        public int? UserId { get; set; }
        public int? OmTypeId { get; set; }
        public int? SectorId { get; set; }
        public int? TrainFees { get; set; }
        public int? MileAllowance { get; set; }
        public int? RoadToll { get; set; }
        public string CarOcmId { get; set; }
        public int? DriverOcmId { get; set; }
        public string EndHour { get; set; }
        public string Blob { get; set; }
        public int? AllowedFees { get; set; }
        public bool? DailyPlan { get; set; }
        public string OtherInfos { get; set; }
    }


    public class Destin
    {
        public int OmId { get; set; }
        public int? HostingId { get; set; }
        public int? TownId { get; set; }
        public int destinationId { get; set; }
        public string TownName { get; set; }
        public string HostName { get; set; }
        public int? FeesHost { get; set; }
        public int? FeesFood { get; set; }
        public DateTime? StartDate { get; set; }

        public DateTime? EndDate { get; set; }
    }

    public class Stat
    {
        public int? OmId { get; set; }
        public string StateName { get; set; }
        public DateTime? StateDate { get; set; }

        public string UserName { get; set; }

        public int ? UserPhone { get; set; }

    }

    public class DriverOcm
    {
        public int DriverOcmId { get; set; }
        public string DriverFirstName { get; set; }
        public string DriverLastName { get; set; }
        public bool? DriverAvailability { get; set; }
    }

    public class CarOcm
    {
        public string CarOcmId { get; set; }
        public string CarOcmDesc { get; set; }
    }

    public class AllInfo
    {
        public DetailsOM detailsOM { get; set; }

        public List<Destin> Destinations { get; set; }

        public List<Stat> States { get; set; }

        public DriverOcm DriverOcm { get; set; }

        public CarOcm CarOcm { get; set; }
    }
}
